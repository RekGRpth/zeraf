.class public final Lcom/google/android/youtube/core/model/UserProfile$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;
.implements Ljava/io/Serializable;


# instance fields
.field private activityUri:Landroid/net/Uri;

.field private age:I

.field private alternateUri:Landroid/net/Uri;

.field private channelId:Ljava/lang/String;

.field private channelViewsCount:J

.field private displayUsername:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private favoritesCount:I

.field private favoritesUri:Landroid/net/Uri;

.field private gender:Lcom/google/android/youtube/core/model/UserProfile$Gender;

.field private isEligibleForChannel:Z

.field private isLightweight:Z

.field private playlistsUri:Landroid/net/Uri;

.field private plusUserId:Ljava/lang/String;

.field private selfUri:Landroid/net/Uri;

.field private subscribersCount:I

.field private subscriptionsCount:I

.field private subscriptionsUri:Landroid/net/Uri;

.field private summary:Ljava/lang/String;

.field private thumbnailUri:Landroid/net/Uri;

.field private uploadViewsCount:J

.field private uploadedCount:I

.field private uploadsUri:Landroid/net/Uri;

.field private uri:Landroid/net/Uri;

.field private username:Ljava/lang/String;

.field private watchHistoryUri:Landroid/net/Uri;

.field private watchLaterUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->age:I

    sget-object v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;->UNKNOWN:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->gender:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->selfUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->alternateUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->username:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->plusUserId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->displayUsername:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->email:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->age:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->gender:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->summary:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isLightweight:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isEligibleForChannel:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadedCount:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesCount:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsCount:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchHistoryUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchLaterUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->playlistsUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->activityUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelViewsCount:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadViewsCount:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscribersCount:I

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/UserProfile$Builder;->build()Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->selfUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->alternateUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->plusUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->displayUsername:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->age:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->gender:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->summary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->thumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isLightweight:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isEligibleForChannel:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadsUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadedCount:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesCount:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsCount:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchHistoryUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchLaterUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->playlistsUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->activityUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelViewsCount:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadViewsCount:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscribersCount:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    return-void
.end method


# virtual methods
.method public final activityUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->activityUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final age(I)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->age:I

    return-object p0
.end method

.method public final alternateUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->alternateUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final build()Lcom/google/android/youtube/core/model/UserProfile;
    .locals 32

    new-instance v2, Lcom/google/android/youtube/core/model/UserProfile;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->selfUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->alternateUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->username:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->displayUsername:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->plusUserId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->email:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->age:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->gender:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->summary:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->thumbnailUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isLightweight:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isEligibleForChannel:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadsUri:Landroid/net/Uri;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadedCount:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesUri:Landroid/net/Uri;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesCount:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsCount:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchHistoryUri:Landroid/net/Uri;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchLaterUri:Landroid/net/Uri;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->playlistsUri:Landroid/net/Uri;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->activityUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelViewsCount:J

    move-wide/from16 v27, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadViewsCount:J

    move-wide/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscribersCount:I

    move/from16 v31, v0

    invoke-direct/range {v2 .. v31}, Lcom/google/android/youtube/core/model/UserProfile;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/youtube/core/model/UserProfile$Gender;Ljava/lang/String;Landroid/net/Uri;ZZLandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JJI)V

    return-object v2
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/UserProfile$Builder;->build()Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v0

    return-object v0
.end method

.method public final channelId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 1

    const-string v0, "channel id cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelId:Ljava/lang/String;

    return-object p0
.end method

.method public final channelViewsCount(J)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->channelViewsCount:J

    return-object p0
.end method

.method public final displayUsername(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->displayUsername:Ljava/lang/String;

    return-object p0
.end method

.method public final email(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public final favoritesCount(I)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesCount:I

    return-object p0
.end method

.method public final favoritesUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->favoritesUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final gender(Lcom/google/android/youtube/core/model/UserProfile$Gender;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->gender:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    return-object p0
.end method

.method public final isEligibleForChannel(Z)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isEligibleForChannel:Z

    return-object p0
.end method

.method public final isLightweight(Z)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->isLightweight:Z

    return-object p0
.end method

.method public final playlistsUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->playlistsUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final plusUserId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->plusUserId:Ljava/lang/String;

    return-object p0
.end method

.method public final selfUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->selfUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final subscribersCount(I)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscribersCount:I

    return-object p0
.end method

.method public final subscriptionsCount(I)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsCount:I

    return-object p0
.end method

.method public final subscriptionsUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->subscriptionsUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final summary(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->summary:Ljava/lang/String;

    return-object p0
.end method

.method public final thumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->thumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final uploadViewsCount(J)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadViewsCount:J

    return-object p0
.end method

.method public final uploadedCount(I)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadedCount:I

    return-object p0
.end method

.method public final uploadsUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uploadsUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->uri:Landroid/net/Uri;

    return-object p0
.end method

.method public final username(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->username:Ljava/lang/String;

    return-object p0
.end method

.method public final watchHistoryUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchHistoryUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final watchLaterUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/UserProfile$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/UserProfile$Builder;->watchLaterUri:Landroid/net/Uri;

    return-object p0
.end method
