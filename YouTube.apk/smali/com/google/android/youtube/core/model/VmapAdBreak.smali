.class public final Lcom/google/android/youtube/core/model/VmapAdBreak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final ads:Ljava/util/List;

.field public final isDisplayAdAllowed:Z

.field public final isLinearAdAllowed:Z

.field public final isNonlinearAdAllowed:Z

.field public final isPreroll:Z

.field public final offsetType:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

.field public final offsetValue:I

.field public final trackingEvents:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/al;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/al;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    const/4 v5, 0x0

    const/4 v0, 0x1

    invoke-static {}, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->values()[Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v0, :cond_1

    move v3, v0

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v0, :cond_2

    move v4, v0

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-ne v6, v0, :cond_0

    move v5, v0

    :cond_0
    invoke-static {p1}, Lcom/google/android/youtube/core/model/VmapAdBreak;->readAdBreakList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/youtube/core/model/VmapAdBreak;->readTrackingEventList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/core/model/VmapAdBreak;-><init>(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;IZZZLjava/util/List;Ljava/util/List;)V

    return-void

    :cond_1
    move v3, v5

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;IZZZLjava/util/List;Ljava/util/List;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "offsetType must not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetType:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    iput-boolean p3, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isLinearAdAllowed:Z

    iput-boolean p4, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isNonlinearAdAllowed:Z

    iput-boolean p5, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isDisplayAdAllowed:Z

    invoke-static {p6}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    const-string v0, "trackingEvents must not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->trackingEvents:Ljava/util/List;

    if-ltz p2, :cond_4

    move v0, v1

    :goto_0
    const-string v3, "offsetValue must be >= 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->POST_ROLL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_5

    :cond_0
    move v0, v2

    :goto_1
    iput v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetValue:I

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->TIME:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->PERCENTAGE:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_6

    move v3, v1

    :goto_2
    if-nez p2, :cond_7

    move v0, v1

    :goto_3
    and-int/2addr v0, v3

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isPreroll:Z

    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, p2

    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;IZZZLjava/util/List;Ljava/util/List;Lcom/google/android/youtube/core/model/al;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/google/android/youtube/core/model/VmapAdBreak;-><init>(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;IZZZLjava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static readAdBreakList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/youtube/core/model/VastAd;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static readTrackingEventList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/youtube/core/model/VmapAdBreak;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetType:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetType:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetValue:I

    iget v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetValue:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isLinearAdAllowed:Z

    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->isLinearAdAllowed:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isNonlinearAdAllowed:Z

    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->isNonlinearAdAllowed:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isDisplayAdAllowed:Z

    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->isDisplayAdAllowed:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->trackingEvents:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VmapAdBreak;->trackingEvents:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetType:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->offsetValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isLinearAdAllowed:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isNonlinearAdAllowed:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isDisplayAdAllowed:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdBreak;->trackingEvents:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
