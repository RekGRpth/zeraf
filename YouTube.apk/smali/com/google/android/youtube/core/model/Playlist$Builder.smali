.class public Lcom/google/android/youtube/core/model/Playlist$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;
.implements Ljava/io/Serializable;


# instance fields
.field private author:Ljava/lang/String;

.field private contentUri:Landroid/net/Uri;

.field private editUri:Landroid/net/Uri;

.field private hqThumbnailUri:Landroid/net/Uri;

.field private id:Ljava/lang/String;

.field private sdThumbnailUri:Landroid/net/Uri;

.field private size:I

.field private summary:Ljava/lang/String;

.field private thumbnailUri:Landroid/net/Uri;

.field private title:Ljava/lang/String;

.field private updated:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->summary:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->author:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->updated:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->contentUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->editUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->hqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->sdThumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->size:I

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Playlist$Builder;->build()Lcom/google/android/youtube/core/model/Playlist;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->summary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->author:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->updated:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->contentUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->editUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->thumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->hqThumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->sdThumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->size:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    return-void
.end method


# virtual methods
.method public author(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->author:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/core/model/Playlist;
    .locals 12

    new-instance v0, Lcom/google/android/youtube/core/model/Playlist;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->summary:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->author:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->updated:Ljava/util/Date;

    iget-object v6, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->contentUri:Landroid/net/Uri;

    iget-object v7, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->editUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->thumbnailUri:Landroid/net/Uri;

    iget-object v9, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v10, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->sdThumbnailUri:Landroid/net/Uri;

    iget v11, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->size:I

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/core/model/Playlist;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;I)V

    return-object v0
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Playlist$Builder;->build()Lcom/google/android/youtube/core/model/Playlist;

    move-result-object v0

    return-object v0
.end method

.method public contentUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->contentUri:Landroid/net/Uri;

    return-object p0
.end method

.method public editUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->editUri:Landroid/net/Uri;

    return-object p0
.end method

.method public hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->hqThumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->sdThumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public size(I)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->size:I

    return-object p0
.end method

.method public summary(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->summary:Ljava/lang/String;

    return-object p0
.end method

.method public thumbnailUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->thumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public updated(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Playlist$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Playlist$Builder;->updated:Ljava/util/Date;

    return-object p0
.end method
