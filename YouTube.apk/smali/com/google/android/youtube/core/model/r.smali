.class public final Lcom/google/android/youtube/core/model/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/youtube/core/model/z;

.field private final c:Lcom/google/android/youtube/core/model/v;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/core/model/r;->a:I

    new-instance v0, Lcom/google/android/youtube/core/model/z;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/r;->b:Lcom/google/android/youtube/core/model/z;

    new-instance v0, Lcom/google/android/youtube/core/model/v;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/r;->c:Lcom/google/android/youtube/core/model/v;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/SubtitleWindow;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindow;

    iget v1, p0, Lcom/google/android/youtube/core/model/r;->a:I

    iget-object v2, p0, Lcom/google/android/youtube/core/model/r;->b:Lcom/google/android/youtube/core/model/z;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/z;->a()Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/model/r;->c:Lcom/google/android/youtube/core/model/v;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/v;->a()Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/SubtitleWindow;-><init>(ILcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;Lcom/google/android/youtube/core/model/q;)V

    return-object v0
.end method

.method public final a(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/r;->c:Lcom/google/android/youtube/core/model/v;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/model/v;->a(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/v;

    return-object p0
.end method

.method public final a(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/r;->b:Lcom/google/android/youtube/core/model/z;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/model/z;->a(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/z;

    return-object p0
.end method

.method public final b(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/r;->b:Lcom/google/android/youtube/core/model/z;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/model/z;->b(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/z;

    return-object p0
.end method

.method public final synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/r;->a()Lcom/google/android/youtube/core/model/SubtitleWindow;

    move-result-object v0

    return-object v0
.end method
