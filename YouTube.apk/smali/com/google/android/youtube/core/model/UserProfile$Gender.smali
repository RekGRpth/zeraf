.class public final enum Lcom/google/android/youtube/core/model/UserProfile$Gender;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/model/UserProfile$Gender;

.field public static final enum FEMALE:Lcom/google/android/youtube/core/model/UserProfile$Gender;

.field public static final enum MALE:Lcom/google/android/youtube/core/model/UserProfile$Gender;

.field public static final enum UNKNOWN:Lcom/google/android/youtube/core/model/UserProfile$Gender;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/model/UserProfile$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;->MALE:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    new-instance v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/core/model/UserProfile$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;->FEMALE:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    new-instance v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/model/UserProfile$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;->UNKNOWN:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/model/UserProfile$Gender;

    sget-object v1, Lcom/google/android/youtube/core/model/UserProfile$Gender;->MALE:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/UserProfile$Gender;->FEMALE:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/model/UserProfile$Gender;->UNKNOWN:Lcom/google/android/youtube/core/model/UserProfile$Gender;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;->$VALUES:[Lcom/google/android/youtube/core/model/UserProfile$Gender;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserProfile$Gender;
    .locals 1

    const-class v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/model/UserProfile$Gender;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/model/UserProfile$Gender;->$VALUES:[Lcom/google/android/youtube/core/model/UserProfile$Gender;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/model/UserProfile$Gender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/model/UserProfile$Gender;

    return-object v0
.end method
