.class public final Lcom/google/android/youtube/core/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Landroid/media/FaceDetector;

.field private final d:I

.field private final e:I

.field private final f:F

.field private g:[Landroid/media/FaceDetector$Face;


# direct methods
.method public constructor <init>(FII)V
    .locals 5

    const/4 v4, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v4, [Landroid/media/FaceDetector$Face;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/o;->g:[Landroid/media/FaceDetector$Face;

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "cropRatio must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p2, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "bitmapWidth must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p3, :cond_2

    :goto_2
    const-string v0, "bitmapHeight must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p3, p0, Lcom/google/android/youtube/core/a/o;->d:I

    iput p2, p0, Lcom/google/android/youtube/core/a/o;->e:I

    iput p1, p0, Lcom/google/android/youtube/core/a/o;->f:F

    int-to-float v0, p3

    const/high16 v1, 0x3e000000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/youtube/core/a/o;->a:I

    int-to-float v0, p3

    const/high16 v1, 0x3f600000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/youtube/core/a/o;->b:I

    new-instance v0, Landroid/media/FaceDetector;

    invoke-direct {v0, p2, p3, v4}, Landroid/media/FaceDetector;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/youtube/core/a/o;->c:Landroid/media/FaceDetector;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;
    .locals 9

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/youtube/core/a/o;->e:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->d:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->f:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/a/o;->e:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->f:F

    div-float/2addr v0, v2

    float-to-int v2, v0

    iget v0, p0, Lcom/google/android/youtube/core/a/o;->e:I

    sub-int/2addr v0, v2

    div-int/lit8 v3, v0, 0x2

    new-instance v0, Landroid/graphics/Rect;

    iget v4, p0, Lcom/google/android/youtube/core/a/o;->d:I

    invoke-direct {v0, v3, v1, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/a/o;->d:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->f:F

    div-float/2addr v0, v2

    float-to-int v3, v0

    iget-object v0, p0, Lcom/google/android/youtube/core/a/o;->c:Landroid/media/FaceDetector;

    iget-object v2, p0, Lcom/google/android/youtube/core/a/o;->g:[Landroid/media/FaceDetector$Face;

    invoke-virtual {v0, p1, v2}, Landroid/media/FaceDetector;->findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I

    move-result v4

    if-nez v4, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/a/o;->d:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x3

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->b:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    new-instance v0, Landroid/graphics/Rect;

    iget v4, p0, Lcom/google/android/youtube/core/a/o;->e:I

    add-int/2addr v3, v2

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5}, Landroid/graphics/PointF;-><init>()V

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    iget-object v6, p0, Lcom/google/android/youtube/core/a/o;->g:[Landroid/media/FaceDetector$Face;

    aget-object v6, v6, v0

    invoke-virtual {v6, v5}, Landroid/media/FaceDetector$Face;->getMidPoint(Landroid/graphics/PointF;)V

    iget v6, v5, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/google/android/youtube/core/a/o;->g:[Landroid/media/FaceDetector$Face;

    aget-object v7, v7, v0

    invoke-virtual {v7}, Landroid/media/FaceDetector$Face;->eyesDistance()F

    move-result v7

    const/high16 v8, 0x40000000

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    add-float/2addr v2, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    int-to-float v0, v4

    div-float v0, v2, v0

    div-int/lit8 v2, v3, 0x2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    float-to-int v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->a:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/google/android/youtube/core/a/o;->b:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    new-instance v0, Landroid/graphics/Rect;

    iget v4, p0, Lcom/google/android/youtube/core/a/o;->e:I

    add-int/2addr v3, v2

    invoke-direct {v0, v1, v2, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method
