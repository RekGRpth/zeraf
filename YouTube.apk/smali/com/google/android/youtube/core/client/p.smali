.class final Lcom/google/android/youtube/core/client/p;
.super Lcom/google/android/youtube/core/client/m;
.source "SourceFile"


# instance fields
.field b:Ljava/util/Iterator;

.field final synthetic c:Lcom/google/android/youtube/core/client/l;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/l;Ljava/lang/String;Ljava/util/Iterator;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/client/p;->c:Lcom/google/android/youtube/core/client/l;

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/youtube/core/client/m;-><init>(Lcom/google/android/youtube/core/client/l;Ljava/lang/String;J)V

    iput-object p3, p0, Lcom/google/android/youtube/core/client/p;->b:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/client/p;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/p;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VastAd;

    iget-boolean v1, v0, Lcom/google/android/youtube/core/model/VastAd;->isVastWrapper:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/client/p;->c:Lcom/google/android/youtube/core/client/l;

    invoke-static {v1}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    invoke-interface {v1, v0, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/core/client/p;->a(Lcom/google/android/youtube/core/model/VastAd;Landroid/net/Uri;)V

    goto :goto_1
.end method
