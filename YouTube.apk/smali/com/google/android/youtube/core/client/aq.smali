.class public final Lcom/google/android/youtube/core/client/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/client/VideoStats2Client;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:Lcom/google/android/youtube/core/utils/e;

.field private final d:J

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:Ljava/util/LinkedList;

.field private final k:Ljava/util/LinkedList;

.field private final l:Lcom/google/android/youtube/core/client/ar;

.field private final m:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field private final n:Ljava/util/List;

.field private final o:Z

.field private final p:Z

.field private final q:Lcom/google/android/youtube/core/client/StatParams;

.field private r:J

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private volatile x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://s.youtube.com/api/stats"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/client/aq;->a:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/youtube/core/client/StatParams;Ljava/util/List;)V
    .locals 17

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->length:Ljava/lang/String;

    move-object/from16 v0, p3

    iget v6, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delay:I

    move-object/from16 v0, p3

    iget-boolean v7, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->autoplay:Z

    move-object/from16 v0, p3

    iget-boolean v8, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->scriptedPlayback:Z

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->adformat:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->vssPlaybackId:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPlaybackId:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-wide v12, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->sessionStartTimestamp:J

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    invoke-direct/range {v1 .. v16}, Lcom/google/android/youtube/core/client/aq;-><init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/client/StatParams;Ljava/util/List;)V

    move-object/from16 v0, p3

    iget-wide v1, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->currentPlaybackPosition:J

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/google/android/youtube/core/client/aq;->r:J

    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delayedPingSent:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/youtube/core/client/aq;->s:Z

    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->finalPingSent:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/youtube/core/client/aq;->t:Z

    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->initialPingSent:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/youtube/core/client/aq;->u:Z

    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->throttled:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/youtube/core/client/aq;->x:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/client/aq;->l:Lcom/google/android/youtube/core/client/ar;

    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPingCounter:I

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/client/ar;->a(I)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/client/StatParams;Ljava/util/List;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/client/aq;->c:Lcom/google/android/youtube/core/utils/e;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/aq;->b:Lcom/google/android/youtube/core/async/au;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/aq;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/core/client/aq;->f:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/youtube/core/client/aq;->i:I

    iput-boolean p6, p0, Lcom/google/android/youtube/core/client/aq;->o:Z

    iput-boolean p7, p0, Lcom/google/android/youtube/core/client/aq;->p:Z

    iput-object p8, p0, Lcom/google/android/youtube/core/client/aq;->e:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/youtube/core/client/aq;->h:Ljava/lang/String;

    iput-wide p11, p0, Lcom/google/android/youtube/core/client/aq;->d:J

    iput-object p13, p0, Lcom/google/android/youtube/core/client/aq;->m:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/youtube/core/client/aq;->q:Lcom/google/android/youtube/core/client/StatParams;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/youtube/core/client/aq;->n:Ljava/util/List;

    new-instance v1, Lcom/google/android/youtube/core/client/ar;

    invoke-direct {v1, p2, p3, p10}, Lcom/google/android/youtube/core/client/ar;-><init>(Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/client/aq;->l:Lcom/google/android/youtube/core/client/ar;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/core/client/aq;->j:Ljava/util/LinkedList;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/core/client/aq;->k:Ljava/util/LinkedList;

    return-void
.end method

.method private static a(Ljava/util/LinkedList;)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {p0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/client/aq;->d(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri$Builder;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/client/aq;->b(Landroid/net/Uri$Builder;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->c:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/aq;->d:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/client/aq;->d(J)Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/client/aq;->d(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rt"

    invoke-virtual {p1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "cmt"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget v0, p0, Lcom/google/android/youtube/core/client/aq;->i:I

    if-lez v0, :cond_0

    const-string v0, "delay"

    iget v1, p0, Lcom/google/android/youtube/core/client/aq;->i:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->o:Z

    if-eqz v0, :cond_1

    const-string v0, "autoplay"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->p:Z

    if-eqz v0, :cond_2

    const-string v0, "splay"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->m:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    if-eq v0, v1, :cond_3

    const-string v0, "feature"

    iget-object v1, p0, Lcom/google/android/youtube/core/client/aq;->m:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->getFeatureString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/aq;->a(Landroid/net/Uri;)V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->t:Z

    if-eqz v0, :cond_1

    const-string v0, "Final ping for this playback has already been sent - Ignoring request"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->x:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pinging "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->b:Lcom/google/android/youtube/core/async/au;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/ap;->d(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/ap;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 5

    sget-object v0, Lcom/google/android/youtube/core/client/aq;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "watchtime"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/aq;->b(Landroid/net/Uri$Builder;)V

    const-string v2, "state"

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->v:Z

    if-eqz v0, :cond_1

    const-string v0, "playing"

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "st"

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/client/aq;->d(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "et"

    iget-wide v3, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/client/aq;->d(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_1
    if-eqz p1, :cond_0

    const-string v0, "final"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/aq;->a(Landroid/net/Uri;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->t:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->t:Z

    return-void

    :cond_1
    const-string v0, "paused"

    goto :goto_0

    :cond_2
    const-string v0, "st"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/aq;->j:Ljava/util/LinkedList;

    invoke-static {v2}, Lcom/google/android/youtube/core/client/aq;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "et"

    iget-object v3, p0, Lcom/google/android/youtube/core/client/aq;->k:Ljava/util/LinkedList;

    invoke-static {v3}, Lcom/google/android/youtube/core/client/aq;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1
.end method

.method private b(Landroid/net/Uri$Builder;)V
    .locals 3

    const-string v0, "cpn"

    iget-object v1, p0, Lcom/google/android/youtube/core/client/aq;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "yt"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "docid"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/aq;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ver"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "len"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/aq;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->q:Lcom/google/android/youtube/core/client/StatParams;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/client/StatParams;->a(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "el"

    const-string v1, "adunit"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "adformat"

    iget-object v1, p0, Lcom/google/android/youtube/core/client/aq;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_0
    return-void

    :cond_0
    const-string v0, "el"

    const-string v1, "detailpage"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method static synthetic c(J)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/client/aq;->d(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(J)Ljava/lang/String;
    .locals 8

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-double v4, p0

    const-wide v6, 0x408f400000000000L

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->w:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->k:Ljava/util/LinkedList;

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->s:Z

    sget-object v0, Lcom/google/android/youtube/core/client/aq;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "delayplay"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/aq;->a(Landroid/net/Uri$Builder;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    const/4 v2, 0x1

    iput-wide p1, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->u:Z

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/core/client/aq;->u:Z

    sget-object v0, Lcom/google/android/youtube/core/client/aq;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "playback"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/aq;->a(Landroid/net/Uri$Builder;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->s:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/client/aq;->i:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/client/aq;->i:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->i()V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->v:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->w:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->w:Z

    if-nez v0, :cond_2

    iput-boolean v2, p0, Lcom/google/android/youtube/core/client/aq;->w:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->j:Ljava/util/LinkedList;

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/client/aq;->l:Lcom/google/android/youtube/core/client/ar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/client/ar;->a(J)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->x:Z

    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/aq;->v:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->s:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/client/aq;->i:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->i()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->h()V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/aq;->a(Z)V

    return-void
.end method

.method public final b(J)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->h()V

    iput-wide p1, p0, Lcom/google/android/youtube/core/client/aq;->r:J

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->v:Z

    return-void
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->v:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->h()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->h()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/aq;->a(Z)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/aq;->h()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/aq;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/aq;->a(Z)V

    :cond_0
    return-void
.end method

.method public final g()Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;
    .locals 20

    new-instance v1, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/youtube/core/client/aq;->d:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/youtube/core/client/aq;->r:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/core/client/aq;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/core/client/aq;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/core/client/aq;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/core/client/aq;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/youtube/core/client/aq;->i:I

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/youtube/core/client/aq;->o:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/youtube/core/client/aq;->p:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/youtube/core/client/aq;->s:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/youtube/core/client/aq;->t:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/youtube/core/client/aq;->u:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/client/aq;->x:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/client/aq;->l:Lcom/google/android/youtube/core/client/ar;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/youtube/core/client/ar;->a(Lcom/google/android/youtube/core/client/ar;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/client/aq;->l:Lcom/google/android/youtube/core/client/ar;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/android/youtube/core/client/ar;->b(Lcom/google/android/youtube/core/client/ar;)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/client/aq;->m:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-object/from16 v19, v0

    invoke-direct/range {v1 .. v19}, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZZZZLjava/lang/String;ILcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    return-object v1
.end method
