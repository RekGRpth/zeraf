.class public final Lcom/google/android/youtube/core/client/ak;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/bg;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final h:Lcom/google/android/youtube/core/async/au;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const-wide/32 v7, 0x6ddd00

    invoke-direct {p0, p1, p4, p2, p3}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/android/youtube/core/client/ak;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/client/ak;->d()Lcom/google/android/youtube/core/cache/c;

    move-result-object v1

    const/16 v2, 0x14

    invoke-static {v2}, Lcom/google/android/youtube/core/client/ak;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/client/al;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/youtube/core/client/al;-><init>(B)V

    new-instance v4, Lcom/google/android/youtube/core/cache/l;

    invoke-direct {v4, v2, v0, v3}, Lcom/google/android/youtube/core/cache/l;-><init>(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/cache/m;)V

    new-instance v2, Lcom/google/android/youtube/core/converter/http/c;

    invoke-direct {v2, p5, p6}, Lcom/google/android/youtube/core/converter/http/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/youtube/core/converter/http/d;

    invoke-direct {v3}, Lcom/google/android/youtube/core/converter/http/d;-><init>()V

    invoke-virtual {p0, v2, v3}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v2

    const-wide/32 v5, 0x5265c00

    invoke-virtual {p0, v1, v2, v5, v6}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/async/s;

    invoke-direct {v2, v1}, Lcom/google/android/youtube/core/async/s;-><init>(Lcom/google/android/youtube/core/async/au;)V

    invoke-virtual {p0, v4, v2, v7, v8}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/ak;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/ca;

    invoke-direct {v1, p5, p6}, Lcom/google/android/youtube/core/converter/http/ca;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cb;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cb;-><init>()V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/async/s;

    invoke-direct {v2, v1}, Lcom/google/android/youtube/core/async/s;-><init>(Lcom/google/android/youtube/core/async/au;)V

    invoke-virtual {p0, v0, v2, v7, v8}, Lcom/google/android/youtube/core/client/ak;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ak;->h:Lcom/google/android/youtube/core/async/au;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ak;->h:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ak;->a:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
