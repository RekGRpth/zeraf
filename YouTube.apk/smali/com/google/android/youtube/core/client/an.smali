.class public final Lcom/google/android/youtube/core/client/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/bj;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/e;

.field private final b:Lcom/google/android/youtube/core/async/au;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/client/an;->a:Lcom/google/android/youtube/core/utils/e;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/an;->b:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V
    .locals 6

    const-string v0, "clock cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    const-string v1, "executor cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/youtube/core/async/an;

    const-string v2, "httpClient cannot be null"

    invoke-static {p3, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/client/HttpClient;

    sget-object v4, Lcom/google/android/youtube/core/converter/http/ec;->a:Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v5, Lcom/google/android/youtube/core/converter/http/bn;->b:Lcom/google/android/youtube/core/converter/http/bn;

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/youtube/core/async/an;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)V

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/client/an;-><init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;)Lcom/google/android/youtube/core/client/QoeStatsClient;
    .locals 10

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/an;->a:Lcom/google/android/youtube/core/utils/e;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/an;->b:Lcom/google/android/youtube/core/async/au;

    iget-object v3, p1, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;->clientPlaybackNonce:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;->videoId:Ljava/lang/String;

    iget-boolean v5, p1, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;->isLive:Z

    iget v6, p1, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;->itag:I

    iget-wide v7, p1, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;->startPlaybackTime:J

    iget-boolean v9, p1, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;->wasEnded:Z

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;-><init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;ZIJZ)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/client/ao;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/client/ao;-><init>(B)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZI)Lcom/google/android/youtube/core/client/QoeStatsClient;
    .locals 10

    const/4 v9, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/an;->a:Lcom/google/android/youtube/core/utils/e;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/an;->b:Lcom/google/android/youtube/core/async/au;

    const-wide/16 v7, -0x1

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;-><init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;ZIJZ)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/client/ao;

    invoke-direct {v0, v9}, Lcom/google/android/youtube/core/client/ao;-><init>(B)V

    goto :goto_0
.end method
