.class public interface abstract Lcom/google/android/youtube/core/client/bc;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract A()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract a()Lcom/google/android/youtube/core/async/GDataRequestFactory;
.end method

.method public abstract a(ILjava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/GDataRequestFactory$ComplaintReason;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;ZLcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract a(Ljava/lang/String;ZLcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract b(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b(Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract c(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract c(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract c(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract c(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract c(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract d(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract d(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract d(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract d(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract d(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract e()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract e(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract e(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract e(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract e(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract f()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract f(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract f(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract f(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract f(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract g()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract g(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract g(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract h()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract h(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract i()V
.end method

.method public abstract i(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
.end method

.method public abstract j()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract k()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract l()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract m()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract n()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract o()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract p()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract q()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract r()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract s()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract t()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract u()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract v()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract w()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract x()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract y()Lcom/google/android/youtube/core/async/au;
.end method

.method public abstract z()Lcom/google/android/youtube/core/async/au;
.end method
