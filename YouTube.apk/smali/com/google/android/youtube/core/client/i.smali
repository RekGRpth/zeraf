.class public Lcom/google/android/youtube/core/client/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/bo;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/e;

.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:Ljava/util/Random;

.field private final d:Lcom/google/android/youtube/core/client/StatParams;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/StatParams;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    const-string v1, "httpClient cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/client/HttpClient;

    const-string v2, "deviceAuthorizer cannot be null"

    invoke-static {p5, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    const-string v3, "accountManagerWrapper cannot be null"

    invoke-static {p6, v3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/async/a;

    const-string v4, "userAuthorizer cannot be null"

    invoke-static {p7, v4}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/client/i;->a(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->b:Lcom/google/android/youtube/core/async/au;

    const-string v0, "clock cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->a:Lcom/google/android/youtube/core/utils/e;

    const-string v0, "random cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->c:Ljava/util/Random;

    const-string v0, "statParams cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/StatParams;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->d:Lcom/google/android/youtube/core/client/StatParams;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/client/StatParams;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    const-string v1, "httpClient cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/client/HttpClient;

    const-string v2, "deviceAuthorizer cannot be null"

    invoke-static {p5, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    invoke-static {v0, v1, v2, v3, v3}, Lcom/google/android/youtube/core/client/i;->a(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->b:Lcom/google/android/youtube/core/async/au;

    const-string v0, "clock cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->a:Lcom/google/android/youtube/core/utils/e;

    const-string v0, "random cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->c:Ljava/util/Random;

    const-string v0, "statParams cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/StatParams;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/i;->d:Lcom/google/android/youtube/core/client/StatParams;

    return-void
.end method

.method private static a(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;)Lcom/google/android/youtube/core/async/au;
    .locals 3

    new-instance v1, Lcom/google/android/youtube/core/async/an;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/cd;

    sget-object v2, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v2, p2}, Lcom/google/android/youtube/core/converter/http/cd;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;)V

    sget-object v2, Lcom/google/android/youtube/core/converter/http/bn;->b:Lcom/google/android/youtube/core/converter/http/bn;

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/youtube/core/async/an;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)V

    if-eqz p4, :cond_0

    if-eqz p3, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/client/f;

    sget-object v2, Lcom/google/android/youtube/core/async/ap;->b:Lcom/google/android/youtube/core/async/m;

    invoke-direct {v0, v1, v2, p4, p3}, Lcom/google/android/youtube/core/client/f;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/m;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/a;)V

    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;IIZZZLjava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Ljava/util/List;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 17

    new-instance v1, Lcom/google/android/youtube/core/client/aq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/client/i;->a:Lcom/google/android/youtube/core/utils/e;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/client/i;->b:Lcom/google/android/youtube/core/async/au;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/core/client/i;->b()Ljava/lang/String;

    move-result-object v10

    if-eqz p6, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/core/client/i;->b()Ljava/lang/String;

    move-result-object v11

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/core/client/i;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/core/client/i;->d:Lcom/google/android/youtube/core/client/StatParams;

    move-object/from16 v4, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p7

    move-object/from16 v14, p8

    move-object/from16 v16, p9

    invoke-direct/range {v1 .. v16}, Lcom/google/android/youtube/core/client/aq;-><init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/client/StatParams;Ljava/util/List;)V

    return-object v1

    :cond_0
    const/4 v11, 0x0

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    const/16 v0, 0xc

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/google/android/youtube/core/client/i;->c:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/client/bb;

    invoke-direct {v0}, Lcom/google/android/youtube/core/client/bb;-><init>()V

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/core/client/aq;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/i;->a:Lcom/google/android/youtube/core/utils/e;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/i;->b:Lcom/google/android/youtube/core/async/au;

    const-string v3, "state cannot be null"

    invoke-static {p1, v3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    iget-object v4, p0, Lcom/google/android/youtube/core/client/i;->d:Lcom/google/android/youtube/core/client/StatParams;

    iget-object v5, p1, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/core/client/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/client/aq;-><init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/youtube/core/client/StatParams;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;IILjava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 10

    const/4 v5, 0x0

    const-string v0, "videoId cannot be null or empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    const-string v0, "adformat cannot be null or empty"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p5}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/client/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v6, v5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/client/i;->a(Ljava/lang/String;IIZZZLjava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Ljava/util/List;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;IZZZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Lcom/google/android/youtube/core/client/VideoStats2Client;
    .locals 10

    const-string v0, "videoId cannot be null or empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-static/range {p6 .. p6}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/client/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    move-object v0, p0

    move v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/client/i;->a(Ljava/lang/String;IIZZZLjava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Ljava/util/List;)Lcom/google/android/youtube/core/client/VideoStats2Client;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    return-object v0
.end method
