.class public final enum Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum AD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum ARTIST_VIDEOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum BROWSE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum CHANNEL_FAVORITE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum CHANNEL_PLAYLIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum CHANNEL_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum EXTERNAL_URL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_ANIMALS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_AUTOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_COMEDY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_EDUCATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_FILM:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_GAMES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_HOWTO:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_MUSIC:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_NEWS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_PEOPLE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_SCIENCE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_SPORTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_TRAVEL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CATEGORY_TRENDING:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_CHANNEL_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_RIVER_ACTIVTY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_RIVER_ACTIVTY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_RIVER_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum GUIDE_RIVER_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum LIVE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum LIVE_TEASER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum MY_FAVORITES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum MY_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum PLAYLISTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum RELATED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum RELATED_ARTIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum REMOTE_QR_SCAN:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum REMOTE_QUEUE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum SEARCH:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum UPLOAD_NOTIFICATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum WATCH_HISTORY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum WATCH_LATER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum WIDGET:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum YT_REMOTE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public static final enum YT_REMOTE_DIAL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "NO_FEATURE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_RIVER_UPLOADS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_RIVER_ACTIVTY"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_ACTIVTY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_RIVER_ACTIVTY_UPLOAD"

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_ACTIVTY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_RIVER_RECOMMENDED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CHANNEL_UPLOADS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CHANNEL_ACTIVITY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CHANNEL_ACTIVITY_UPLOAD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_TRENDING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRENDING:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_RECOMMENDED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_AUTOS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_AUTOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_COMEDY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_COMEDY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_EDUCATION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_EDUCATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_ENTERTAINMENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_FILM"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_FILM:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_GAMES"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_GAMES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_MUSIC"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_NEWS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_NEWS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_NONPROFIT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_PEOPLE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_PEOPLE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_ANIMALS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_ANIMALS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_SCIENCE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_SCIENCE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_SPORTS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_SPORTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_HOWTO"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_HOWTO:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "GUIDE_CATEGORY_TRAVEL"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRAVEL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "CHANNEL_ACTIVITY"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "CHANNEL_FAVORITE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_FAVORITE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "CHANNEL_PLAYLIST"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_PLAYLIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "CHANNEL_UPLOAD"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "AD"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->AD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "EXTERNAL_URL"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->EXTERNAL_URL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "LIVE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->LIVE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "LIVE_TEASER"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->LIVE_TEASER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "MY_FAVORITES"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_FAVORITES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "MY_UPLOADS"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "PLAYER_EMBEDDED"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "PLAYLISTS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYLISTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "RELATED"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->RELATED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "WATCH_LATER"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_LATER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "WATCH_HISTORY"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_HISTORY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "ARTIST_VIDEOS"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ARTIST_VIDEOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "RELATED_ARTIST"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->RELATED_ARTIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "REMOTE_QR_SCAN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QR_SCAN:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "REMOTE_QUEUE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QUEUE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "UPLOAD_NOTIFICATION"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->UPLOAD_NOTIFICATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "YT_REMOTE"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->YT_REMOTE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "YT_REMOTE_DIAL"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->YT_REMOTE_DIAL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "WIDGET"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WIDGET:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "SEARCH"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->SEARCH:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    new-instance v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-string v1, "BROWSE"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->BROWSE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/16 v0, 0x32

    new-array v0, v0, [Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_ACTIVTY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_ACTIVTY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRENDING:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_AUTOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_COMEDY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_EDUCATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_FILM:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_GAMES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_NEWS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_PEOPLE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_ANIMALS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_SCIENCE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_SPORTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_HOWTO:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRAVEL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_FAVORITE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_PLAYLIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->AD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->EXTERNAL_URL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->LIVE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->LIVE_TEASER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_FAVORITES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYLISTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->RELATED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_LATER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_HISTORY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ARTIST_VIDEOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->RELATED_ARTIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QR_SCAN:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QUEUE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->UPLOAD_NOTIFICATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->YT_REMOTE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->YT_REMOTE_DIAL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WIDGET:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->SEARCH:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->BROWSE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->$VALUES:[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;
    .locals 1

    const-class v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->$VALUES:[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    return-object v0
.end method


# virtual methods
.method public final getFeatureString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/youtube/core/client/bn;->a:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    const-string v0, "related"

    goto :goto_0

    :pswitch_2
    const-string v0, "my_favorites"

    goto :goto_0

    :pswitch_3
    const-string v0, "my_uploads"

    goto :goto_0

    :pswitch_4
    const-string v0, "my_watch_later"

    goto :goto_0

    :pswitch_5
    const-string v0, "my_history"

    goto :goto_0

    :pswitch_6
    const-string v0, "ad"

    goto :goto_0

    :pswitch_7
    const-string v0, "player_embedded"

    goto :goto_0

    :pswitch_8
    const-string v0, "g-pets"

    goto :goto_0

    :pswitch_9
    const-string v0, "g-auto"

    goto :goto_0

    :pswitch_a
    const-string v0, "g-comedy"

    goto :goto_0

    :pswitch_b
    const-string v0, "g-edu"

    goto :goto_0

    :pswitch_c
    const-string v0, "g-ent"

    goto :goto_0

    :pswitch_d
    const-string v0, "g-film"

    goto :goto_0

    :pswitch_e
    const-string v0, "g-games"

    goto :goto_0

    :pswitch_f
    const-string v0, "g-howto"

    goto :goto_0

    :pswitch_10
    const-string v0, "g-music"

    goto :goto_0

    :pswitch_11
    const-string v0, "g-news"

    goto :goto_0

    :pswitch_12
    const-string v0, "g-npo"

    goto :goto_0

    :pswitch_13
    const-string v0, "g-people"

    goto :goto_0

    :pswitch_14
    const-string v0, "g-sports"

    goto :goto_0

    :pswitch_15
    const-string v0, "g-sci"

    goto :goto_0

    :pswitch_16
    const-string v0, "g-travel"

    goto :goto_0

    :pswitch_17
    const-string v0, "g-vrec"

    goto :goto_0

    :pswitch_18
    const-string v0, "g-trend"

    goto :goto_0

    :pswitch_19
    const-string v0, "g-all-lsb"

    goto :goto_0

    :pswitch_1a
    const-string v0, "g-all-lss"

    goto :goto_0

    :pswitch_1b
    const-string v0, "browse"

    goto :goto_0

    :pswitch_1c
    const-string v0, "g-all-u"

    goto :goto_0

    :pswitch_1d
    const-string v0, "g-all-a"

    goto :goto_0

    :pswitch_1e
    const-string v0, "g-user-u"

    goto :goto_0

    :pswitch_1f
    const-string v0, "custom_uri"

    goto :goto_0

    :pswitch_20
    const-string v0, "g-all-au"

    goto :goto_0

    :pswitch_21
    const-string v0, "g-all-r"

    goto :goto_0

    :pswitch_22
    const-string v0, "g-user-a"

    goto :goto_0

    :pswitch_23
    const-string v0, "g-user-au"

    goto :goto_0

    :pswitch_24
    const-string v0, "c-activity"

    goto :goto_0

    :pswitch_25
    const-string v0, "c-favorite"

    goto :goto_0

    :pswitch_26
    const-string v0, "c-playlist"

    goto :goto_0

    :pswitch_27
    const-string v0, "c-upload"

    goto :goto_0

    :pswitch_28
    const-string v0, "search"

    goto :goto_0

    :pswitch_29
    const-string v0, "artist_video"

    goto :goto_0

    :pswitch_2a
    const-string v0, "playlist"

    goto :goto_0

    :pswitch_2b
    const-string v0, "ytremote_d"

    goto/16 :goto_0

    :pswitch_2c
    const-string v0, "ytremote"

    goto/16 :goto_0

    :pswitch_2d
    const-string v0, "related_artist"

    goto/16 :goto_0

    :pswitch_2e
    const-string v0, "remote_qr_scan"

    goto/16 :goto_0

    :pswitch_2f
    const-string v0, "remote_queue"

    goto/16 :goto_0

    :pswitch_30
    const-string v0, "upload_notification"

    goto/16 :goto_0

    :pswitch_31
    const-string v0, "widget"

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
    .end packed-switch
.end method
