.class public final Lcom/google/android/youtube/core/client/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Lcom/google/android/youtube/core/async/a;

.field private final c:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final d:Lcom/google/android/youtube/core/async/m;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/m;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "retryStrategy cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/m;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/f;->d:Lcom/google/android/youtube/core/async/m;

    const-string v0, "userAuthorizer cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/f;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/f;->b:Lcom/google/android/youtube/core/async/a;

    const-string v0, "targetRequester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    invoke-static {v0, p4, p2}, Lcom/google/android/youtube/core/async/k;->a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/m;)Lcom/google/android/youtube/core/async/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/f;->a:Lcom/google/android/youtube/core/async/au;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 3

    check-cast p1, Lcom/google/android/youtube/core/async/ap;

    iget-object v0, p1, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/client/f;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/f;->b:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/async/a;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/f;->d:Lcom/google/android/youtube/core/async/m;

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/m;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/model/UserAuth;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/ap;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/client/f;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v2, Lcom/google/android/youtube/core/client/g;

    invoke-direct {v2, p0, p2, p1}, Lcom/google/android/youtube/core/client/g;-><init>(Lcom/google/android/youtube/core/client/f;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/async/ap;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method
