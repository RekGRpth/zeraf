.class public final Lcom/google/android/youtube/core/async/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/DeviceAuthorizer;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/as;

.field private final b:Landroid/content/SharedPreferences;

.field private volatile c:Z

.field private final d:Landroid/os/ConditionVariable;

.field private volatile e:Lcom/google/android/youtube/core/model/d;

.field private volatile f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/as;Landroid/content/SharedPreferences;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "deviceRegistrationClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/as;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->a:Lcom/google/android/youtube/core/client/as;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->b:Landroid/content/SharedPreferences;

    const-string v0, ""

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/model/d;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lcom/google/android/youtube/core/model/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->d:Landroid/os/ConditionVariable;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/d;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/youtube/core/async/v;->c:Z

    if-nez v2, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/async/v;->c:Z

    iget-object v1, p0, Lcom/google/android/youtube/core/async/v;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->a:Lcom/google/android/youtube/core/client/as;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/client/as;->a(Lcom/google/android/youtube/core/async/n;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/model/d;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    new-instance v0, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;-><init>(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/v;->c:Z

    const-string v0, "device registration failed"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/d;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/v;->f:Lcom/google/android/youtube/core/async/DeviceAuthorizer$DeviceRegistrationException;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->e:Lcom/google/android/youtube/core/model/d;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/v;->b:Landroid/content/SharedPreferences;

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/model/d;->a(Lcom/google/android/youtube/core/model/d;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/v;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/v;->c:Z

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    return-void
.end method
