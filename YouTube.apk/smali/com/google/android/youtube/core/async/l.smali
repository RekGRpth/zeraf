.class final Lcom/google/android/youtube/core/async/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/n;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/google/android/youtube/core/async/k;

.field private d:Z


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/k;Lcom/google/android/youtube/core/async/n;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/l;->a:Lcom/google/android/youtube/core/async/n;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/l;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/l;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/k;->a(Lcom/google/android/youtube/core/async/k;)Lcom/google/android/youtube/core/async/m;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/m;->a(Ljava/lang/Object;Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/l;->d:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/k;->a(Lcom/google/android/youtube/core/async/k;)Lcom/google/android/youtube/core/async/m;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/async/m;->a(Ljava/lang/Object;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v2

    if-eqz v2, :cond_0

    const-class v1, Lcom/google/android/youtube/core/async/k;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/k;->b(Lcom/google/android/youtube/core/async/k;)Lcom/google/android/youtube/core/async/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/a;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/k;->b(Lcom/google/android/youtube/core/async/k;)Lcom/google/android/youtube/core/async/a;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/async/a;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v6

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    iget-object v0, v2, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v1, v6, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v6, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v5, v6, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    iget-object v6, v6, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/k;->a(Lcom/google/android/youtube/core/async/k;)Lcom/google/android/youtube/core/async/m;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/m;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/model/UserAuth;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/l;->c:Lcom/google/android/youtube/core/async/k;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/k;->c(Lcom/google/android/youtube/core/async/k;)Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    invoke-interface {v1, v0, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/l;->a:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/l;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    move-object v0, v6

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/l;->a:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/l;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
