.class public final Lcom/google/android/youtube/core/async/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/bk;

.field private volatile b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/bk;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/async/o;->a:Lcom/google/android/youtube/core/async/bk;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/o;->b:Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/o;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/o;->a:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/async/bk;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/o;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/o;->a:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/o;->b:Z

    return v0
.end method

.method public final g_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/o;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/o;->a:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V

    :cond_0
    return-void
.end method
