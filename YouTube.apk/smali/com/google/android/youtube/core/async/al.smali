.class final Lcom/google/android/youtube/core/async/al;
.super Lcom/google/android/youtube/core/async/ar;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/ar;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z
    .locals 1

    instance-of v0, p2, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "operation needs a full YouTube account"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/async/ar;->a(Lcom/google/android/youtube/core/async/ap;Ljava/lang/Exception;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/model/UserAuth;)Ljava/lang/Object;
    .locals 6

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    const-string v0, "newUserAuth can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v1, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/GDataRequest;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/youtube/core/async/GDataRequest;->e:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/ap;->a(Lcom/google/android/youtube/core/async/ap;)[B

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/util/Map;[BB)V

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/youtube/core/async/ap;Ljava/lang/Exception;)Z
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/async/al;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/async/al;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method
