.class final Lcom/google/android/youtube/core/async/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/a/g;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/a/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/async/a/g;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/async/a/i;-><init>(Lcom/google/android/youtube/core/async/a/g;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;I)I

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/a/g;->c(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    const/4 v4, 0x0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v2}, Lcom/google/android/youtube/core/async/a/g;->b(Lcom/google/android/youtube/core/async/a/g;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/async/a/h;

    invoke-direct {v3, p1, v0}, Lcom/google/android/youtube/core/async/a/h;-><init>(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Video;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/a/g;->b(Lcom/google/android/youtube/core/async/a/g;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/a/g;->d(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/a/g;->e(Lcom/google/android/youtube/core/async/a/g;)V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v2}, Lcom/google/android/youtube/core/async/a/g;->d(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/a/g;->a(Lcom/google/android/youtube/core/async/a/g;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/a/g;->g(Lcom/google/android/youtube/core/async/a/g;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/i;->a:Lcom/google/android/youtube/core/async/a/g;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/a/g;->f(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/a/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2
.end method
