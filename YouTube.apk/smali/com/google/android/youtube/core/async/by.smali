.class public final Lcom/google/android/youtube/core/async/by;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Landroid/net/Uri;


# instance fields
.field protected final a:Lcom/google/android/youtube/core/async/cc;

.field private final c:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final d:Lcom/google/android/youtube/core/client/bc;

.field private final e:Lcom/google/android/youtube/core/client/bh;

.field private final f:Lcom/google/android/youtube/core/client/be;

.field private final g:Lcom/google/android/youtube/core/Analytics;

.field private final h:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://m.youtube.com/create_channel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/async/by;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bh;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/Analytics;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "userAuthorizer can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "gdataClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->d:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "plusClient can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bh;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->e:Lcom/google/android/youtube/core/client/bh;

    const-string v0, "imageClient can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->f:Lcom/google/android/youtube/core/client/be;

    const-string v0, "analytics can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->g:Lcom/google/android/youtube/core/Analytics;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    const-string v0, "accountManager can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/bz;

    invoke-direct {v0, p0, p5}, Lcom/google/android/youtube/core/async/bz;-><init>(Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/core/async/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/by;->a:Lcom/google/android/youtube/core/async/cc;

    return-void
.end method

.method static synthetic a()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/async/by;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/core/async/bk;Lcom/google/android/youtube/core/model/UserAuth;Landroid/app/Activity;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p3, v0}, Landroid/app/Activity;->showDialog(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "WebChannelUpgradeDialogShown"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/core/async/bk;Lcom/google/android/youtube/core/model/UserAuth;Landroid/app/Activity;I)V
    .locals 3

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "userAuth"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "messageId"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v2, 0x1

    invoke-virtual {p3, v2, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "PlusChannelUpgradeDialogShown"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/async/by;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    if-eqz p4, :cond_0

    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/async/cd;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/core/async/cd;-><init>(Lcom/google/android/youtube/core/async/by;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;I)V

    move-object p2, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/by;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/async/by;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/by;->d:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 3

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0046

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0047

    new-instance v2, Lcom/google/android/youtube/core/async/cb;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/core/async/cb;-><init>(Lcom/google/android/youtube/core/async/by;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/async/ca;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/async/ca;-><init>(Lcom/google/android/youtube/core/async/by;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "args cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "userAuth"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/android/youtube/core/model/UserAuth;

    const-string v0, "userAuth missing from args bundle"

    invoke-static {v6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "messageId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/youtube/core/ui/l;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/by;->d:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/by;->f:Lcom/google/android/youtube/core/client/be;

    iget-object v4, p0, Lcom/google/android/youtube/core/async/by;->e:Lcom/google/android/youtube/core/client/bh;

    iget-object v5, p0, Lcom/google/android/youtube/core/async/by;->g:Lcom/google/android/youtube/core/Analytics;

    iget-object v7, p0, Lcom/google/android/youtube/core/async/by;->a:Lcom/google/android/youtube/core/async/cc;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/core/ui/l;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bh;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/cc;)V

    return-object v0
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V
    .locals 2

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f0b0041

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/youtube/core/async/by;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V
    .locals 2

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "messageId cannot be 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/async/by;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
