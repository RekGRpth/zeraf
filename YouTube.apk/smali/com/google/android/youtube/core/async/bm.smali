.class final Lcom/google/android/youtube/core/async/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final b:Lcom/google/android/youtube/core/async/bk;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/bk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bm;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bm;->b:Lcom/google/android/youtube/core/async/bk;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/UserAuthorizer;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bm;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->b(Lcom/google/android/youtube/core/async/UserAuthorizer;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->b:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/async/bk;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/core/async/bm;->b:Lcom/google/android/youtube/core/async/bk;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/core/model/UserAuth;->cloneWithAccountDetails(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/async/bk;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d(Lcom/google/android/youtube/core/async/UserAuthorizer;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c(Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->b:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->a:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c(Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bm;->b:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V

    return-void
.end method
