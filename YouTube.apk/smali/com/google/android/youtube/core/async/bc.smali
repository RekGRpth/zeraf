.class public final Lcom/google/android/youtube/core/async/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private final a:Landroid/os/ConditionVariable;

.field private volatile b:Ljava/lang/String;

.field private volatile c:Lcom/google/android/youtube/core/model/UserAuth;

.field private volatile d:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/os/ConditionVariable;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/UserAuth;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bc;->d:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->b:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/model/UserAuth;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->d:Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bc;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/model/UserAuth;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bc;->d:Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public final g_()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->c:Lcom/google/android/youtube/core/model/UserAuth;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bc;->d:Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bc;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method
