.class final Lcom/google/android/youtube/core/async/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/as;

.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/as;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/at;->a:Lcom/google/android/youtube/core/async/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "targetCallback can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/at;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/at;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/async/at;->a:Lcom/google/android/youtube/core/async/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/as;->b(Lcom/google/android/youtube/core/async/as;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/at;->a:Lcom/google/android/youtube/core/async/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/as;->a(Lcom/google/android/youtube/core/async/as;)Lcom/google/android/youtube/core/utils/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/at;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
