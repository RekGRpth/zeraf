.class final Lcom/google/android/youtube/core/async/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/y;

.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/y;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/ad;->a:Lcom/google/android/youtube/core/async/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "targetCallback can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ad;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ad;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ad;->a:Lcom/google/android/youtube/core/async/y;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/y;->a(Lcom/google/android/youtube/core/async/y;)Lcom/google/android/youtube/core/cache/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ad;->a:Lcom/google/android/youtube/core/async/y;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/youtube/core/async/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/youtube/core/utils/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/cache/a;->a(Lcom/google/android/youtube/core/utils/t;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ad;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
