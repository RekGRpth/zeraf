.class public Lcom/google/android/youtube/core/async/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/t;

.field private final b:Lcom/google/android/youtube/core/cache/a;

.field private final c:Lcom/google/android/youtube/core/async/au;


# direct methods
.method protected constructor <init>(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "cache may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/cache/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/y;->b:Lcom/google/android/youtube/core/cache/a;

    const-string v0, "target may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/y;->c:Lcom/google/android/youtube/core/async/au;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/y;->a:Lcom/google/android/youtube/core/utils/t;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/utils/t;Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "filter may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/t;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/y;->a:Lcom/google/android/youtube/core/utils/t;

    const-string v0, "cache may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/cache/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/y;->b:Lcom/google/android/youtube/core/cache/a;

    const-string v0, "target may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/y;->c:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/y;)Lcom/google/android/youtube/core/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/y;->b:Lcom/google/android/youtube/core/cache/a;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/youtube/core/utils/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/y;->a:Lcom/google/android/youtube/core/utils/t;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/y;->c:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/async/ad;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/core/async/ad;-><init>(Lcom/google/android/youtube/core/async/y;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
