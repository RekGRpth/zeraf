.class final Lcom/google/android/youtube/app/prefetch/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/al;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/model/Video;

.field final synthetic b:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

.field final synthetic c:I

.field final synthetic d:Lcom/google/android/youtube/app/prefetch/PrefetchService;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/prefetch/PrefetchService;Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/prefetch/a;->d:Lcom/google/android/youtube/app/prefetch/PrefetchService;

    iput-object p2, p0, Lcom/google/android/youtube/app/prefetch/a;->a:Lcom/google/android/youtube/core/model/Video;

    iput-object p3, p0, Lcom/google/android/youtube/app/prefetch/a;->b:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    iput p4, p0, Lcom/google/android/youtube/app/prefetch/a;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/ak;)V
    .locals 8

    iget-object v0, p1, Lcom/google/android/youtube/core/model/ak;->b:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/a;->d:Lcom/google/android/youtube/app/prefetch/PrefetchService;

    invoke-static {v1}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Lcom/google/android/youtube/app/prefetch/PrefetchService;)Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/a;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/prefetch/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/a;->d:Lcom/google/android/youtube/app/prefetch/PrefetchService;

    invoke-static {v1}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b(Lcom/google/android/youtube/app/prefetch/PrefetchService;)I

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "dnc"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/a;->d:Lcom/google/android/youtube/app/prefetch/PrefetchService;

    invoke-static {v1}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d(Lcom/google/android/youtube/app/prefetch/PrefetchService;)Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/a;->d:Lcom/google/android/youtube/app/prefetch/PrefetchService;

    invoke-static {v2}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Lcom/google/android/youtube/app/prefetch/PrefetchService;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/prefetch/b;

    iget-object v4, p0, Lcom/google/android/youtube/app/prefetch/a;->d:Lcom/google/android/youtube/app/prefetch/PrefetchService;

    iget-object v5, p0, Lcom/google/android/youtube/app/prefetch/a;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v6, p0, Lcom/google/android/youtube/app/prefetch/a;->b:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    iget v7, p0, Lcom/google/android/youtube/app/prefetch/a;->c:I

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/youtube/app/prefetch/b;-><init>(Lcom/google/android/youtube/app/prefetch/PrefetchService;Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;I)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No streams found for video: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/a;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    return-void
.end method
