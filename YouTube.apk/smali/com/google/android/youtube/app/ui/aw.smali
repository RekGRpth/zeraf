.class final Lcom/google/android/youtube/app/ui/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/client/bg;

.field private c:Lcom/google/android/youtube/core/model/MusicVideo;

.field private final d:Lcom/google/android/youtube/app/ui/ep;

.field private final e:Lcom/google/android/youtube/app/ui/ef;

.field private final f:Lcom/google/android/youtube/app/ui/eg;

.field private final g:Lcom/google/android/youtube/app/ui/eq;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->a:Landroid/app/Activity;

    const-string v0, "musicClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bg;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->b:Lcom/google/android/youtube/core/client/bg;

    const-string v0, "statusListener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ep;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->d:Lcom/google/android/youtube/app/ui/ep;

    const-string v0, "artistInfoOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ef;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->e:Lcom/google/android/youtube/app/ui/ef;

    const-string v0, "artistTracksOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/eg;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->f:Lcom/google/android/youtube/app/ui/eg;

    const-string v0, "relatedArtistsOutline cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/eq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->g:Lcom/google/android/youtube/app/ui/eq;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/aw;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ep;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->d:Lcom/google/android/youtube/app/ui/ep;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/aw;Lcom/google/android/youtube/core/model/MusicVideo;)Lcom/google/android/youtube/core/model/MusicVideo;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/aw;->c:Lcom/google/android/youtube/core/model/MusicVideo;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/aw;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->b:Lcom/google/android/youtube/core/client/bg;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/aw;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/ay;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/ui/ay;-><init>(Lcom/google/android/youtube/app/ui/aw;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/bg;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/aw;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/aw;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->e:Lcom/google/android/youtube/app/ui/ef;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ef;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->f:Lcom/google/android/youtube/app/ui/eg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eg;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->g:Lcom/google/android/youtube/app/ui/eq;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eq;->c(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ef;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->e:Lcom/google/android/youtube/app/ui/ef;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/eg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->f:Lcom/google/android/youtube/app/ui/eg;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/core/model/MusicVideo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->c:Lcom/google/android/youtube/core/model/MusicVideo;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/eq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->g:Lcom/google/android/youtube/app/ui/eq;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->c:Lcom/google/android/youtube/core/model/MusicVideo;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/aw;->a(Z)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->c:Lcom/google/android/youtube/core/model/MusicVideo;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/aw;->a(Z)V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->couldBeMusicVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/aw;->b:Lcom/google/android/youtube/core/client/bg;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/aw;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/ax;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/ui/ax;-><init>(Lcom/google/android/youtube/app/ui/aw;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/client/bg;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aw;->d:Lcom/google/android/youtube/app/ui/ep;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ep;->A()V

    goto :goto_0
.end method
