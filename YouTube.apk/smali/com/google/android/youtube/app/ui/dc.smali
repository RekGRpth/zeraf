.class public final Lcom/google/android/youtube/app/ui/dc;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/transfer/i;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/az;

.field private final g:Lcom/google/android/youtube/app/adapter/cd;

.field private final h:Lcom/google/android/youtube/app/ui/bx;

.field private final i:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final j:Lcom/google/android/youtube/core/client/bc;

.field private final k:Lcom/google/android/youtube/core/Analytics;

.field private l:Lcom/google/android/youtube/core/model/UserAuth;

.field private m:Lcom/google/android/youtube/core/utils/aa;

.field private final n:Lcom/google/android/youtube/app/d;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/app/adapter/cd;Lcom/google/android/youtube/app/ui/bx;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;)V
    .locals 7

    new-instance v1, Lcom/google/android/youtube/app/ui/dd;

    invoke-direct {v1}, Lcom/google/android/youtube/app/ui/dd;-><init>()V

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/async/q;->a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/c;)Lcom/google/android/youtube/core/async/au;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p5

    move-object/from16 v6, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    const-string v1, "gDataClient may not be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->j:Lcom/google/android/youtube/core/client/bc;

    const-string v1, "userAuthorizer may not be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->i:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v1, "navigation cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/d;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->n:Lcom/google/android/youtube/app/d;

    const-string v1, "rowAdapter cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/bx;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->h:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->h:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    const-string v1, "analytics cannot be null"

    invoke-static {p8, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->k:Lcom/google/android/youtube/core/Analytics;

    new-instance v1, Lcom/google/android/youtube/app/ui/az;

    invoke-direct {v1}, Lcom/google/android/youtube/app/ui/az;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->a:Lcom/google/android/youtube/app/ui/az;

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/ui/cz;)Lcom/google/android/youtube/app/ui/cz;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/cd;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/cz;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/cz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dc;Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/core/model/Video;
    .locals 6

    iget-object v0, p2, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "upload_file_duration"

    iget v2, p1, Lcom/google/android/youtube/core/model/Video;->duration:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    sub-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->buildUpon()Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/model/Video$Builder;->duration(I)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Video$Builder;->uploadedDate(Ljava/util/Date;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video$Builder;->build()Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dc;Lcom/google/android/youtube/core/transfer/Transfer;Lcom/google/android/youtube/core/model/Video;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/cz;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/dc;->a(Lcom/google/android/youtube/app/ui/cz;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-static {p2}, Lcom/google/android/youtube/app/ui/cz;->a(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/adapter/cd;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private g(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->l:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/dc;->h(Lcom/google/android/youtube/core/transfer/Transfer;)V

    :cond_0
    return-void
.end method

.method private h(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/cz;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/dc;->a(Lcom/google/android/youtube/app/ui/cz;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/youtube/app/adapter/cd;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    new-instance v1, Lcom/google/android/youtube/app/ui/df;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/app/ui/df;-><init>(Lcom/google/android/youtube/core/transfer/Transfer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cd;->a(Lcom/google/android/youtube/core/utils/t;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/app/adapter/cd;->c(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/core/ui/j;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->i:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method protected final a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->i:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dc;->l:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 4

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->h:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->b:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/de;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lcom/google/android/youtube/app/ui/de;-><init>(Lcom/google/android/youtube/app/ui/dc;Lcom/google/android/youtube/core/transfer/Transfer;B)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dc;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/dc;->l:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/youtube/core/client/bc;->c(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/dc;->g(Lcom/google/android/youtube/core/transfer/Transfer;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->l:Lcom/google/android/youtube/core/model/UserAuth;

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/android/youtube/app/ui/cz;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/cz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->a:Lcom/google/android/youtube/app/ui/az;

    iget-object v0, p1, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/az;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/dc;->g(Lcom/google/android/youtube/core/transfer/Transfer;)V

    return-void
.end method

.method public final c()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/aa;->b(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/youtube/core/transfer/Transfer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/cd;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/dc;->h(Lcom/google/android/youtube/core/transfer/Transfer;)V

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/cz;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cd;->b(Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->b:Landroid/app/Activity;

    const v1, 0x7f0b0202

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final f(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    iget-object v1, p1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final g_()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->l:Lcom/google/android/youtube/core/model/UserAuth;

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/cd;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->g:Lcom/google/android/youtube/app/adapter/cd;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/cd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/cz;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/cz;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->k:Lcom/google/android/youtube/core/Analytics;

    sget-object v2, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Uploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v1, v2, p3}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dc;->n:Lcom/google/android/youtube/app/d;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    return-void
.end method

.method public final t_()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dc;->m:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/l;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/dc;->g(Lcom/google/android/youtube/core/transfer/Transfer;)V

    goto :goto_0

    :cond_0
    return-void
.end method
