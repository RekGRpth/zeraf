.class public final Lcom/google/android/youtube/app/ui/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/remote/ag;
.implements Lcom/google/android/youtube/core/async/n;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final d:Lcom/google/android/youtube/core/e;

.field private final e:Landroid/app/Activity;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/Button;

.field private final h:Landroid/view/View;

.field private i:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private j:Lcom/google/android/youtube/core/async/p;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "max-results"

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/ui/br;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;Landroid/view/View;Lcom/google/android/youtube/core/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->e:Landroid/app/Activity;

    const-string v0, "requester can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->b:Lcom/google/android/youtube/core/async/au;

    const-string v0, "playlistVideosRequest can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->c:Lcom/google/android/youtube/core/async/GDataRequest;

    const-string v0, "playAllLayout can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->f:Landroid/view/View;

    const-string v0, "errorHelper can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->d:Lcom/google/android/youtube/core/e;

    const-string v0, "mediaRouteManger can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f0700fb

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->g:Landroid/widget/Button;

    const v0, 0x7f0700fc

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->h:Landroid/view/View;

    invoke-virtual {p2, p0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ag;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2}, Lcom/google/android/youtube/app/remote/ad;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/ui/br;->a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->g:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eq p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    :cond_1
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/br;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error loading playlist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->d:Lcom/google/android/youtube/core/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/br;->d:Lcom/google/android/youtube/core/e;

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/br;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/Page;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/util/List;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/br;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/youtube/app/ui/br;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/br;->b:Lcom/google/android/youtube/core/async/au;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/br;->c:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/util/List;)V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/br;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    goto :goto_2
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->g:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->e:Landroid/app/Activity;

    invoke-static {v0, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/br;->b:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/br;->c:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/br;->j:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
