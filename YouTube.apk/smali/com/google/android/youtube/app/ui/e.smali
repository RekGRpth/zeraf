.class public final Lcom/google/android/youtube/app/ui/e;
.super Lcom/google/android/youtube/app/adapter/cn;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:I

.field private final c:I

.field private final d:I

.field private e:Lcom/google/android/youtube/app/ui/f;

.field private f:I

.field private final g:Landroid/view/ViewGroup;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/Button;

.field private final k:Landroid/view/View;

.field private final l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    iput v2, p0, Lcom/google/android/youtube/app/ui/e;->a:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/app/ui/e;->c:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/youtube/app/ui/e;->d:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/e;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07005e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/e;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/e;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/e;->h:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/e;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070060

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/e;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/e;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070061

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/e;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070062

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/e;->k:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/e;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/e;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/ui/e;->e(I)V

    return-void
.end method

.method private e(I)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/youtube/app/ui/e;->f:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/youtube/app/ui/e;->f:I

    iget v0, p0, Lcom/google/android/youtube/app/ui/e;->f:I

    if-ne v0, v3, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/e;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/ui/e;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/e;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/e;->f:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/e;->c(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/e;->e:Lcom/google/android/youtube/app/ui/f;

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/e;->e(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/e;->e(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/e;->e(I)V

    return-void
.end method

.method public final e()V
    .locals 2

    const/16 v1, 0x8

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/e;->e(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->e:Lcom/google/android/youtube/app/ui/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->l:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->e:Lcom/google/android/youtube/app/ui/f;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/f;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/e;->e:Lcom/google/android/youtube/app/ui/f;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/f;->h()V

    goto :goto_0
.end method

.method public final p_()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/e;->e(I)V

    return-void
.end method
