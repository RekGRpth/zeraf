.class public abstract Lcom/google/android/youtube/app/ui/av;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/d;

.field private final b:Ljava/lang/String;

.field protected final g:Landroid/app/Activity;

.field protected final h:Lcom/google/android/youtube/core/Analytics;

.field protected final i:Lcom/google/android/youtube/app/compat/r;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/av;->a:Lcom/google/android/youtube/app/d;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/av;->h:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/av;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/av;->i:Lcom/google/android/youtube/app/compat/r;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->i:Lcom/google/android/youtube/app/compat/r;

    const v1, 0x7f110002

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 5

    const/4 v1, 0x1

    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->f()I

    move-result v0

    const v2, 0x7f070189

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->a:Lcom/google/android/youtube/app/d;

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->c()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f070185

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Search"

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/av;->d()V

    move v0, v1

    goto :goto_0

    :cond_1
    const v2, 0x7f070180

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "UploadFromMenu"

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const v2, 0x7f070186

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->a:Lcom/google/android/youtube/app/d;

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->i()V

    move v0, v1

    goto :goto_0

    :cond_3
    const v2, 0x7f070188

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    const v2, 0x7f0b021f

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/av;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    const v2, 0x7f0b021e

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const v2, 0x7f070187

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    const/16 v2, 0x3f1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->showDialog(I)V

    :goto_2
    move v0, v1

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/av;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v0

    const v4, 0x7f0b0220

    invoke-static {v3, v0, v4}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected abstract d()V
.end method
