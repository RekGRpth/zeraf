.class public final enum Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field public static final enum LOCAL:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field public static final enum MOST_SUBSCRIBED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field public static final enum MOST_VIEWED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field public static final enum NOTEWORTHY:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field public static final enum RECOMMENDED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;


# instance fields
.field public final position:I

.field public final stringId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const-string v1, "RECOMMENDED"

    const v2, 0x7f0b0268

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->RECOMMENDED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    new-instance v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const-string v1, "MOST_SUBSCRIBED"

    const v2, 0x7f0b0269

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->MOST_SUBSCRIBED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    new-instance v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const-string v1, "MOST_VIEWED"

    const v2, 0x7f0b026a

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->MOST_VIEWED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    new-instance v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const-string v1, "LOCAL"

    const v2, 0x7f0b026b

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->LOCAL:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    new-instance v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const-string v1, "NOTEWORTHY"

    const v2, 0x7f0b026c

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->NOTEWORTHY:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->RECOMMENDED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->MOST_SUBSCRIBED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->MOST_VIEWED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->LOCAL:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->NOTEWORTHY:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->$VALUES:[Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->position:I

    iput p4, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->stringId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;
    .locals 1

    const-class v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->$VALUES:[Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {v0}, [Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    return-object v0
.end method


# virtual methods
.method public final toString(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->stringId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
