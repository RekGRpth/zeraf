.class public final Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/google/android/youtube/app/ui/DefaultSlider;

.field private d:Landroid/view/VelocityTracker;

.field private e:F

.field private f:F

.field private g:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/DefaultSlider;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/DefaultSlider;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->c:Lcom/google/android/youtube/app/ui/DefaultSlider;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b:I

    return-void
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->c:Lcom/google/android/youtube/app/ui/DefaultSlider;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b()Lcom/google/android/youtube/app/ui/Slider$Orientation;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->e:F

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_0
.end method

.method public final c(Landroid/view/MotionEvent;)I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    :goto_0
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    sub-float/2addr v1, v0

    float-to-int v1, v1

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    return v1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto :goto_0
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 3

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    :goto_0
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a:I

    if-le v1, v2, :cond_1

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e(Landroid/view/MotionEvent;)Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;
    .locals 5

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    iget v4, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b:I

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    iget v3, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    invoke-virtual {v1, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v1

    float-to-int v1, v1

    :goto_1
    iget v3, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->e:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v3, 0x41a00000

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    move v0, v2

    :goto_2
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3

    const/16 v3, 0xc8

    if-le v1, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->c:Lcom/google/android/youtube/app/ui/DefaultSlider;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v3

    if-ne v3, v2, :cond_3

    sget-object v0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;->BACK:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;

    :goto_3
    return-object v0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    iget v3, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    invoke-virtual {v1, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v1

    float-to-int v1, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    if-eqz v0, :cond_4

    const/16 v0, -0xc8

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->c:Lcom/google/android/youtube/app/ui/DefaultSlider;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f()I

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;->FORWARD:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;

    goto :goto_3

    :cond_4
    sget-object v0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;->NONE:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;

    goto :goto_3
.end method

.method public final f(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    if-ne v1, v2, :cond_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    :goto_1
    iput v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f:F

    iput v1, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->e:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->g:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    goto :goto_1
.end method
