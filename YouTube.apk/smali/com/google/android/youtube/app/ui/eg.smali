.class public final Lcom/google/android/youtube/app/ui/eg;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/f;


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/bm;

.field private final c:Lcom/google/android/youtube/app/adapter/ac;

.field private final d:Lcom/google/android/youtube/app/adapter/az;

.field private final e:Lcom/google/android/youtube/app/ui/e;

.field private final f:Lcom/google/android/youtube/core/a/l;

.field private final g:Landroid/app/Activity;

.field private final h:Lcom/google/android/youtube/core/async/au;

.field private final i:Ljava/util/LinkedHashMap;

.field private final j:Lcom/google/android/youtube/core/async/n;

.field private final k:Lcom/google/android/youtube/app/adapter/cg;

.field private l:Lcom/google/android/youtube/core/async/p;

.field private final m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/adapter/az;Lcom/google/android/youtube/app/adapter/cg;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/e;I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    aput-object p5, v0, v3

    const/4 v1, 0x1

    aput-object p6, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2, p8}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "headingOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ac;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->c:Lcom/google/android/youtube/app/adapter/ac;

    const-string v0, "pagingAdapter cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/az;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->d:Lcom/google/android/youtube/app/adapter/az;

    const-string v0, "videoListOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cg;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/app/adapter/cg;

    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/az;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bm;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->a:Lcom/google/android/youtube/app/adapter/bm;

    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/e;->a(Lcom/google/android/youtube/app/ui/f;)V

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->f:Lcom/google/android/youtube/core/a/l;

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    const-string v0, "gdataClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Lcom/google/android/youtube/core/client/bc;->A()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->h:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/LinkedHashMap;

    new-instance v0, Lcom/google/android/youtube/app/ui/ei;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/ui/ei;-><init>(Lcom/google/android/youtube/app/ui/eg;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->j:Lcom/google/android/youtube/core/async/n;

    const v0, 0x7f0b010e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->m:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eg;
    .locals 14

    const v10, 0x7f040027

    const v11, 0x7f040078

    const v12, 0x7f04001a

    const v13, 0x7f0400b4

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v0 .. v13}, Lcom/google/android/youtube/app/ui/eg;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIII)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIII)Lcom/google/android/youtube/app/ui/eg;
    .locals 14

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0013

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080084

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01e9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v3, Lcom/google/android/youtube/app/adapter/a;

    invoke-direct {v3, p0, p1}, Lcom/google/android/youtube/app/adapter/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;)V

    new-instance v6, Lcom/google/android/youtube/app/adapter/bm;

    move/from16 v0, p11

    invoke-direct {v6, p0, v0, v3}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    new-instance v13, Lcom/google/android/youtube/app/adapter/az;

    move/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-direct {v13, v6, v0, v1, v2}, Lcom/google/android/youtube/app/adapter/az;-><init>(Landroid/widget/BaseAdapter;III)V

    new-instance v4, Lcom/google/android/youtube/core/a/c;

    const/4 v3, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/a/g;

    invoke-direct {v4, v13, v3, v5}, Lcom/google/android/youtube/core/a/c;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/cg;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    new-instance v9, Lcom/google/android/youtube/app/ui/eh;

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-direct {v9, v6, v0, v1}, Lcom/google/android/youtube/app/ui/eh;-><init>(Lcom/google/android/youtube/app/adapter/bm;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;)V

    const/4 v10, 0x0

    move-object/from16 v6, p9

    invoke-direct/range {v3 .. v10}, Lcom/google/android/youtube/app/adapter/cg;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;)V

    new-instance v11, Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, p12

    invoke-direct {v11, v4, v0, v5, v12}, Lcom/google/android/youtube/app/ui/e;-><init>(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v10, Lcom/google/android/youtube/core/a/l;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/youtube/core/a/e;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v5, 0x1

    aput-object v11, v4, v5

    invoke-direct {v10, v4}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v9, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v0, p8

    move/from16 v1, p10

    invoke-direct {v9, p0, v0, v1, v10}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;ILcom/google/android/youtube/core/a/e;)V

    new-instance v4, Lcom/google/android/youtube/app/ui/eg;

    move-object v5, p0

    move-object/from16 v6, p2

    move-object v7, v13

    move-object v8, v3

    move/from16 v12, p13

    invoke-direct/range {v4 .. v12}, Lcom/google/android/youtube/app/ui/eg;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/adapter/az;Lcom/google/android/youtube/app/adapter/cg;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/e;I)V

    return-object v4
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/eg;)Ljava/util/LinkedHashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->a:Lcom/google/android/youtube/app/adapter/bm;

    return-object v0
.end method

.method public static b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eg;
    .locals 14

    const v10, 0x7f040026

    const v11, 0x7f040012

    const v12, 0x7f040019

    const v13, 0x7f0400b3

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v0 .. v13}, Lcom/google/android/youtube/app/ui/eg;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIII)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/ui/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/eg;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->o:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->l:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->l:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->l:Lcom/google/android/youtube/core/async/p;

    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->j:Lcom/google/android/youtube/core/async/n;

    invoke-static {v1}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->l:Lcom/google/android/youtube/core/async/p;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->h:Lcom/google/android/youtube/core/async/au;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eg;->l:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/eg;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eg;->n:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->a:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->m:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/e;->a(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/MusicVideo;->trackName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eg;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eg;->i:Ljava/util/LinkedHashMap;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/MusicVideo;->videoId:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->e()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/eg;->d()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->f:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    return-void
.end method

.method public final b()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->k:Lcom/google/android/youtube/app/adapter/cg;

    const v2, 0x7f08009d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v3, 0x7f08009f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    const v4, 0x7f08009e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    const v5, 0x7f0800a0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/youtube/app/adapter/cg;->a(IIII)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eg;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->c:Lcom/google/android/youtube/app/adapter/ac;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eg;->g:Landroid/app/Activity;

    const v2, 0x7f0b01cd

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ac;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->d:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->c()Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->d:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/e;->c(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/eg;->k()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eg;->e:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->e()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/eg;->d()V

    return-void
.end method
