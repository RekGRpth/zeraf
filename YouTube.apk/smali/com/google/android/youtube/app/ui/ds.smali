.class final Lcom/google/android/youtube/app/ui/ds;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/dh;

.field final synthetic b:Lcom/google/android/youtube/app/ui/dr;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/dr;Lcom/google/android/youtube/app/ui/dh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ds;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dr;->a(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/app/adapter/be;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/be;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    if-nez v0, :cond_3

    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->h(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "WatchLater"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dz;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/dr;->b(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v5, v5, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v5}, Lcom/google/android/youtube/app/ui/dh;->e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v6}, Lcom/google/android/youtube/app/ui/dr;->c(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/e;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/app/ui/dz;-><init>(Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->k(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dk;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/dr;->b(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/app/ui/dk;-><init>(Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/model/Video;)V

    const v3, 0x7f0b01bf

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/dh;->j(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/app/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->e()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->k(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dt;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/dr;->d(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/e;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/ui/dt;-><init>(Lcom/google/android/youtube/app/ui/ds;Lcom/google/android/youtube/core/e;)V

    const v3, 0x7f0b01be

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/dh;->j(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/app/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->d()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->h(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "SaveToPlaylist"

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v2, v2, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v2, v2, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/dm;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ds;->b:Lcom/google/android/youtube/app/ui/dr;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {v3, v4, v0}, Lcom/google/android/youtube/app/ui/dm;-><init>(Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/model/Playlist;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    goto/16 :goto_0
.end method
