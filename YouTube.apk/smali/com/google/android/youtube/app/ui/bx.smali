.class public final Lcom/google/android/youtube/app/ui/bx;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/content/res/Resources;

.field private final d:Lcom/google/android/youtube/core/a/a;

.field private e:Landroid/widget/AdapterView$OnItemClickListener;

.field private f:I

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private final n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    iput v3, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->g:F

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->a:Landroid/content/Context;

    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010020

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->n:I

    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Lcom/google/android/youtube/app/ui/bx;B)V

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/a/a;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;
    .locals 5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    new-instance v0, Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/ui/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    const v1, 0x7f080065

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->b(I)V

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->c(I)V

    const v1, 0x7f080067

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->d(I)V

    const v1, 0x7f080061

    const v2, 0x7f080063

    const v3, 0x7f080062

    const v4, 0x7f080064

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/bx;->a(IIII)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "numColumns must be > 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/bx;->notifyDataSetChanged()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIII)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->h:F

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->g:F

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->i:F

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->j:F

    return-void
.end method

.method public final a(ILjava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/a/a;->a(ILjava/lang/Iterable;)V

    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bx;->e:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/util/Collection;)V

    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    return v0
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    return-void
.end method

.method public final b(Ljava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->b(Ljava/lang/Iterable;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->c()I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->l:F

    return-void
.end method

.method public final c(ILjava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/a/a;->c(ILjava/lang/Object;)V

    return-void
.end method

.method public final c(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/google/android/youtube/core/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    return-object v0
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bx;->k:F

    return-void
.end method

.method public final getCount()I
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/high16 v10, 0x40000000

    const/4 v9, 0x1

    const/4 v8, -0x2

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-nez p2, :cond_2

    new-instance p2, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v8}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v3, v4

    :goto_0
    if-nez p1, :cond_3

    iget v0, p0, Lcom/google/android/youtube/app/ui/bx;->g:F

    iget v2, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/bx;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_4

    iget v2, p0, Lcom/google/android/youtube/app/ui/bx;->j:F

    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    :goto_2
    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->h:F

    iget v6, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget v6, p0, Lcom/google/android/youtube/app/ui/bx;->i:F

    iget v7, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-virtual {p2, v5, v0, v6, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    if-eqz v3, :cond_0

    array-length v0, v3

    iget v2, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    if-eq v0, v2, :cond_b

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    new-array v0, v0, [Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    iget v2, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    int-to-float v2, v2

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move-object v2, v0

    :goto_3
    move v3, v1

    :goto_4
    iget v0, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    if-ge v3, v0, :cond_6

    aget-object v0, v2, v3

    if-nez v0, :cond_5

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/bx;->a:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    aput-object v0, v2, v3

    aget-object v0, v2, v3

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setClickable(Z)V

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->n:I

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v1, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v6, 0x3f800000

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    if-lez v3, :cond_1

    iget v6, p0, Lcom/google/android/youtube/app/ui/bx;->l:F

    iget v7, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    mul-float/2addr v7, v10

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    :cond_1
    invoke-virtual {p2, v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_5
    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v3

    const v6, 0x7f070027

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v6, v5}, Landroid/widget/FrameLayout;->setTag(ILjava/lang/Object;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_2
    check-cast p2, Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/widget/FrameLayout;

    move-object v3, v0

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto/16 :goto_1

    :cond_4
    iget v2, p0, Lcom/google/android/youtube/app/ui/bx;->k:F

    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->m:F

    mul-float/2addr v5, v10

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    goto/16 :goto_2

    :cond_5
    aget-object v0, v2, v3

    goto :goto_5

    :cond_6
    move v3, v1

    :goto_6
    iget v0, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    if-ge v3, v0, :cond_a

    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v5, p0, Lcom/google/android/youtube/app/ui/bx;->f:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v3

    iget-object v6, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v6}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_9

    iget-object v6, p0, Lcom/google/android/youtube/app/ui/bx;->d:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v6, v5, v0, p2}, Lcom/google/android/youtube/core/a/a;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    if-eq v0, v5, :cond_8

    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_7

    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_7
    aget-object v0, v2, v3

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    aget-object v0, v2, v3

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v0, v2, v3

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setClickable(Z)V

    aget-object v0, v2, v3

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v7, -0x1

    invoke-direct {v6, v7, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_8
    :goto_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    :cond_9
    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    aget-object v0, v2, v3

    const v5, 0x7f070027

    invoke-virtual {v0, v5, v4}, Landroid/widget/FrameLayout;->setTag(ILjava/lang/Object;)V

    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    aget-object v0, v2, v3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_7

    :cond_a
    return-object p2

    :cond_b
    move-object v2, v3

    goto/16 :goto_3
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->e:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    const v0, 0x7f070027

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bx;->e:Landroid/widget/AdapterView$OnItemClickListener;

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/bx;->getItemId(I)J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method
