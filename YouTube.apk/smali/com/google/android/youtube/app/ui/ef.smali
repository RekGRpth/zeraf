.class public final Lcom/google/android/youtube/app/ui/ef;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/cn;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/adapter/cn;I)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2, p5}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cn;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->a:Lcom/google/android/youtube/app/adapter/cn;

    const-string v0, "artistInfoView cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f070054

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->c:Landroid/widget/TextView;

    const v0, 0x7f070055

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->d:Landroid/widget/TextView;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ef;
    .locals 3

    const v0, 0x7f040027

    const v1, 0x7f040011

    const v2, 0x7f0400b4

    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/youtube/app/ui/ef;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;III)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;III)Lcom/google/android/youtube/app/ui/ef;
    .locals 7

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    new-instance v5, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v5, v6}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/ac;

    const v1, 0x7f0b01cc

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V

    new-instance v1, Lcom/google/android/youtube/app/ui/ef;

    move-object v2, p0

    move-object v3, v6

    move-object v4, v0

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/app/ui/ef;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/adapter/cn;I)V

    return-object v1
.end method

.method public static b(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ef;
    .locals 3

    const v0, 0x7f040026

    const v1, 0x7f040010

    const v2, 0x7f0400b3

    invoke-static {p0, p1, v0, v1, v2}, Lcom/google/android/youtube/app/ui/ef;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;III)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->a:Lcom/google/android/youtube/app/adapter/cn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->d:Landroid/widget/TextView;

    const v1, 0x7f0b01d0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ef;->d:Landroid/widget/TextView;

    const-string v1, "[\\r\\n]+"

    const-string v2, "\r\n\r\n"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
