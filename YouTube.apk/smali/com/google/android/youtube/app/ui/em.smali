.class final Lcom/google/android/youtube/app/ui/em;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/ek;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/ek;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/em;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/ek;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/em;-><init>(Lcom/google/android/youtube/app/ui/ek;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error retrieving user thumbnail"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/em;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->f(Lcom/google/android/youtube/app/ui/ek;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/em;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->g(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
