.class final Lcom/google/android/youtube/app/ui/bl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bh;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->k(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteQueueRemoveVideoWatchPage"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->k(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteQueueAddVideo"

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
