.class public final Lcom/google/android/youtube/app/ui/ej;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/adapter/ay;
.implements Lcom/google/android/youtube/app/ui/dn;
.implements Lcom/google/android/youtube/app/ui/f;


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/cn;

.field private final c:Lcom/google/android/youtube/app/adapter/aw;

.field private final d:Lcom/google/android/youtube/app/ui/e;

.field private final e:Lcom/google/android/youtube/core/a/l;

.field private final f:Landroid/app/Activity;

.field private final g:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final h:Lcom/google/android/youtube/app/ui/dh;

.field private final i:Landroid/widget/EditText;

.field private final j:I

.field private final k:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/ui/dh;ILcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/aw;Lcom/google/android/youtube/app/ui/e;I)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p6, v0, v1

    const/4 v1, 0x1

    aput-object p7, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2, p10}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "addCommentOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cn;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Lcom/google/android/youtube/app/adapter/cn;

    const-string v0, "pagedOutline cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/aw;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/adapter/aw;->a(Lcom/google/android/youtube/app/adapter/ay;)V

    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p9, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/e;->a(Lcom/google/android/youtube/app/ui/f;)V

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->e:Lcom/google/android/youtube/core/a/l;

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->f:Landroid/app/Activity;

    invoke-interface {p2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    const-string v0, "videoActionsHelper cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dh;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->h:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->h:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dn;)V

    invoke-virtual {p5}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput p4, p0, Lcom/google/android/youtube/app/ui/ej;->j:I

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->k:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ej;
    .locals 14

    const v9, 0x7f040027

    const v10, 0x7f04000a

    const v11, 0x7f040029

    const v12, 0x7f04001a

    const v13, 0x7f0400b4

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-static/range {v0 .. v13}, Lcom/google/android/youtube/app/ui/ej;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIIII)Lcom/google/android/youtube/app/ui/ej;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIIII)Lcom/google/android/youtube/app/ui/ej;
    .locals 20

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    move/from16 v0, p10

    invoke-virtual {v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v14, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v14, v2}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/aw;

    new-instance v5, Lcom/google/android/youtube/app/adapter/ad;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/youtube/app/adapter/ad;-><init>(Landroid/content/Context;)V

    invoke-interface/range {p1 .. p1}, Lcom/google/android/youtube/core/client/bc;->x()Lcom/google/android/youtube/core/async/au;

    move-result-object v6

    move-object/from16 v3, p0

    move/from16 v4, p11

    move-object/from16 v7, p2

    move/from16 v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v2 .. v11}, Lcom/google/android/youtube/app/adapter/aw;-><init>(Landroid/app/Activity;ILcom/google/android/youtube/app/adapter/bv;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;IIILcom/google/android/youtube/core/a/g;)V

    new-instance v18, Lcom/google/android/youtube/app/ui/e;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v18

    move/from16 v1, p12

    invoke-direct {v0, v3, v1, v4, v13}, Lcom/google/android/youtube/app/ui/e;-><init>(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v8, Lcom/google/android/youtube/core/a/l;

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/youtube/core/a/e;

    const/4 v4, 0x0

    aput-object v14, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    const/4 v4, 0x2

    aput-object v18, v3, v4

    invoke-direct {v8, v3}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v4, p0

    move-object/from16 v5, p8

    move-object v6, v12

    move/from16 v7, p9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V

    new-instance v9, Lcom/google/android/youtube/app/ui/ej;

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p3

    move/from16 v13, p4

    move-object v15, v3

    move-object/from16 v16, v8

    move-object/from16 v17, v2

    move/from16 v19, p13

    invoke-direct/range {v9 .. v19}, Lcom/google/android/youtube/app/ui/ej;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/ui/dh;ILcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/aw;Lcom/google/android/youtube/app/ui/e;I)V

    return-object v9
.end method

.method public static b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ej;
    .locals 14

    const v9, 0x7f040026

    const v10, 0x7f040009

    const v11, 0x7f040028

    const v12, 0x7f040019

    const v13, 0x7f0400b3

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-static/range {v0 .. v13}, Lcom/google/android/youtube/app/ui/ej;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIIII)Lcom/google/android/youtube/app/ui/ej;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Comment;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/adapter/aw;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Landroid/widget/EditText;

    const v1, 0x7f0b01f5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Landroid/widget/EditText;

    const v1, 0x7f0b01f5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    new-array v1, v2, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ej;->g:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    iget v4, p0, Lcom/google/android/youtube/app/ui/ej;->j:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ej;->f:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0112

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/google/android/youtube/app/ui/e;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->a:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->f_()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/e;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->e:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Landroid/widget/EditText;

    const v1, 0x7f0b01f6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->e()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->d()V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ej;->k:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/e;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->p_()V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->e()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->c:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->d()V

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->i:Landroid/widget/EditText;

    const v1, 0x7f0b01f5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/ej;->a(Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->h:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dh;->g()V

    return-void
.end method

.method public final u_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ej;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->d()V

    return-void
.end method
