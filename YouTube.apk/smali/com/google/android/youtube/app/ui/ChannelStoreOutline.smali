.class public final Lcom/google/android/youtube/app/ui/ChannelStoreOutline;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/youtube/core/a/g;


# instance fields
.field private final c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "ChannelStoreList"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a:Lcom/google/android/youtube/core/a/g;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 2

    const-string v0, "outlines cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/youtube/core/a/e;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/a/e;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "contentOutlines cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/cw;Landroid/view/View;)Lcom/google/android/youtube/app/ui/ChannelStoreOutline;
    .locals 25

    invoke-static {}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->values()[Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    move-result-object v14

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    const v3, 0x7f080049

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/content/Context;I)Lcom/google/android/youtube/core/a/e;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v0, v14

    move/from16 v17, v0

    const/4 v3, 0x0

    move v13, v3

    :goto_0
    move/from16 v0, v17

    if-ge v13, v0, :cond_1

    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->position:I

    if-eqz v3, :cond_0

    const v3, 0x7f08004c

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/content/Context;I)Lcom/google/android/youtube/core/a/e;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/ui/l;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Lcom/google/android/youtube/app/d;)Lcom/google/android/youtube/app/ui/l;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v19, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a:Lcom/google/android/youtube/core/a/g;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v3, 0x7f0a0008

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v21

    const v3, 0x7f08004b

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    const v3, 0x7f080048

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v3, 0x7f080047

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p0

    invoke-static/range {v3 .. v12}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/cw;Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/ab;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/a/c;

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/google/android/youtube/core/a/g;

    invoke-direct {v4, v3, v5, v6}, Lcom/google/android/youtube/core/a/c;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/cg;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    new-instance v9, Lcom/google/android/youtube/app/ui/o;

    invoke-direct {v9}, Lcom/google/android/youtube/app/ui/o;-><init>()V

    const/4 v10, 0x0

    move-object/from16 v6, v19

    move/from16 v7, v21

    move/from16 v8, v22

    invoke-direct/range {v3 .. v10}, Lcom/google/android/youtube/app/adapter/cg;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;)V

    const/4 v5, 0x0

    move/from16 v0, v24

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v3, v0, v5, v1, v2}, Lcom/google/android/youtube/app/adapter/cg;->a(IIII)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f040024

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    new-instance v10, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v10, v5}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v9, Lcom/google/android/youtube/core/a/l;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/youtube/core/a/e;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v10, v5, v3

    invoke-direct {v9, v5}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v5, Lcom/google/android/youtube/app/ui/n;

    move-object/from16 v6, p0

    move-object/from16 v7, p3

    move-object v8, v4

    move-object/from16 v11, v20

    move-object/from16 v12, v18

    invoke-direct/range {v5 .. v12}, Lcom/google/android/youtube/app/ui/n;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/a/c;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/cn;Landroid/content/res/Resources;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V

    invoke-interface {v15, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v13, 0x1

    move v13, v3

    goto/16 :goto_0

    :cond_1
    const v3, 0x7f08004a

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/content/Context;I)Lcom/google/android/youtube/core/a/e;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    move-object/from16 v0, v16

    invoke-direct {v3, v15, v0}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v3
.end method

.method private static a(Landroid/content/Context;I)Lcom/google/android/youtube/core/a/e;
    .locals 4

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/a/m;->a(Landroid/view/View;Z)Lcom/google/android/youtube/core/a/m;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/n;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/n;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/n;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/n;->a(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/n;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/n;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/n;->q_()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/n;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/n;->b(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    return-void
.end method
