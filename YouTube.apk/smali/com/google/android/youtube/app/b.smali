.class final Lcom/google/android/youtube/app/b;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Lcom/google/android/youtube/app/a;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/a;Landroid/content/SharedPreferences;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/b;->b:Lcom/google/android/youtube/app/a;

    iput-object p2, p0, Lcom/google/android/youtube/app/b;->a:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const-string v0, "com.google.android.youtube.action.set_flag"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "flag_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "flag_value"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/b;->a:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/b;->b:Lcom/google/android/youtube/app/a;

    invoke-static {v3, v0}, Lcom/google/android/youtube/app/a;->a(Lcom/google/android/youtube/app/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    iget-object v2, p0, Lcom/google/android/youtube/app/b;->b:Lcom/google/android/youtube/app/a;

    invoke-static {v2}, Lcom/google/android/youtube/app/a;->a(Lcom/google/android/youtube/app/a;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
