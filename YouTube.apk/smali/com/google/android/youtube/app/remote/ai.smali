.class final Lcom/google/android/youtube/app/remote/ai;
.super Lcom/android/athome/picker/media/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/ad;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-direct {p0}, Lcom/android/athome/picker/media/d;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/ad;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/ai;-><init>(Lcom/google/android/youtube/app/remote/ad;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->d(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->h()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/ad;->f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v3, 0x1388

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    add-int/2addr v0, p2

    const/16 v1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/remote/bf;->a(I)V

    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/Object;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->d(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bf;->h()I

    move-result v1

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ai;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/remote/bf;->d(I)V

    :cond_1
    return-void
.end method
