.class public final Lcom/google/android/youtube/app/remote/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/bb;


# instance fields
.field private final a:Lcom/google/android/ytremote/model/d;

.field private final b:Lcom/google/android/ytremote/model/CloudScreen;


# direct methods
.method public constructor <init>(Lcom/google/android/ytremote/model/CloudScreen;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "screen can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/ytremote/model/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "device can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/d;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/ytremote/model/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    return-object v0
.end method

.method public final c()Lcom/google/android/ytremote/model/CloudScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lcom/google/android/youtube/app/remote/bp;

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/youtube/app/remote/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    iget-object v3, p1, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v3, p1, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/d;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/CloudScreen;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "YouTubeTvScreen [device="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bp;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cloudScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bp;->b:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
