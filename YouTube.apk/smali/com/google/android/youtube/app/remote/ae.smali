.class final Lcom/google/android/youtube/app/remote/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/ad;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/ae;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/ad;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/ae;-><init>(Lcom/google/android/youtube/app/remote/ad;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    const/4 v1, -0x1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_1

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ae;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->d(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ae;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v2

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ae;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ae;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->h()I

    move-result v0

    :goto_0
    const-string v2, "RemoteDialogVolumeAdjust"

    sub-int v3, v0, v3

    if-eq v0, v1, :cond_2

    if-lez v3, :cond_0

    const-string v0, "RemoteDialogVolumeUp"

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ae;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->g(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    if-gez v3, :cond_2

    const-string v0, "RemoteDialogVolumeDown"

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method
