.class public final Lcom/google/android/youtube/app/remote/AtHomeConnection;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/android/athome/picker/b/a;

.field private final c:Landroid/os/Handler;

.field private final d:Landroid/support/place/api/broker/BrokerManager;

.field private e:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

.field private final f:Lcom/google/android/youtube/app/remote/b;

.field private final g:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->c:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/app/remote/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/b;-><init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->f:Lcom/google/android/youtube/app/remote/b;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    sget-object v0, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->DISCONNECTED:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->e:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    invoke-static {p1}, Landroid/support/place/api/broker/BrokerManager;->getInstance(Landroid/content/Context;)Landroid/support/place/api/broker/BrokerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    :goto_0
    invoke-static {p1}, Lcom/android/athome/picker/b/a;->a(Landroid/content/Context;)Lcom/android/athome/picker/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b:Lcom/android/athome/picker/b/a;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b:Lcom/android/athome/picker/b/a;

    const-string v1, "com.google.android.youtube.athome.common.AtHomeVideoService"

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b:Lcom/android/athome/picker/b/a;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/AtHomeConnection;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->c()V

    return-void
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->DISCONNECTED:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->e:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    if-eq v1, v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AtHomeConnection state changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->e:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->c:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/app/remote/a;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/remote/a;-><init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->CONNECTING_TO_BROKER:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getPlaces()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->LOOKING_FOR_PLACES:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->PLACES_AVAILABLE:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->e:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/m;
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b:Lcom/android/athome/picker/b/a;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;)Lcom/android/athome/picker/b/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/athome/picker/b/v;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    const-string v2, "com.google.android.youtube.athome.common.AtHomeVideoService"

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/android/athome/picker/media/q;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/android/athome/picker/media/q;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v2, Lcom/google/android/youtube/app/remote/m;

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/app/remote/m;-><init>(Landroid/support/place/connector/ConnectorInfo;Ljava/lang/String;)V

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a:Landroid/content/Context;

    const v2, 0x7f0b0259

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/youtube/app/remote/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->f:Lcom/google/android/youtube/app/remote/b;

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->startListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b:Lcom/android/athome/picker/b/a;

    invoke-virtual {v0}, Lcom/android/athome/picker/b/a;->a()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->c()V

    goto :goto_0
.end method

.method final b()Landroid/support/place/connector/Broker;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    invoke-virtual {v0}, Landroid/support/place/api/broker/BrokerManager;->getBroker()Landroid/support/place/connector/Broker;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/youtube/app/remote/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->d:Landroid/support/place/api/broker/BrokerManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->f:Lcom/google/android/youtube/app/remote/b;

    invoke-virtual {v0, v1}, Landroid/support/place/api/broker/BrokerManager;->stopListening(Landroid/support/place/api/broker/BrokerManager$ConnectionListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b:Lcom/android/athome/picker/b/a;

    invoke-virtual {v0}, Lcom/android/athome/picker/b/a;->b()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->c()V

    goto :goto_0
.end method
