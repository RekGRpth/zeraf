.class final Lcom/google/android/youtube/app/remote/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/bd;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/ad;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/ad;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/aj;-><init>(Lcom/google/android/youtube/app/remote/ad;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/youtube/app/remote/bb;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/app/remote/bp;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "YouTubeTvScreen removed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ad;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ad;Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/remote/bp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/youtube/app/remote/ad;->a:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->c(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ad;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ad;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public final synthetic b(Lcom/google/android/youtube/app/remote/bb;)V
    .locals 1

    check-cast p1, Lcom/google/android/youtube/app/remote/bp;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aj;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method
