.class final Lcom/google/android/youtube/app/remote/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/al;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/as;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/as;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/as;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/ax;-><init>(Lcom/google/android/youtube/app/remote/as;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->b(Lcom/google/android/youtube/app/remote/as;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/as;->b(Lcom/google/android/youtube/app/remote/as;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Landroid/util/Pair;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->c(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/o;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/as;->d(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/p;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v1, v5, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->b(Lcom/google/android/youtube/app/remote/as;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/remote/ax;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->e(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/aq;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/app/remote/at;->b:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->f(Lcom/google/android/youtube/app/remote/as;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->g(Lcom/google/android/youtube/app/remote/as;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->f(Lcom/google/android/youtube/app/remote/as;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->g(Lcom/google/android/youtube/app/remote/as;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    new-instance v1, Lcom/google/android/youtube/core/async/o;

    new-instance v2, Lcom/google/android/youtube/app/remote/ay;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    const/4 v4, 0x0

    invoke-direct {v2, v3, p1, v4}, Lcom/google/android/youtube/app/remote/ay;-><init>(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;B)V

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/async/o;-><init>(Lcom/google/android/youtube/core/async/bk;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/o;)Lcom/google/android/youtube/core/async/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->h(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/as;->c(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->e(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/aq;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ax;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->o()Z

    return-void
.end method

.method public final n()V
    .locals 0

    return-void
.end method
