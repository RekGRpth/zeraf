.class final Lcom/google/android/youtube/app/remote/cg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/br;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/br;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/cg;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/br;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/cg;-><init>(Lcom/google/android/youtube/app/remote/br;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error loading available screens"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    const/4 v2, 0x0

    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cg;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->j(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    :cond_0
    :goto_0
    if-nez v2, :cond_6

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/cg;->a:Lcom/google/android/youtube/app/remote/br;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cg;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->j(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/cg;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v3, v0}, Lcom/google/android/youtube/app/remote/br;->d(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    move v1, v2

    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/remote/bp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/cg;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v3, v0}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_3

    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    return-void
.end method
