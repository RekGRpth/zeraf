.class public final Lcom/google/android/youtube/app/remote/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/bq;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final c:Ljava/util/Map;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Landroid/content/res/Resources;

.field private final f:Ljava/util/concurrent/Executor;

.field private g:Z

.field private final h:Lcom/google/android/ytremote/backend/a/a;

.field private final i:Lcom/google/android/ytremote/backend/a/d;

.field private final j:Lcom/google/android/youtube/app/remote/bf;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Landroid/content/res/Resources;Lcom/google/android/youtube/app/remote/bf;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    const-string v0, "preferences can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    const-string v0, "resources can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->e:Landroid/content/res/Resources;

    const-string v0, "youTubeTvRemoteControl can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bf;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->j:Lcom/google/android/youtube/app/remote/bf;

    new-instance v0, Lcom/google/android/ytremote/backend/a/f;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/f;-><init>()V

    new-instance v1, Lcom/google/android/youtube/app/remote/w;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/remote/w;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/backend/a/f;)V

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/ytremote/backend/a/a;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->h:Lcom/google/android/ytremote/backend/a/a;

    new-instance v0, Lcom/google/android/ytremote/backend/a/d;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->i:Lcom/google/android/ytremote/backend/a/d;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/v;->c:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/ScreenId;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/v;->a(Lcom/google/android/ytremote/model/ScreenId;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/ytremote/model/ScreenId;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 7

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/v;->b()V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/v;->e:Landroid/content/res/Resources;

    const v3, 0x7f0b023b

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/remote/v;->a(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v3

    if-nez v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/v;->a(Lcom/google/android/ytremote/model/ScreenId;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/v;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/SharedPreferences;)Ljava/util/List;
    .locals 8

    const-string v0, "screenIds"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "screenIds"

    const-string v1, ""

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-string v0, "screenNames"

    const-string v1, ""

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_1
    array-length v1, v3

    if-ge v0, v1, :cond_3

    aget-object v1, v3, v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Lcom/google/android/ytremote/model/CloudScreen;

    new-instance v6, Lcom/google/android/ytremote/model/ScreenId;

    invoke-direct {v6, v1}, Lcom/google/android/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    array-length v1, v4

    if-ge v0, v1, :cond_2

    aget-object v1, v4, v0

    :goto_2
    const/4 v7, 0x0

    invoke-direct {v5, v6, v1, v7}, Lcom/google/android/ytremote/model/CloudScreen;-><init>(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/ytremote/model/LoungeToken;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const-string v1, ""

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Landroid/content/SharedPreferences;)Ljava/util/List;
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/app/remote/v;->a(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Ljava/util/List;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    new-instance v3, Lcom/google/android/youtube/app/remote/bp;

    invoke-direct {v3, v0}, Lcom/google/android/youtube/app/remote/bp;-><init>(Lcom/google/android/ytremote/model/CloudScreen;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/v;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/v;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/v;->b()V

    const/4 v0, 0x2

    move-object v1, p1

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/remote/v;->a(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v2

    if-nez v2, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/v;->g:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    const-string v1, "screenIds"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/v;->a(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/ac;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/ac;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/v;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/youtube/app/remote/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->j:Lcom/google/android/youtube/app/remote/bf;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/v;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/ytremote/backend/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->h:Lcom/google/android/ytremote/backend/a/a;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/v;)Lcom/google/android/ytremote/backend/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->i:Lcom/google/android/ytremote/backend/a/d;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/v;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/v;->b()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/v;)V
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->d:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v3, "screenIds"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "screenNames"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/y;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/ytremote/model/ScreenId;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/ab;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/app/remote/ab;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/ScreenId;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/aa;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/app/remote/aa;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/app/remote/x;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/android/youtube/app/remote/x;-><init>(Lcom/google/android/youtube/app/remote/v;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/v;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/app/remote/z;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/z;-><init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
