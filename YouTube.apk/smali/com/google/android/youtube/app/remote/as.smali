.class public final Lcom/google/android/youtube/app/remote/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/ag;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/core/client/bc;

.field private final c:Lcom/google/android/youtube/app/remote/ax;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/async/n;

.field private final f:Landroid/content/res/Resources;

.field private final g:Lcom/google/android/youtube/app/remote/av;

.field private final h:Lcom/google/android/youtube/app/remote/aq;

.field private i:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private j:Lcom/google/android/youtube/core/async/o;

.field private k:Lcom/google/android/youtube/core/async/p;

.field private final l:Lcom/google/android/youtube/app/remote/ad;

.field private final m:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/remote/aq;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Landroid/content/Context;

    const-string v0, "mediaRouteManager can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/ad;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->l:Lcom/google/android/youtube/app/remote/ad;

    const-string v0, "gDataClient can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "userAuthorizer can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "remoteControlClientHelper cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/aq;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->h:Lcom/google/android/youtube/app/remote/aq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->f:Landroid/content/res/Resources;

    new-instance v0, Lcom/google/android/youtube/app/remote/ba;

    invoke-direct {v0, p0, p4, v2}, Lcom/google/android/youtube/app/remote/ba;-><init>(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/client/be;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->e:Lcom/google/android/youtube/core/async/n;

    new-instance v0, Lcom/google/android/youtube/app/remote/ax;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/remote/ax;-><init>(Lcom/google/android/youtube/app/remote/as;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->c:Lcom/google/android/youtube/app/remote/ax;

    new-instance v0, Lcom/google/android/youtube/app/remote/av;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/remote/av;-><init>(Lcom/google/android/youtube/app/remote/as;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->g:Lcom/google/android/youtube/app/remote/av;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->g:Lcom/google/android/youtube/app/remote/av;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/av;->a()V

    invoke-virtual {p2, p0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ag;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.youtube.action.remote_next"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.youtube.action.remote_playpause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.youtube.action.remote_prev"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/youtube/app/remote/aw;

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/remote/aw;-><init>(Lcom/google/android/youtube/app/remote/as;B)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/google/android/youtube/app/remote/az;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->g:Lcom/google/android/youtube/app/remote/av;

    invoke-direct {v0, p2, v1, p6}, Lcom/google/android/youtube/app/remote/az;-><init>(Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/remote/av;Lcom/google/android/youtube/app/remote/aq;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->m:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/as;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    invoke-static {p1, v1, v2, v0, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/as;->k:Lcom/google/android/youtube/core/async/p;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/o;)Lcom/google/android/youtube/core/async/o;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/as;->j:Lcom/google/android/youtube/core/async/o;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "We should be connected to a screen, but the value is null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b0241

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->g:Lcom/google/android/youtube/app/remote/av;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/av;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->h:Lcom/google/android/youtube/app/remote/aq;

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/as;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->m:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->j:Lcom/google/android/youtube/core/async/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->j:Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/o;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/as;->j:Lcom/google/android/youtube/core/async/o;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->k:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->k:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/as;->k:Lcom/google/android/youtube/core/async/p;

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->j:Lcom/google/android/youtube/core/async/o;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->k:Lcom/google/android/youtube/core/async/p;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->h:Lcom/google/android/youtube/app/remote/aq;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/as;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/as;->a()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/as;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/as;->b()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->e:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->b:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/av;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->g:Lcom/google/android/youtube/app/remote/av;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->l:Lcom/google/android/youtube/app/remote/ad;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->c:Lcom/google/android/youtube/app/remote/ax;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/al;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/as;->b()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/as;->a()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->c:Lcom/google/android/youtube/app/remote/ax;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/al;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/as;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/as;->c:Lcom/google/android/youtube/app/remote/ax;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/al;)V

    :cond_1
    return-void
.end method
