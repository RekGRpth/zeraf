.class public final Lcom/google/android/youtube/app/remote/bf;
.super Lcom/google/android/youtube/app/remote/n;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/bl;


# static fields
.field private static final a:Lorg/json/JSONObject;

.field private static final b:Landroid/content/IntentFilter;

.field private static final c:Landroid/content/IntentFilter;

.field private static final d:Ljava/util/Map;

.field private static final e:Ljava/util/Map;

.field private static final f:Ljava/util/Random;


# instance fields
.field private A:Lcom/google/android/youtube/app/remote/bn;

.field private B:I

.field private final C:Ljava/util/Map;

.field private D:D

.field private E:J

.field private F:Z

.field private G:Lcom/google/android/youtube/core/model/SubtitleTrack;

.field private final H:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final I:Lcom/google/android/ytremote/logic/a;

.field private final J:Ljava/util/Map;

.field private K:Ljava/util/Set;

.field private L:Z

.field private final M:Ljava/lang/String;

.field private final N:Lcom/google/android/youtube/app/a;

.field private final O:Z

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/ytremote/backend/browserchannel/k;

.field private final i:Lcom/google/android/ytremote/logic/d;

.field private final j:Lcom/google/android/youtube/core/utils/p;

.field private final k:Lcom/google/android/youtube/app/remote/bh;

.field private final l:Lcom/google/android/ytremote/backend/a/a;

.field private final m:Lcom/google/android/ytremote/backend/logic/b;

.field private final n:Ljava/util/Map;

.field private final o:Z

.field private final p:Landroid/content/SharedPreferences;

.field private q:Z

.field private r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private s:Z

.field private t:Lcom/google/android/youtube/app/remote/bp;

.field private u:Lcom/google/android/ytremote/model/CloudScreen;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private final x:Ljava/util/List;

.field private final y:Ljava/util/Map;

.field private final z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/remote/bf;->a:Lorg/json/JSONObject;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/remote/bf;->b:Landroid/content/IntentFilter;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/remote/bf;->c:Landroid/content/IntentFilter;

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->b:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->LOUNGE_SERVER_CONNECTION_ERROR:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->b:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_NO_NETWORK:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->b:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_IPV6_ERROR:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->c:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->c:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "notFound"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "private"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "restrictedRegion"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->REGION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "limitedSyndication"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIMITED_SYNDICATION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "liveNotSupported"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIVE_NOT_SUPPORTED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "unknown"

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->UNSPECIFIED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "LOUNGE_SCREEN"

    sget-object v3, Lcom/google/android/ytremote/backend/model/DeviceType;->LOUNGE_SCREEN:Lcom/google/android/ytremote/backend/model/DeviceType;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "REMOTE_CONTROL"

    sget-object v3, Lcom/google/android/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/ytremote/backend/model/DeviceType;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/remote/bf;->d:Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/remote/bf;->e:Ljava/util/Map;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/remote/bf;->f:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/p;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;ZZLcom/google/android/youtube/app/a;)V
    .locals 8

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/n;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    iput-boolean p5, p0, Lcom/google/android/youtube/app/remote/bf;->o:Z

    iput-boolean p6, p0, Lcom/google/android/youtube/app/remote/bf;->O:Z

    const-string v0, "flags can not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->N:Lcom/google/android/youtube/app/a;

    const-string v0, "context can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "preferences can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->p:Landroid/content/SharedPreferences;

    const-string v0, "userAuthorizer can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->H:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->g:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/bf;->j:Lcom/google/android/youtube/core/utils/p;

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->p:Landroid/content/SharedPreferences;

    const-string v2, "remote_id"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->p:Landroid/content/SharedPreferences;

    const-string v2, "remote_id"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->M:Ljava/lang/String;

    new-instance v7, Lcom/google/android/ytremote/backend/browserchannel/k;

    sget-object v1, Lcom/google/android/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/ytremote/backend/model/DeviceType;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->M:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lcom/google/android/youtube/app/remote/bk;

    const/4 v3, 0x0

    invoke-direct {v5, p0, v3}, Lcom/google/android/youtube/app/remote/bk;-><init>(Lcom/google/android/youtube/app/remote/bf;B)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "android"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v6, "device"

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/DeviceType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "id"

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "name"

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "app"

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/t;

    const-string v2, "www.youtube.com"

    const/16 v3, 0x50

    const-string v4, "/api/lounge/bc/"

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ytremote/backend/browserchannel/t;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/ytremote/backend/browserchannel/u;Ljava/util/Map;)V

    invoke-direct {v7, p1, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;-><init>(Landroid/content/Context;Lcom/google/android/ytremote/backend/browserchannel/c;)V

    iput-object v7, p0, Lcom/google/android/youtube/app/remote/bf;->h:Lcom/google/android/ytremote/backend/browserchannel/k;

    new-instance v0, Lcom/google/android/ytremote/b/d;

    new-instance v1, Lcom/google/android/ytremote/backend/a/f;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/a/f;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/b/d;-><init>(Lcom/google/android/ytremote/backend/logic/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->i:Lcom/google/android/ytremote/logic/d;

    new-instance v0, Lcom/google/android/ytremote/b/c;

    invoke-direct {v0}, Lcom/google/android/ytremote/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->I:Lcom/google/android/ytremote/logic/a;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->n:Ljava/util/Map;

    new-instance v0, Lcom/google/android/ytremote/backend/a/a;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->l:Lcom/google/android/ytremote/backend/a/a;

    new-instance v0, Lcom/google/android/ytremote/backend/a/d;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->m:Lcom/google/android/ytremote/backend/logic/b;

    new-instance v0, Lcom/google/android/youtube/app/remote/bh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/bh;-><init>(Lcom/google/android/youtube/app/remote/bf;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->k:Lcom/google/android/youtube/app/remote/bh;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->y:Ljava/util/Map;

    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/google/android/youtube/app/remote/bl;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/youtube/app/remote/bl;-><init>(Lcom/google/android/youtube/app/remote/bf;Landroid/os/Looper;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {p4, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bl;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->J:Ljava/util/Map;

    new-instance v0, Lcom/google/android/youtube/app/remote/bo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/bo;-><init>(Lcom/google/android/youtube/app/remote/bf;B)V

    sget-object v1, Lcom/google/android/youtube/app/remote/bf;->c:Landroid/content/IntentFilter;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->C:Ljava/util/Map;

    return-void

    :cond_0
    new-instance v1, Ljava/math/BigInteger;

    const/16 v2, 0x82

    sget-object v3, Lcom/google/android/youtube/app/remote/bf;->f:Ljava/util/Random;

    invoke-direct {v1, v2, v3}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->p:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "remote_id"

    invoke-virtual {v2, v3, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    goto/16 :goto_0
.end method

.method static synthetic A(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    return-object v0
.end method

.method static synthetic A()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic B(Lcom/google/android/youtube/app/remote/bf;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic B()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->d:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic C(Lcom/google/android/youtube/app/remote/bf;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->F:Z

    return v0
.end method

.method static synthetic D(Lcom/google/android/youtube/app/remote/bf;)D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    return-wide v0
.end method

.method static synthetic E(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/core/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->j:Lcom/google/android/youtube/core/utils/p;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/model/SubtitleTrack;)Lcom/google/android/youtube/core/model/SubtitleTrack;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->G:Lcom/google/android/youtube/core/model/SubtitleTrack;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;J)Lcom/google/android/ytremote/backend/model/Params;
    .locals 4

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoIds"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "currentTime"

    const-wide/16 v2, 0x3e8

    div-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/ytremote/model/CloudScreen;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->u:Lcom/google/android/ytremote/model/CloudScreen;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->K:Ljava/util/Set;

    return-object p1
.end method

.method private a(D)V
    .locals 2

    iput-wide p1, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/remote/bf;->E:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;D)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/bf;->a(D)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 6

    const-wide/16 v2, 0x0

    sget-object v0, Lcom/google/android/youtube/app/remote/bg;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_1

    iput-wide v2, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/remote/bf;->E:J

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/youtube/app/remote/bf;->E:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    goto :goto_0

    :pswitch_2
    iput-wide v2, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/bf;->f(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;J)V
    .locals 10

    const-string v0, "screen can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/remote/bf;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/remote/bp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/remote/bf;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->K:Ljava/util/Set;

    iget-object v7, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    iget-object v8, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v9, 0x3

    new-instance v0, Lcom/google/android/youtube/app/remote/bi;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/remote/bi;-><init>(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;JZB)V

    invoke-static {v8, v9, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sending "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->h:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bf;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->F:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bf;I)Lcom/google/android/youtube/app/remote/an;
    .locals 1

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lcom/google/android/youtube/app/remote/ao;->c:Lcom/google/android/youtube/app/remote/an;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/app/remote/be;->b:Lcom/google/android/youtube/app/remote/an;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/youtube/app/remote/be;->d:Lcom/google/android/youtube/app/remote/an;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/youtube/app/remote/be;->c:Lcom/google/android/youtube/app/remote/an;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/youtube/app/remote/ao;->a:Lcom/google/android/youtube/app/remote/an;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->h:Lcom/google/android/ytremote/backend/browserchannel/k;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    return-object p1
.end method

.method private b(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "username"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "authToken"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->UPDATE_USERNAME:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bf;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->q:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->v:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->J:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bf;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/remote/bf;->s:Z

    return p1
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/bf;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/remote/bf;->L:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/bf;)V
    .locals 2

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->GET_NOW_PLAYING:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/bf;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    return v0
.end method

.method private f(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->O:Z

    if-nez v0, :cond_0

    const-string v0, "true"

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->N:Lcom/google/android/youtube/app/a;

    const-string v2, "enable_mdx_logs"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "YouTube MDX"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->C:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/bf;)Z
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connected remotes are "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->K:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->K:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/model/c;

    iget-object v0, v0, Lcom/google/android/ytremote/backend/model/c;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->M:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/logic/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->i:Lcom/google/android/ytremote/logic/d;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/logic/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->I:Lcom/google/android/ytremote/logic/a;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/remote/bf;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->q:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/bh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->k:Lcom/google/android/youtube/app/remote/bh;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->u:Lcom/google/android/ytremote/model/CloudScreen;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->y:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->n:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->p:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->l:Lcom/google/android/ytremote/backend/a/a;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/logic/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->m:Lcom/google/android/ytremote/backend/logic/b;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->H:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/youtube/app/remote/bf;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->s:Z

    return v0
.end method

.method static synthetic u(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/bn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->A:Lcom/google/android/youtube/app/remote/bn;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/bp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->K:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/youtube/app/remote/bf;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->b:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/core/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->G:Lcom/google/android/youtube/core/model/SubtitleTrack;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/youtube/app/remote/bf;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z()Lorg/json/JSONObject;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/remote/bf;->a:Lorg/json/JSONObject;

    return-object v0
.end method


# virtual methods
.method public final G()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->UPDATE_USERNAME:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    :cond_0
    return-void
.end method

.method protected final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    if-nez v0, :cond_1

    const-string v0, "We should reconnect, but we lost the screen"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->q:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->k:Lcom/google/android/youtube/app/remote/bh;

    sget-object v2, Lcom/google/android/youtube/app/remote/bf;->b:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->h:Lcom/google/android/ytremote/backend/browserchannel/k;

    new-instance v1, Lcom/google/android/ytremote/backend/model/b;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/b;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->u:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/ytremote/model/LoungeToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/ytremote/backend/model/b;->a(Lcom/google/android/ytremote/model/LoungeToken;)Lcom/google/android/ytremote/backend/model/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/b;->a()Lcom/google/android/ytremote/backend/model/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    const/16 v0, 0x64

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v1, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v2, "delta"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v2, "volume"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v2, Lcom/google/android/ytremote/backend/model/Method;->SET_VOLUME:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v2, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->C:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/core/model/UserAuth;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;J)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/app/remote/bp;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;J)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/bn;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->A:Lcom/google/android/youtube/app/remote/bn;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "format"

    iget v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->format:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "kind"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "languageCode"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "languageName"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "sourceLanguageCode"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "trackName"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "videoId"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->SET_SUBTITLES_TRACK:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/core/model/UserAuth;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    const-string v0, "videoId can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;J)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/remote/bf;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->ADD_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v1, "currentTime"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoIds"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->ADD_VIDEOS:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final a(Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/youtube/app/remote/bj;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    invoke-direct {v2, v3, p1}, Lcom/google/android/youtube/app/remote/bj;-><init>(Lcom/google/android/youtube/app/remote/bp;Z)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-boolean v4, p0, Lcom/google/android/youtube/app/remote/bf;->L:Z

    iput-boolean v4, p0, Lcom/google/android/youtube/app/remote/bf;->s:Z

    iput-object v5, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    iput-object v5, p0, Lcom/google/android/youtube/app/remote/bf;->u:Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->C:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->y:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    const/16 v1, 0x1e

    iput v1, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->y:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->y:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    return-object v0
.end method

.method protected final b()V
    .locals 0

    return-void
.end method

.method public final b(I)V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->F:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "newTime"

    div-int/lit16 v2, p1, 0x3e8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->SEEK_TO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    int-to-double v0, p1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(D)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->ADD_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final c()V
    .locals 2

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->PLAY:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->REMOVE_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final d()V
    .locals 2

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->PAUSE:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final d(I)V
    .locals 3

    new-instance v0, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v1, "volume"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    iput p1, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->SET_VOLUME:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/remote/bf;->c(I)V

    return-void
.end method

.method protected final d(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bf;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->G:Lcom/google/android/youtube/core/model/SubtitleTrack;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->z:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->STOP:Lcom/google/android/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/ytremote/backend/model/Params;->a:Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final g_()V
    .locals 0

    return-void
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/remote/bf;->B:I

    return v0
.end method

.method public final i()D
    .locals 6

    iget-wide v2, p0, Lcom/google/android/youtube/app/remote/bf;->D:D

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->r:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/youtube/app/remote/bf;->E:J

    sub-long/2addr v0, v4

    :goto_0
    long-to-double v0, v0

    add-double/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->o:Z

    return v0
.end method

.method public final l()Lcom/google/android/youtube/app/remote/am;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bf;->L:Z

    return v0
.end method

.method public final o()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final p()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->o()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->d(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v2, "videoId"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v0, "currentTime"

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    goto :goto_0
.end method

.method public final q()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final r()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/bf;->q()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bf;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bf;->w:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/bf;->d(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/Params;-><init>()V

    const-string v2, "videoId"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    const-string v0, "currentTime"

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/Params;

    sget-object v0, Lcom/google/android/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/ytremote/backend/model/Method;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V

    goto :goto_0
.end method

.method public final s()Lcom/google/android/youtube/core/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->G:Lcom/google/android/youtube/core/model/SubtitleTrack;

    return-object v0
.end method

.method public final bridge synthetic t()Lcom/google/android/youtube/app/remote/bb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    return-object v0
.end method

.method public final x()Lcom/google/android/youtube/app/remote/bp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bf;->t:Lcom/google/android/youtube/app/remote/bp;

    return-object v0
.end method
