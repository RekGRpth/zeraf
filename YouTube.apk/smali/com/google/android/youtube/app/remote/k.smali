.class final Lcom/google/android/youtube/app/remote/k;
.super Lcom/google/android/youtube/athome/app/common/i;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/e;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    invoke-direct {p0}, Lcom/google/android/youtube/athome/app/common/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/e;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/k;-><init>(Lcom/google/android/youtube/app/remote/e;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/support/place/rpc/RpcContext;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/athome/app/common/i;->a(ILandroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/remote/e;->b(Lcom/google/android/youtube/app/remote/e;I)I

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;Landroid/support/place/rpc/RpcContext;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/athome/app/common/i;->a(Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/e;Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/athome/app/common/i;->a(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/e;->e(Lcom/google/android/youtube/app/remote/e;)Lcom/google/android/youtube/athome/app/common/k;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/athome/app/common/k;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final b(ILandroid/support/place/rpc/RpcContext;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/athome/app/common/i;->b(ILandroid/support/place/rpc/RpcContext;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/e;->d(Lcom/google/android/youtube/app/remote/e;)Lcom/google/android/youtube/athome/app/common/l;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/athome/app/common/l;->a(I)V

    return-void
.end method

.method public final onConnected(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    return-void
.end method

.method public final onDisconnected()V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    sget-object v1, Lcom/google/android/youtube/app/remote/ao;->a:Lcom/google/android/youtube/app/remote/an;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/an;)V

    return-void
.end method

.method public final onError(Landroid/support/place/rpc/RpcError;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RPC error while listening to the player events: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/k;->a:Lcom/google/android/youtube/app/remote/e;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/e;->c(Lcom/google/android/youtube/app/remote/e;)Landroid/support/place/rpc/RpcErrorHandler;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/place/rpc/RpcErrorHandler;->onError(Landroid/support/place/rpc/RpcError;)V

    return-void
.end method
