.class public final Lcom/google/android/youtube/app/player/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lorg/apache/http/params/HttpParams;

.field private final b:Lorg/apache/http/protocol/HttpService;

.field private final c:Lcom/google/android/youtube/app/player/a/k;

.field private d:Ljava/net/ServerSocket;

.field private e:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>(Ljava/security/Key;Landroid/content/SharedPreferences;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-string v1, "http.connection.stalecheck"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/params/BasicHttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "http.tcp.nodelay"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "http.socket.buffer-size"

    const/16 v2, 0x2000

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->a:Lorg/apache/http/params/HttpParams;

    new-instance v0, Lcom/google/android/youtube/app/player/a/k;

    invoke-direct {v0}, Lcom/google/android/youtube/app/player/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->c:Lcom/google/android/youtube/app/player/a/k;

    new-instance v0, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/e;->c:Lcom/google/android/youtube/app/player/a/k;

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    new-instance v1, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v1}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    new-instance v1, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v1}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    new-instance v1, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v1}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    const-string v2, "/local"

    new-instance v3, Lcom/google/android/youtube/app/player/a/d;

    invoke-direct {v3, p1, p2}, Lcom/google/android/youtube/app/player/a/d;-><init>(Ljava/security/Key;Landroid/content/SharedPreferences;)V

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    new-instance v2, Lorg/apache/http/protocol/HttpService;

    new-instance v3, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v3}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v4, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v2, v0, v3, v4}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/player/a/e;->b:Lorg/apache/http/protocol/HttpService;

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->b:Lorg/apache/http/protocol/HttpService;

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->b:Lorg/apache/http/protocol/HttpService;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/e;->a:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    return-void
.end method

.method public static a(Ljava/util/concurrent/Executor;Ljava/security/Key;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/player/a/e;
    .locals 2

    :try_start_0
    new-instance v0, Lcom/google/android/youtube/app/player/a/e;

    invoke-direct {v0, p1, p2}, Lcom/google/android/youtube/app/player/a/e;-><init>(Ljava/security/Key;Landroid/content/SharedPreferences;)V

    new-instance v1, Lcom/google/android/youtube/app/player/a/f;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/app/player/a/f;-><init>(Lcom/google/android/youtube/app/player/a/e;)V

    invoke-interface {p0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Cannot instantiate MediaServer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a/e;)V
    .locals 4

    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0}, Ljava/net/ServerSocket;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->d:Ljava/net/ServerSocket;

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->d:Ljava/net/ServerSocket;

    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-static {}, Ljava/net/InetAddress;->getLocalHost()Ljava/net/InetAddress;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->e:Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/youtube/app/player/a/g;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/player/a/g;-><init>(Lcom/google/android/youtube/app/player/a/e;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/player/a/e;)Ljava/net/ServerSocket;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->d:Ljava/net/ServerSocket;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/player/a/e;)Lorg/apache/http/params/HttpParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->a:Lorg/apache/http/params/HttpParams;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/player/a/e;)Lorg/apache/http/protocol/HttpService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->b:Lorg/apache/http/protocol/HttpService;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "127.0.0.1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a/e;->d:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/local"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "f"

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/e;->c:Lcom/google/android/youtube/app/player/a/k;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/player/a/k;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->d:Ljava/net/ServerSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/e;->d:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->isBound()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
