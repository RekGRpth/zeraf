.class public final Lcom/google/android/youtube/app/player/mp4/c;
.super Lcom/google/android/youtube/app/player/mp4/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(ILcom/google/android/youtube/app/player/mp4/s;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/player/mp4/a;-><init>(ILcom/google/android/youtube/app/player/mp4/s;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/mp4/c;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/player/mp4/s;IZ)Lcom/google/android/youtube/app/player/mp4/a;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/mp4/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/a;->c()Lcom/google/android/youtube/app/player/mp4/s;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/app/player/mp4/s;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/a;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    if-ne v1, p2, :cond_1

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/mp4/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/a;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x8

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;IZ)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/player/mp4/s;I)Lcom/google/android/youtube/app/player/mp4/c;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;IZ)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/c;

    return-object v0
.end method

.method public final a(Ljava/io/DataInputStream;)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/mp4/c;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/app/player/mp4/b;->a(Ljava/io/DataInputStream;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/player/mp4/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/youtube/app/player/mp4/a;->a()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/mp4/c;->b()I

    move-result v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incorrect container atom size."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/c;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;IZ)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/c;

    return-object v0
.end method

.method public final c(Lcom/google/android/youtube/app/player/mp4/s;)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/mp4/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/a;->c()Lcom/google/android/youtube/app/player/mp4/s;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/app/player/mp4/s;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/a;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
