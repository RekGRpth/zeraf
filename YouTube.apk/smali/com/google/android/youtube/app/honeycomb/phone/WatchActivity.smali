.class public Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;
.super Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;
.source "SourceFile"


# instance fields
.field private o:Lcom/google/android/youtube/app/ui/bd;

.field private p:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Branding;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->a(Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method protected final e(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->B()Lcom/google/android/youtube/core/player/PlayerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v1, v1

    const v2, 0x3fe374bc

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bd;->d()V

    return-void
.end method

.method protected final f(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->c(Z)V

    return-void
.end method

.method public final h_()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->h_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bd;->b()V

    return-void
.end method

.method public final j(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->j(Z)V

    return-void
.end method

.method protected final m()I
    .locals 1

    const v0, 0x7f040064

    return v0
.end method

.method protected final n()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final o()V
    .locals 12

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C()Lcom/google/android/youtube/app/ui/dh;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    move-object v0, p0

    move-object v11, p0

    invoke-static/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/bd;->a(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/bh;Lcom/google/android/youtube/app/ui/ep;)Lcom/google/android/youtube/app/ui/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    const v0, 0x7f0700f9

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/youtube/core/a/i;

    invoke-direct {v1}, Lcom/google/android/youtube/core/a/i;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/a/i;->b(Lcom/google/android/youtube/core/a/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bd;->c()Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070102

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->p:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    return-void
.end method

.method protected final q()Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->p:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    return-object v0
.end method
