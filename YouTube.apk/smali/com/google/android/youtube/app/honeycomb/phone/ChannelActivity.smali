.class public Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/bb;
.implements Lcom/google/android/youtube/app/ui/cq;
.implements Lcom/google/android/youtube/app/ui/j;


# static fields
.field private static final n:Ljava/util/Map;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/g;

.field private B:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private C:Ljava/lang/String;

.field private D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private E:Lcom/google/android/youtube/app/ui/bx;

.field private F:Lcom/google/android/youtube/app/ui/bx;

.field private G:Lcom/google/android/youtube/app/ui/bx;

.field private H:Landroid/view/View;

.field private I:Landroid/view/View;

.field private J:Lcom/google/android/youtube/core/ui/Workspace;

.field private K:Z

.field private L:Z

.field private o:Landroid/content/res/Resources;

.field private p:Lcom/google/android/youtube/core/async/au;

.field private q:Lcom/google/android/youtube/core/async/au;

.field private r:Lcom/google/android/youtube/core/async/au;

.field private s:Lcom/google/android/youtube/core/client/bc;

.field private t:Lcom/google/android/youtube/core/client/bg;

.field private u:Lcom/google/android/youtube/core/client/be;

.field private v:Lcom/google/android/youtube/core/e;

.field private w:Lcom/google/android/youtube/app/ui/ea;

.field private x:Lcom/google/android/youtube/app/ui/ea;

.field private y:Lcom/google/android/youtube/app/ui/ag;

.field private z:Lcom/google/android/youtube/app/ui/ba;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Ljava/util/Map;

    const-string v1, "uploads"

    const v2, 0x7f07006b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Ljava/util/Map;

    const-string v1, "favorites"

    const v2, 0x7f07006c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Ljava/util/Map;

    const-string v1, "activity"

    const v2, 0x7f07006d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Ljava/util/Map;

    const-string v1, "playlists"

    const v2, 0x7f07006e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "username"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;Lcom/google/android/youtube/core/model/Branding;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/Branding;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->K:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->L:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/app/ui/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->e(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y:Lcom/google/android/youtube/app/ui/ag;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ag;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->z:Lcom/google/android/youtube/app/ui/ba;

    new-array v1, v5, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->g(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ba;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->K:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->L:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Landroid/content/res/Resources;

    const v1, 0x7f0a000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Landroid/content/res/Resources;

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Landroid/content/res/Resources;

    const v1, 0x7f0a0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_2
    return-void
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Landroid/content/res/Resources;

    const v1, 0x7f0e0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Landroid/content/res/Resources;

    const v2, 0x7f0f0007

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(ZF)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->o:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->B:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->l()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->p:Lcom/google/android/youtube/core/async/au;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->q()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->q:Lcom/google/android/youtube/core/async/au;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->p()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;Z)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Playlist;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;Z)V

    return-void
.end method

.method public final n_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->g()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->h()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 24

    invoke-super/range {p0 .. p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f04001e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/d;->b()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    const-string v2, "channel_name_state_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->finish()V

    :cond_1
    new-instance v2, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->B:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    const-string v9, "ChannelActivity"

    move-object/from16 v3, p0

    move-object/from16 v8, p0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/cq;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->D:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const v2, 0x7f07006f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Landroid/view/View;

    const v2, 0x7f070068

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Landroid/view/View;

    const v3, 0x7f07006a

    const v4, 0x7f070069

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v5, "selected_tab_id"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "selected_tab_id"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    :goto_1
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v2}, Lcom/google/android/youtube/core/ui/Workspace;->a(Landroid/app/Activity;III)Lcom/google/android/youtube/core/ui/Workspace;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->J:Lcom/google/android/youtube/core/ui/Workspace;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v12

    new-instance v2, Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/client/be;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v9}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/app/ui/j;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->A:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/g;->a()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->h()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/client/be;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/client/bg;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->g()V

    new-instance v2, Lcom/google/android/youtube/app/ui/ea;

    const v3, 0x7f07006b

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->E:Lcom/google/android/youtube/app/ui/bx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->p:Lcom/google/android/youtube/core/async/au;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    const/4 v8, 0x1

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    sget-object v13, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/ea;->d()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/client/be;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/client/bg;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->g()V

    new-instance v2, Lcom/google/android/youtube/app/ui/ea;

    const v3, 0x7f07006c

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->F:Lcom/google/android/youtube/app/ui/bx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->q:Lcom/google/android/youtube/core/async/au;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    const/4 v8, 0x1

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_FAVORITE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    sget-object v13, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelFavorites:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/ea;->d()V

    new-instance v13, Lcom/google/android/youtube/app/ui/ag;

    const v2, 0x7f07006d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/google/android/youtube/core/ui/PagedView;

    new-instance v17, Lcom/google/android/youtube/app/adapter/al;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/al;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    move-object/from16 v19, v0

    sget-object v21, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelActivity:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v22, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/16 v23, 0x0

    move-object/from16 v14, p0

    move-object v15, v9

    move-object/from16 v20, v12

    invoke-direct/range {v13 .. v23}, Lcom/google/android/youtube/app/ui/ag;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y:Lcom/google/android/youtube/app/ui/ag;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->y:Lcom/google/android/youtube/app/ui/ag;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/ag;->d()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->u:Lcom/google/android/youtube/core/client/be;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->t:Lcom/google/android/youtube/core/client/bg;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->g()V

    new-instance v2, Lcom/google/android/youtube/app/ui/ba;

    const v3, 0x7f07006e

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->G:Lcom/google/android/youtube/app/ui/bx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->r:Lcom/google/android/youtube/core/async/au;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->v:Lcom/google/android/youtube/core/e;

    move-object/from16 v3, p0

    move-object/from16 v8, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/ui/ba;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/bb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->z:Lcom/google/android/youtube/app/ui/ba;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->K:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->L:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->I:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->H:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->J:Lcom/google/android/youtube/core/ui/Workspace;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->J:Lcom/google/android/youtube/core/ui/Workspace;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/ui/Workspace;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/ui/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/m;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/youtube/app/honeycomb/phone/m;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/ui/PagedListView;->setOnPagedViewStateChangeListener(Lcom/google/android/youtube/core/ui/g;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->s:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    new-instance v4, Lcom/google/android/youtube/app/honeycomb/phone/o;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/google/android/youtube/app/honeycomb/phone/o;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;B)V

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/client/bc;->d(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void

    :cond_2
    const-string v3, "username"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    const-string v5, "selected_tab_name"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "selected_tab_name"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->n:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "channel_name_state_key"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_channel"

    return-object v0
.end method
