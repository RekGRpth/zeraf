.class abstract Lcom/google/android/youtube/app/honeycomb/x;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/x;-><init>()V

    return-void
.end method

.method private d()Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->a()Ljava/lang/Class;

    move-result-object v0

    :goto_0
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->b()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/Class;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->b()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b()Ljava/lang/Class;
.end method

.method protected c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/app/honeycomb/Shell;->a(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Landroid/content/SharedPreferences;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/Shell;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/Shell;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/Shell;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/Shell;->d()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/Shell;->b()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "forward_intent"

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/x;->d()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/honeycomb/Shell;->a(J)J

    invoke-static {v1}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v1

    const-string v2, "upgrade_prompt_shown_millis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;J)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/compat/ad;->a()V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/x;->startActivity(Landroid/content/Intent;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/x;->finish()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/x;->d()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/x;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
