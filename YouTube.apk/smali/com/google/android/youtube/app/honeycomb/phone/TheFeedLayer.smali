.class public final Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/prefetch/f;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/client/be;

.field private final f:Lcom/google/android/youtube/core/client/bg;

.field private final g:Lcom/google/android/youtube/core/utils/p;

.field private final h:Lcom/google/android/youtube/app/prefetch/d;

.field private final i:Lcom/google/android/youtube/core/e;

.field private final j:Lcom/google/android/youtube/core/Analytics;

.field private final k:Lcom/google/android/youtube/core/client/bc;

.field private final l:Lcom/google/android/youtube/app/d;

.field private final m:Lcom/google/android/youtube/app/ui/ar;

.field private final n:Landroid/view/ViewGroup;

.field private final o:Lcom/google/android/youtube/core/ui/PagedListView;

.field private final p:Lcom/google/android/youtube/app/honeycomb/phone/da;

.field private q:Landroid/view/View;

.field private r:Z

.field private s:Lcom/google/android/youtube/app/ui/bx;

.field private t:Lcom/google/android/youtube/app/ui/bx;

.field private u:Lcom/google/android/youtube/app/ui/a;

.field private v:Lcom/google/android/youtube/app/ui/ea;

.field private w:Lcom/google/android/youtube/core/async/au;

.field private x:Lcom/google/android/youtube/app/adapter/bm;

.field private y:Lcom/google/android/youtube/app/adapter/bm;

.field private final z:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;Lcom/google/android/youtube/app/honeycomb/phone/da;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    const-string v0, "displayMode may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->z:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    const-string v0, "listener may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/da;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->p:Lcom/google/android/youtube/app/honeycomb/phone/da;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f040099

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->e:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->f:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->g:Lcom/google/android/youtube/core/utils/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->i:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->j:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->l:Lcom/google/android/youtube/app/d;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->n:Landroid/view/ViewGroup;

    const v1, 0x7f070144

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v0, Lcom/google/android/youtube/app/ui/ar;

    invoke-direct {v0}, Lcom/google/android/youtube/app/ui/ar;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->m:Lcom/google/android/youtube/app/ui/ar;

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/adapter/bm;)Lcom/google/android/youtube/app/ui/bx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/core/ui/PagedListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->r:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->n()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/app/ui/ar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->m:Lcom/google/android/youtube/app/ui/ar;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->j:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->l:Lcom/google/android/youtube/app/d;

    return-object v0
.end method

.method private n()V
    .locals 4

    const/4 v3, -0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->r:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f04009a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    const v1, 0x7f070146

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->q:Landroid/view/View;

    const v1, 0x7f070148

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private p()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->s:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->s:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->t:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->t:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    const/4 v7, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->z:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->WHAT_TO_WATCH:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    if-ne v0, v1, :cond_1

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->y:Lcom/google/android/youtube/app/adapter/bm;

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->v:Lcom/google/android/youtube/app/ui/ea;

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->t:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->m:Lcom/google/android/youtube/app/ui/ar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ar;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->e:Lcom/google/android/youtube/core/client/be;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->f:Lcom/google/android/youtube/core/client/bg;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->g:Lcom/google/android/youtube/core/utils/p;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->h:Lcom/google/android/youtube/app/prefetch/d;

    new-instance v5, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v5, v0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/app/adapter/cj;->a(Landroid/graphics/Typeface;)V

    invoke-static {v0, v2, v3, v7}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v3

    new-instance v6, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v8

    const/16 v9, 0x8

    invoke-direct {v6, v4, v8, v9, v13}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v4, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v4}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/adapter/an;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Lcom/google/android/youtube/app/adapter/an;-><init>(Landroid/content/res/Resources;Lcom/google/android/youtube/app/adapter/bv;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/bw;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/youtube/app/adapter/bw;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v1}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v1, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/bm;

    const v3, 0x7f040098

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->x:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->x:Lcom/google/android/youtube/app/adapter/bm;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a(Lcom/google/android/youtube/app/adapter/bm;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->s:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->p()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    const v1, 0x7f0b011a

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setEmptyText(I)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/cx;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->s:Lcom/google/android/youtube/app/ui/bx;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->i:Lcom/google/android/youtube/core/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/honeycomb/phone/cx;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->u:Lcom/google/android/youtube/app/ui/a;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->u:Lcom/google/android/youtube/app/ui/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/a;->d()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->z:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->MY_SUBSCRIPTIONS:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    if-ne v0, v1, :cond_0

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->x:Lcom/google/android/youtube/app/adapter/bm;

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->u:Lcom/google/android/youtube/app/ui/a;

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->s:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->e:Lcom/google/android/youtube/core/client/be;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->f:Lcom/google/android/youtube/core/client/bg;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->g:Lcom/google/android/youtube/core/utils/p;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/prefetch/d;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->y:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->y:Lcom/google/android/youtube/app/adapter/bm;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a(Lcom/google/android/youtube/app/adapter/bm;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->t:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->l()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->w:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/cy;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->t:Lcom/google/android/youtube/app/ui/bx;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->w:Lcom/google/android/youtube/core/async/au;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->i:Lcom/google/android/youtube/core/e;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->l:Lcom/google/android/youtube/app/d;

    sget-object v10, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v11, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->j:Lcom/google/android/youtube/core/Analytics;

    sget-object v12, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move v7, v13

    move v9, v13

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/app/honeycomb/phone/cy;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->v:Lcom/google/android/youtube/app/ui/ea;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->p()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->v:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->d()V

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->n()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->u:Lcom/google/android/youtube/app/ui/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->v:Lcom/google/android/youtube/app/ui/ea;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->j(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->u:Lcom/google/android/youtube/app/ui/a;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->k:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->k(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/a;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->x:Lcom/google/android/youtube/app/adapter/bm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->y:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->x:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    const v0, 0x7f0b017f

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->z:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->MY_SUBSCRIPTIONS:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    if-ne v1, v2, :cond_0

    const v0, 0x7f0b0180

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->g()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->a(Lcom/google/android/youtube/app/prefetch/f;)V

    return-void
.end method

.method public final g_()V
    .locals 0

    return-void
.end method

.method public final i()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->i()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->b(Lcom/google/android/youtube/app/prefetch/f;)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->p:Lcom/google/android/youtube/app/honeycomb/phone/da;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/phone/da;->f()V

    return-void
.end method
