.class final Lcom/google/android/youtube/app/honeycomb/phone/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ab;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->i(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->j(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "REMOTE"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->k(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "ACCOUNT"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->l(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "WHAT_TO_WATCH"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "MY_SUBSCRIPTIONS"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->n(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "CHANNEL_STORE"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->o(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "RECOMMENDED_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->p(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "TRENDING_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->q(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    if-ne p3, v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "LIVE_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->b(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->b(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/adapter/bm;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subscription;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/af;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
