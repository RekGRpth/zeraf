.class public Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/ab;
.implements Lcom/google/android/youtube/app/ui/ec;
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Landroid/net/Uri;

.field private G:Lcom/google/android/youtube/core/async/n;

.field private H:Lcom/google/android/youtube/app/prefetch/d;

.field private I:Z

.field private J:Ljava/lang/String;

.field private K:I

.field private L:Lcom/google/android/youtube/app/remote/ad;

.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/au;

.field private p:Lcom/google/android/youtube/core/async/au;

.field private q:Lcom/google/android/youtube/core/client/bc;

.field private r:Lcom/google/android/youtube/core/client/be;

.field private s:Lcom/google/android/youtube/core/client/bg;

.field private t:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private u:Lcom/google/android/youtube/core/e;

.field private v:Lcom/google/android/youtube/app/ui/v;

.field private w:I

.field private x:Lcom/google/android/youtube/app/ui/ea;

.field private y:Lcom/google/android/youtube/app/adapter/bm;

.field private z:Lcom/google/android/youtube/app/ui/bx;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "playlist_uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->c(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    return v0
.end method

.method private c(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Landroid/widget/TextView;

    const v1, 0x7f0b01d2

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    if-ne p1, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Landroid/widget/TextView;

    const v1, 0x7f0b01d3

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Landroid/widget/TextView;

    const v1, 0x7f0b01d4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/ui/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    return v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3ec
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->r:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->s:Lcom/google/android/youtube/core/client/bg;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->b()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->o:Lcom/google/android/youtube/core/async/au;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->j()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->H:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->L:Lcom/google/android/youtube/app/remote/ad;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/bc;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    invoke-virtual {v3, v4, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;I)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Z

    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYLISTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, p2, v2, v3}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;IZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/16 v1, 0x8

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Z

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYLISTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v3, v2, v4, v5}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;IZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->summary:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->summary:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget v0, p2, Lcom/google/android/youtube/core/model/Playlist;->size:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->c(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    if-eqz v3, :cond_4

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->finish()V

    return-void
.end method

.method public final synthetic a(ILjava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:I

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/v;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bs;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-direct {v1, p0, p2, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/bs;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/e;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->f()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040067

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->b()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/r;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/r;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/google/android/youtube/core/utils/r;->a:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-boolean v0, v2, Lcom/google/android/youtube/core/utils/r;->b:Z

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "playlist_id_state_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    const-string v0, "play_first_on_load_state_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    const-string v0, "playlist_uri_state_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    if-nez v0, :cond_5

    const-string v0, "Invalid intent: Playlist Uri or Playlist Id not set"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->finish()V

    :goto_4
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const-string v0, "playlist_uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    goto :goto_2

    :cond_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->i(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    :goto_5
    const-string v0, "authenticate"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Z

    const v0, 0x7f0700c2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    const v0, 0x7f0700fd

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    const v0, 0x7f0700fe

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Z

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/async/au;

    :goto_6
    new-instance v0, Lcom/google/android/youtube/app/ui/br;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->L:Lcom/google/android/youtube/app/remote/ad;

    const v1, 0x7f0700fa

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/core/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/br;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;Landroid/view/View;Lcom/google/android/youtube/core/e;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Z

    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x3ec

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b01fc

    const v2, 0x7f0200d0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:I

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->r:Lcom/google/android/youtube/core/client/be;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->s:Lcom/google/android/youtube/core/client/bg;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->H:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:Lcom/google/android/youtube/app/ui/v;

    const v10, 0x7f0400bc

    move-object v5, p0

    invoke-static/range {v5 .. v10}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    :goto_7
    const v0, 0x7f0700ff

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    invoke-static {p0, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->f()V

    new-instance v5, Lcom/google/android/youtube/app/honeycomb/phone/br;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    iget-object v11, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/core/e;

    const/4 v12, 0x1

    move-object v6, p0

    move-object v7, p0

    move-object v10, v3

    move-object v13, p0

    invoke-direct/range {v5 .. v13}, Lcom/google/android/youtube/app/honeycomb/phone/br;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/ui/ec;)V

    iput-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-static {p0, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/core/async/n;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto/16 :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    goto/16 :goto_5

    :cond_7
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->o:Lcom/google/android/youtube/core/async/au;

    goto/16 :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->r:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->s:Lcom/google/android/youtube/core/client/bg;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->H:Lcom/google/android/youtube/app/prefetch/d;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    goto :goto_7
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->d()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const-string v0, "playlist_uri_state_key"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "playlist_id_state_key"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "play_first_on_load_state_key"

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->e()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_playlist"

    return-object v0
.end method
