.class public Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x2

    const/4 v0, 0x0

    const v1, 0x7f0d0032

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1, v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f050001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->a:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->a:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->setAnimation(Landroid/view/animation/Animation;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->setVisibility(I)V

    return-void
.end method
