.class public abstract Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.super Lcom/google/android/youtube/app/ui/av;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/ui/av;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    return-void
.end method

.method public static final a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
    .locals 4

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/f;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/f;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    :goto_0
    return-object v0

    :cond_0
    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/d;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/a;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    if-lt v0, v1, :cond_3

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/h;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/ui/h;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to instantiate ActionBarMenuHelper on an unsupported platform version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public abstract a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;
.end method

.method public abstract a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method
