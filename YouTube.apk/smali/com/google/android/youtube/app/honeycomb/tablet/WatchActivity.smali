.class public Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;
.super Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;
.source "SourceFile"


# instance fields
.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/ListView;

.field private r:Landroid/widget/ListView;

.field private s:Lcom/google/android/youtube/app/ui/ek;

.field private t:Lcom/google/android/youtube/app/ui/bd;

.field private u:Lcom/google/android/youtube/app/ui/as;

.field private v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

.field private w:Landroid/widget/FrameLayout;

.field private x:Landroid/widget/FrameLayout;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;-><init>()V

    return-void
.end method

.method private N()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->o:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->y:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->z:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->h(Z)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Branding;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->a(Lcom/google/android/youtube/core/model/Branding;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->u:Lcom/google/android/youtube/app/ui/as;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/as;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->u:Lcom/google/android/youtube/app/ui/as;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/as;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->s:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method protected final e(Z)V
    .locals 13

    const/4 v1, -0x1

    const v12, 0x3fe374bc

    const v11, 0x7f07013f

    const/16 v3, 0x8

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->y:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {p0, v11}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    invoke-virtual {v1}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    const v7, 0x7f0800bb

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    const v8, 0x7f0800bc

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    const v9, 0x7f0800bd

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    const v10, 0x7f0800be

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->setPadding(IIII)V

    if-eqz p1, :cond_1

    const v6, 0x7f08008c

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x7f08008b

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v1, v6, v1, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->a(IIII)V

    mul-int/lit8 v4, v4, 0x2

    div-int/lit8 v4, v4, 0x3

    mul-int/lit8 v6, v1, 0x2

    sub-int/2addr v4, v6

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v4, v4

    div-float/2addr v4, v12

    float-to-int v4, v4

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v0, 0x1

    invoke-virtual {v5, v0, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v0, 0x6

    invoke-virtual {v5, v0, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iput v1, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->q:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->r:Landroid/widget/ListView;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->z:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->x:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->u:Lcom/google/android/youtube/app/ui/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/as;->r_()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->N()V

    return-void

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    int-to-float v1, v4

    div-float/2addr v1, v12

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v0, 0x3

    invoke-virtual {v5, v0, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->a(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->q:Landroid/widget/ListView;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->z:Z

    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->w:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bd;->d()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method protected final f(Z)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->z:Z

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->q:Landroid/widget/ListView;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->y:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->r:Landroid/widget/ListView;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->y:Z

    if-eqz v3, :cond_1

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->N()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final h_()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->h_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bd;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->u:Lcom/google/android/youtube/app/ui/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/as;->b()V

    return-void
.end method

.method public final j(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bd;->j(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->s:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ek;->a(Z)V

    return-void
.end method

.method protected final m()I
    .locals 1

    const v0, 0x7f040097

    return v0
.end method

.method protected final n()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final o()V
    .locals 14

    const v0, 0x7f070142

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->o:Landroid/view/View;

    const v0, 0x7f0700f4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->p:Landroid/view/View;

    const v0, 0x7f070140

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->q:Landroid/widget/ListView;

    const v0, 0x7f070141

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->r:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v3

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->C()Lcom/google/android/youtube/app/ui/dh;

    move-result-object v6

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v7

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v8

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    move-object v0, p0

    move-object v11, p0

    invoke-static/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/bd;->a(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/bh;Lcom/google/android/youtube/app/ui/ep;)Lcom/google/android/youtube/app/ui/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v2

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v3

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->C()Lcom/google/android/youtube/app/ui/dh;

    move-result-object v6

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v7

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    move-result-object v8

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v9

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v10

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v11

    move-object v0, p0

    move-object v12, p0

    invoke-static/range {v0 .. v12}, Lcom/google/android/youtube/app/ui/as;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/ep;)Lcom/google/android/youtube/app/ui/as;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->u:Lcom/google/android/youtube/app/ui/as;

    new-instance v0, Lcom/google/android/youtube/core/a/i;

    invoke-direct {v0}, Lcom/google/android/youtube/core/a/i;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/i;->b(Lcom/google/android/youtube/core/a/e;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->q:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/google/android/youtube/core/a/i;

    invoke-direct {v0}, Lcom/google/android/youtube/core/a/i;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->u:Lcom/google/android/youtube/app/ui/as;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/i;->b(Lcom/google/android/youtube/core/a/e;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->r:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v3

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v4

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v5

    invoke-virtual {v13}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->o:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->o:Landroid/view/View;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/ui/ek;-><init>(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->s:Lcom/google/android/youtube/app/ui/ek;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->s:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->C()Lcom/google/android/youtube/app/ui/dh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/app/ui/dh;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->t:Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bd;->c()Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v1

    const v0, 0x7f070102

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    const v0, 0x7f07016a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->w:Landroid/widget/FrameLayout;

    const v0, 0x7f070177

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->x:Landroid/widget/FrameLayout;

    return-void
.end method

.method protected final p()Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .locals 1

    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->g(Z)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v0

    return-object v0
.end method

.method protected final q()Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->v:Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    return-object v0
.end method

.method protected final r()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 1

    const v0, 0x7f07013f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    return-object v0
.end method
