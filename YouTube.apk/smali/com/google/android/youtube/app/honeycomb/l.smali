.class public final Lcom/google/android/youtube/app/honeycomb/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/d;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/l;->l()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->b:Ljava/lang/Class;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const-string v0, "ancestor_classname"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/l;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private l()Ljava/lang/Class;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ancestor_classname"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->b:Ljava/lang/Class;

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;IZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;Landroid/net/Uri;IZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->a(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->a(Landroid/content/Context;Lcom/google/android/youtube/core/model/Video;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->b(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->b:Ljava/lang/Class;

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyFavoritesActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final i()V
    .locals 3

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final k()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/l;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/l;->a(Landroid/content/Intent;)V

    return-void
.end method
