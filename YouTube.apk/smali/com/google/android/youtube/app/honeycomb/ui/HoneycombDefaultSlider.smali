.class public Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;
.super Lcom/google/android/youtube/app/ui/DefaultSlider;
.source "SourceFile"


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;-><init>(Landroid/app/Activity;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b:Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->a(Z)V

    return-void
.end method

.method protected final a(I)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->h()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/youtube/app/ui/Slider$Order;->values()[Lcom/google/android/youtube/app/ui/Slider$Order;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;)Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    move-result-object v4

    sget-object v5, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->f()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->g()I

    move-result v5

    mul-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->h()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b()Lcom/google/android/youtube/app/ui/Slider$Orientation;

    move-result-object v5

    sget-object v6, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    if-ne v5, v6, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;

    move-result-object v3

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;

    move-result-object v3

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->requestLayout()V

    :cond_3
    return-void
.end method

.method protected final a(Z)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;)Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->RESIZE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;)Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->RESIZE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b:Z

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b()Lcom/google/android/youtube/app/ui/Slider$Orientation;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->a(Z)V

    invoke-super/range {p0 .. p5}, Lcom/google/android/youtube/app/ui/DefaultSlider;->onLayout(ZIIII)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0
.end method

.method public setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/HoneycombDefaultSlider;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method
