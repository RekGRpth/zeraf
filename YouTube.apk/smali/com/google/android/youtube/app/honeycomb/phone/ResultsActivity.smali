.class public Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/youtube/app/ui/aq;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/google/android/youtube/app/ui/ea;

.field private C:Lcom/google/android/youtube/app/ui/q;

.field private D:Landroid/widget/Spinner;

.field private E:Landroid/widget/Spinner;

.field private F:Lcom/google/android/youtube/app/ui/ao;

.field private G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

.field private H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

.field private I:Landroid/view/View;

.field private J:Lcom/google/android/youtube/core/ui/PagedListView;

.field private K:Lcom/google/android/youtube/core/ui/PagedListView;

.field private L:Lcom/google/android/youtube/app/ui/v;

.field private M:Landroid/provider/SearchRecentSuggestions;

.field private N:Z

.field private O:Z

.field private P:Landroid/view/inputmethod/InputMethodManager;

.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private p:Lcom/google/android/youtube/core/client/bc;

.field private q:Landroid/content/SharedPreferences;

.field private r:Lcom/google/android/youtube/core/async/au;

.field private s:Lcom/google/android/youtube/core/async/au;

.field private t:Lcom/google/android/youtube/core/client/be;

.field private u:Lcom/google/android/youtube/core/client/bg;

.field private v:Lcom/google/android/youtube/core/e;

.field private w:Lcom/google/android/youtube/gmsplus1/f;

.field private x:Lcom/google/android/youtube/app/d;

.field private y:Lcom/google/android/youtube/app/ui/bx;

.field private z:Lcom/google/android/youtube/app/ui/bx;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selected_time_filter"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->v:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method private f()V
    .locals 12

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/core/ui/PagedListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/ui/ao;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Landroid/widget/Spinner;

    const v2, 0x7f0400be

    invoke-static {p0, p0, v0, v1, v2}, Lcom/google/android/youtube/app/ui/ao;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/youtube/app/ui/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/ui/ao;

    :goto_0
    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x406

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b00f0

    const v2, 0x7f02012b

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/cc;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cc;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->t:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->u:Lcom/google/android/youtube/core/client/bg;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    new-instance v2, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, v0, v3, v6}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v0

    new-instance v3, Lcom/google/android/youtube/app/adapter/h;

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v5

    const/4 v7, 0x2

    invoke-direct {v3, v4, v5, v7, v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v4, Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {v4, v1}, Lcom/google/android/youtube/app/adapter/aj;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v1}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f040037

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    invoke-static {p0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m()V

    new-instance v0, Lcom/google/android/youtube/app/ui/ea;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->r:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v7

    sget-object v9, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->SEARCH:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->SearchResults:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->B:Lcom/google/android/youtube/app/ui/ea;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->B:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->h()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ao;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    goto/16 :goto_0
.end method

.method private g()V
    .locals 9

    const/16 v2, 0x8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->t:Lcom/google/android/youtube/core/client/be;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m()V

    new-instance v0, Lcom/google/android/youtube/app/ui/q;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->s:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/ui/q;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Lcom/google/android/youtube/app/ui/q;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->C:Lcom/google/android/youtube/app/ui/q;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/q;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->h()V

    return-void
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->P:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method private m()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->z:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F:Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ao;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3fa -> :sswitch_0
        0x406 -> :sswitch_1
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->q:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->o:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->t:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->u:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->e()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->r:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->f()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->s:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->q()Landroid/provider/SearchRecentSuggestions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->M:Landroid/provider/SearchRecentSuggestions;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/gmsplus1/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->w:Lcom/google/android/youtube/gmsplus1/f;

    return-void
.end method

.method public final synthetic a(Ljava/lang/Enum;)V
    .locals 3

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "TimeFilter"

    invoke-virtual {p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->f()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->N:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;Z)V

    :cond_0
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->L:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->a()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->m()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040081

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/cb;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cb;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/d;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->x:Lcom/google/android/youtube/app/d;

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->b()V

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->P:Landroid/view/inputmethod/InputMethodManager;

    const v0, 0x7f07005c

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->E:Landroid/widget/Spinner;

    const v0, 0x7f070067

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->J:Lcom/google/android/youtube/core/ui/PagedListView;

    const v0, 0x7f070124

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K:Lcom/google/android/youtube/core/ui/PagedListView;

    const v0, 0x7f070123

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->I:Landroid/view/View;

    const v0, 0x7f070122

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v0, 0x1090008

    invoke-direct {v1, p0, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v0, 0x1090009

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->values()[Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget v4, v4, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->nameRes:I

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->D:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->K()Lcom/google/android/youtube/app/honeycomb/phone/bb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v0

    if-ne v0, p3, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->ordinal()I

    move-result v0

    if-ne v0, p3, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->g()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    const-string v2, "is:channel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    const-string v2, "is:channel"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    const-string v2, "is:video"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    const-string v2, "is:video"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    :goto_1
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    const-string v0, "hide_query"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->N:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->q:Landroid/content/SharedPreferences;

    const-string v2, "no_search_history"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->O:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->N:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->O:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->M:Landroid/provider/SearchRecentSuggestions;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;Z)V

    :cond_3
    const-string v0, "selected_time_filter"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->H:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->CHANNEL:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    if-ne v0, v1, :cond_9

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->g()V

    :cond_5
    :goto_3
    return-void

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->A:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;->VIDEO:Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity$SearchType;

    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ResultsActivity;->f()V

    goto :goto_3
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_results"

    return-object v0
.end method
