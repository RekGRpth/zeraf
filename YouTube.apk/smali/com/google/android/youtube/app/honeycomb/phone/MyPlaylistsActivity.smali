.class public Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/bb;
.implements Lcom/google/android/youtube/core/async/bk;


# static fields
.field private static final D:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/ac;

.field private B:Lcom/google/android/youtube/core/model/Playlist;

.field private C:Lcom/google/android/youtube/core/async/by;

.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/au;

.field private p:Lcom/google/android/youtube/core/client/bc;

.field private q:Lcom/google/android/youtube/core/client/be;

.field private r:Lcom/google/android/youtube/core/client/bg;

.field private s:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private t:Lcom/google/android/youtube/core/model/UserAuth;

.field private u:Lcom/google/android/youtube/core/e;

.field private v:Lcom/google/android/youtube/app/ui/bx;

.field private w:Lcom/google/android/youtube/app/k;

.field private x:Lcom/google/android/youtube/app/ui/ba;

.field private y:Lcom/google/android/youtube/app/adapter/bm;

.field private z:Lcom/google/android/youtube/app/ui/v;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".selectedPlaylist"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->D:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;Lcom/google/android/youtube/core/model/Playlist;)Lcom/google/android/youtube/core/model/Playlist;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bk;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bk;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bl;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bl;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/n;)V

    invoke-virtual {v1, p0, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/app/ui/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/core/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 6

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bi;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bi;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V

    new-instance v1, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b0212

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/w;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->A:Lcom/google/android/youtube/app/ui/ac;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bj;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bj;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ac;->a(Lcom/google/android/youtube/app/ui/af;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_1
        0x3f5 -> :sswitch_0
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->q:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->w:Lcom/google/android/youtube/app/k;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->o()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->C:Lcom/google/android/youtube/core/async/by;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/e;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Playlist;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;Z)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ba;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->finish()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f110004

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 4

    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->f()I

    move-result v0

    const v1, 0x7f07018d

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->C:Lcom/google/android/youtube/core/async/by;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bh;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/bh;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;Lcom/google/android/youtube/core/e;)V

    const v2, 0x7f0b01be

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->w:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->d()Z

    move-result v3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->a()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->f()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    :cond_0
    const v0, 0x7f04005b

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->setContentView(I)V

    const v0, 0x7f0b0193

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->b(I)V

    new-instance v0, Lcom/google/android/youtube/app/ui/ac;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/ui/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->A:Lcom/google/android/youtube/app/ui/ac;

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x3f5

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b0211

    const v2, 0x7f0200d0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/v;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bg;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bg;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->q:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->r:Lcom/google/android/youtube/core/client/bg;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/v;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/v;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->y:Lcom/google/android/youtube/app/adapter/bm;

    invoke-static {p0, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->f()V

    const v0, 0x7f0700e6

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/PagedView;

    new-instance v0, Lcom/google/android/youtube/app/ui/ba;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/e;

    move-object v1, p0

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/ba;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/bb;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4

    const/16 v0, 0x3ee

    if-ne p1, v0, :cond_0

    const v0, 0x7f0b0212

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast p2, Landroid/app/AlertDialog;

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->D:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ba;->e()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_playlist"

    return-object v0
.end method
