.class public final Lcom/google/android/youtube/app/honeycomb/ui/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/aa;


# static fields
.field public static a:Z

.field private static final b:[Ljava/lang/String;

.field private static final c:Landroid/net/Uri;


# instance fields
.field private final A:Landroid/widget/TextView;

.field private final B:Lcom/google/android/youtube/app/ui/da;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Lcom/google/android/youtube/core/model/Video$Privacy;

.field private G:Z

.field private final H:Ljava/util/List;

.field private I:I

.field private J:Z

.field private K:J

.field private final L:Lcom/google/android/youtube/core/client/bc;

.field private M:Z

.field private N:Z

.field private O:Landroid/widget/Button;

.field private P:Lcom/google/android/youtube/app/compat/t;

.field private Q:Ljava/util/Map;

.field private R:Ljava/util/Map;

.field private S:Ljava/util/Map;

.field private T:Landroid/widget/Button;

.field private U:Landroid/view/View;

.field private V:Landroid/view/View;

.field private final d:Landroid/app/Activity;

.field private final e:Landroid/content/res/Resources;

.field private final f:Landroid/content/ContentResolver;

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Lcom/google/android/youtube/core/Analytics;

.field private final i:Lcom/google/android/youtube/app/honeycomb/ui/aa;

.field private final j:Lcom/google/android/youtube/core/transfer/x;

.field private final k:Lcom/google/android/youtube/core/e;

.field private final l:Lcom/google/android/youtube/app/compat/r;

.field private m:Lcom/google/android/youtube/core/async/n;

.field private n:Lcom/google/android/youtube/core/async/n;

.field private o:Lcom/google/android/youtube/core/model/UserAuth;

.field private p:Ljava/lang/String;

.field private q:Landroid/graphics/Bitmap;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/widget/ImageView;

.field private final u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/CheckBox;

.field private final x:Landroid/widget/EditText;

.field private final y:Landroid/widget/EditText;

.field private final z:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "longitude"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->b:[Ljava/lang/String;

    sput-boolean v3, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:Z

    const-string v0, "http://m.youtube.com/#/account_sharing"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/honeycomb/ui/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/compat/r;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->L:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "activity can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->d:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->i:Lcom/google/android/youtube/app/honeycomb/ui/aa;

    const-string v0, "errorHelper can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->k:Lcom/google/android/youtube/core/e;

    const-string v0, "menuInflater can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/r;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->l:Lcom/google/android/youtube/app/compat/r;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->e:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->f:Landroid/content/ContentResolver;

    const-string v1, "youtube"

    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->g:Landroid/content/SharedPreferences;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->h:Lcom/google/android/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->g:Landroid/content/SharedPreferences;

    const-string v1, "upload_privacy"

    sget-object v2, Lcom/google/android/youtube/core/model/Video$Privacy;->PRIVATE:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/model/Video$Privacy;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    const v0, 0x7f07002c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->r:Landroid/widget/TextView;

    const v0, 0x7f070035

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->t:Landroid/widget/ImageView;

    const v0, 0x7f070059

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->s:Landroid/widget/TextView;

    const v0, 0x7f070163

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->v:Landroid/widget/TextView;

    const v0, 0x7f070150

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->x:Landroid/widget/EditText;

    const v0, 0x7f070151

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->y:Landroid/widget/EditText;

    const v0, 0x7f070152

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->z:Landroid/widget/EditText;

    const v0, 0x7f07009f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->setPrivacy(Lcom/google/android/youtube/core/model/Video$Privacy;)V

    const v0, 0x7f070153

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->w:Landroid/widget/CheckBox;

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->Q:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->R:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->S:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->Q:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f070157

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->Q:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f07015a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->Q:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f07015d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->R:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f070158

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->R:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f07015b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->R:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f07015e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->S:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f070159

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->S:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f07015c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->S:Ljava/util/Map;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    const v0, 0x7f07015f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f070156

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->V:Landroid/view/View;

    const v0, 0x7f070154

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->U:Landroid/view/View;

    const v0, 0x7f070155

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->T:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->T:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/u;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/u;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070160

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_0
    const v0, 0x7f07009d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    const v1, 0x7f0b015f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/v;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/v;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f07009e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->A:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->A:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/da;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/w;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/w;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/app/ui/da;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->B:Lcom/google/android/youtube/app/ui/da;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ab;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/ab;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->m:Lcom/google/android/youtube/core/async/n;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ac;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/honeycomb/ui/ac;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->n:Lcom/google/android/youtube/core/async/n;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    new-instance v1, Lcom/google/android/youtube/core/transfer/x;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-direct {v1, p1, p3, v0}, Lcom/google/android/youtube/core/transfer/x;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->j:Lcom/google/android/youtube/core/transfer/x;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ad;
    .locals 8

    const/4 v6, 0x0

    const-string v0, "contentUri may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->f:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/t;->b:[Ljava/lang/String;

    const-string v3, "mime_type LIKE \'video/%\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error resolving content from URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    move-object v0, v6

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/ad;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;-><init>(B)V

    const-string v2, "_id"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/Long;)Ljava/lang/Long;

    const-string v2, "_data"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "_display_name"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "_size"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/Long;)Ljava/lang/Long;

    const-string v2, "mime_type"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->c(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "duration"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->c(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/Long;)Ljava/lang/Long;

    const-string v2, "latitude"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->d(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "longitude"

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->e(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->q:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->q:Landroid/graphics/Bitmap;

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :goto_1
    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->i(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->j(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unable to read video file ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_0

    :cond_3
    :try_start_3
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->f:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->i(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x3

    const/4 v7, 0x0

    invoke-static {v2, v3, v4, v5, v7}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    :try_start_4
    new-instance v2, Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->j(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "not a file uri ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_0

    :cond_5
    :try_start_5
    invoke-static {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;Landroid/net/Uri;)Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->f(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->k(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "video/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invalid file type ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->k(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_0

    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/t;Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ad;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ad;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Lcom/google/android/youtube/core/transfer/d;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/core/transfer/d;

    invoke-direct {v0}, Lcom/google/android/youtube/core/transfer/d;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->p:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "username"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->g(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "upload_title"

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->g(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->d(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->d(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    const-string v2, "upload_file_thumbnail"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;[B)V

    :cond_2
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->h(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, "upload_file_duration"

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->h(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;J)V

    :cond_3
    const-string v1, "upload_start_time_millis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;J)V

    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->o:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "upload_description"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->D:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "upload_keywords"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "upload_privacy"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->G:Z

    if-eqz v1, :cond_4

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/util/Pair;

    move-result-object v1

    const-string v2, "upload_location"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/t;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/t;Lcom/google/android/youtube/core/model/SocialSettings;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->V:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->U:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings;->facebook:Lcom/google/android/youtube/core/model/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings;->twitter:Lcom/google/android/youtube/core/model/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings;->orkut:Lcom/google/android/youtube/core/model/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->V:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->U:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->T:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;Lcom/google/android/youtube/core/model/k;)V
    .locals 7

    const/4 v3, 0x4

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->Q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->R:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->S:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setVisibility(I)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    if-eqz p2, :cond_1

    iget-boolean v2, p2, Lcom/google/android/youtube/core/model/k;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p2, Lcom/google/android/youtube/core/model/k;->c:Ljava/util/Set;

    new-instance v3, Lcom/google/android/youtube/core/model/SocialSettings$Action;

    sget-object v4, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->UPLOAD:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/core/model/SocialSettings$Action;-><init>(Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;Z)V

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/z;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/ui/z;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/t;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->J:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/ui/t;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->K:J

    return-wide v0
.end method

.method private static b(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/util/Pair;
    .locals 2

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/ui/t;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->N:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/ui/t;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->h:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/ui/t;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->h()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/ui/t;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->B:Lcom/google/android/youtube/app/ui/da;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/da;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/transfer/x;->a(Lcom/google/android/youtube/core/transfer/aa;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->d:Landroid/app/Activity;

    const/16 v1, 0x3fd

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method static synthetic f()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->c:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/ui/t;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->g:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "upload_privacy"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->i:Lcom/google/android/youtube/app/honeycomb/ui/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->i:Lcom/google/android/youtube/app/honeycomb/ui/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/ui/aa;->f()V

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->t:Landroid/widget/ImageView;

    return-object v0
.end method

.method private h()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->x:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->y:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->D:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->z:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->u:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->a()Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->w:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->G:Z

    sget-boolean v1, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:Z

    if-nez v1, :cond_2

    const/4 v1, 0x0

    new-array v14, v1, [Ljava/lang/String;

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/android/youtube/app/honeycomb/ui/ad;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->f(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    invoke-static {v11, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->g(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->l(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->f(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->o:Lcom/google/android/youtube/core/model/UserAuth;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->D:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->G:Z

    if-eqz v10, :cond_5

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/util/Pair;

    move-result-object v10

    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->J:Z

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v14}, Lcom/google/android/youtube/core/transfer/x;->a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/transfer/d;ZZ[Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->Q:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->id:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v14, v1

    goto/16 :goto_0

    :cond_5
    const/4 v10, 0x0

    goto :goto_1

    :cond_6
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v2, v1

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/android/youtube/app/honeycomb/ui/ad;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->f(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->g(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    move v15, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->l(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->f(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->o:Lcom/google/android/youtube/core/model/UserAuth;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->F:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->g(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->D:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->E:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->G:Z

    if-eqz v10, :cond_8

    invoke-static {v11}, Lcom/google/android/youtube/app/honeycomb/ui/t;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/util/Pair;

    move-result-object v10

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Lcom/google/android/youtube/core/transfer/d;

    move-result-object v11

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->J:Z

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v14}, Lcom/google/android/youtube/core/transfer/x;->a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/youtube/core/transfer/d;ZZ[Ljava/lang/String;)V

    move v2, v15

    goto :goto_3

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->C:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->g(Lcom/google/android/youtube/app/honeycomb/ui/ad;Ljava/lang/String;)Ljava/lang/String;

    add-int/lit8 v15, v2, 0x1

    goto :goto_4

    :cond_8
    const/4 v10, 0x0

    goto :goto_5
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method private i()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->N:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->e:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->v:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->w:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/ui/t;)Lcom/google/android/youtube/app/compat/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->s:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/honeycomb/ui/t;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/youtube/app/honeycomb/ui/t;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->k:Lcom/google/android/youtube/core/e;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->B:Lcom/google/android/youtube/app/ui/da;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/da;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->j:Lcom/google/android/youtube/core/transfer/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/x;->b()V

    return-void
.end method

.method public final a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "intent can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "userAuth can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->o:Lcom/google/android/youtube/core/model/UserAuth;

    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.youtube.intent.action.UPLOAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->q:Landroid/graphics/Bitmap;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "content://media/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Ignoring non-media-content uri: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_3
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->k:Lcom/google/android/youtube/core/e;

    const v1, 0x7f0b00d4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    :cond_7
    const-string v0, "no media content uri(s)"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_3
    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->L:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->m:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p2, v1}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :cond_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->K:J

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "UploadFormShown"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/ui/x;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    new-array v1, v2, [Ljava/util/List;

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_3
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    const v2, 0x7f0b00c7

    const/4 v1, 0x1

    const-string v0, "Error requesting location for upload"

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->k:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->c(I)Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "UploadDestinationError"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->O:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->g()V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 2

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->g()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 3

    const/4 v0, 0x0

    const v1, 0x7f070185

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->l:Lcom/google/android/youtube/app/compat/r;

    const v2, 0x7f110003

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    const v1, 0x7f07018c

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->P:Lcom/google/android/youtube/app/compat/t;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/y;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/y;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->M:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i()V

    return-void
.end method

.method public final c()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->d:Landroid/app/Activity;

    const v1, 0x7f0b0161

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->i:Lcom/google/android/youtube/app/honeycomb/ui/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->i:Lcom/google/android/youtube/app/honeycomb/ui/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/ui/aa;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->I:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->g()V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 3

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->o:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->T:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->L:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->o:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/t;->n:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->e(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    return-void
.end method
