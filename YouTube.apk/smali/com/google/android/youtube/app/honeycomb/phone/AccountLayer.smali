.class public final Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# static fields
.field private static m:Landroid/content/res/Resources;


# instance fields
.field private final b:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private c:Lcom/google/android/youtube/core/model/UserAuth;

.field private final d:Lcom/google/android/youtube/core/client/bc;

.field private final e:Lcom/google/android/youtube/core/client/be;

.field private final f:Lcom/google/android/youtube/core/e;

.field private g:Lcom/google/android/youtube/app/ui/bx;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/ImageView;

.field private final n:Lcom/google/android/youtube/core/utils/p;

.field private o:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sput-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->n:Lcom/google/android/youtube/core/utils/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->f:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/app/ui/bx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->j:Landroid/widget/TextView;

    const-string v1, "%1$,d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p1, Lcom/google/android/youtube/core/model/UserProfile;->subscribersCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->k:Landroid/widget/TextView;

    const-string v1, "%1$,d"

    new-array v2, v4, [Ljava/lang/Object;

    iget-wide v3, p1, Lcom/google/android/youtube/core/model/UserProfile;->uploadViewsCount:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/i;

    invoke-direct {v3, p0, v5}, Lcom/google/android/youtube/app/honeycomb/phone/i;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->q()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->q()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->f:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->e:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->n:Lcom/google/android/youtube/core/utils/p;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method private n()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040001

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v2, 0x7f07002d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f070030

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f070031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f070039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f07003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f07002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->p()V

    return-void
.end method

.method private p()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->o:Landroid/view/View;

    const v1, 0x7f07002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->notifyDataSetChanged()V

    return-void
.end method

.method private q()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->l:Landroid/widget/ImageView;

    const v1, 0x7f0201c2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method should only be called once."

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/bm;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v2, 0x7f040004

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/c;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/phone/c;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    new-instance v1, Lcom/google/android/youtube/app/ui/bx;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/app/ui/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f0800aa

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f0800ab

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f0800ac

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->d(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f0800af

    const v2, 0x7f0800ad

    const v3, 0x7f0800ae

    const v4, 0x7f0800b0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/bx;->a(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/a;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/a;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->values()[Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->b(Ljava/lang/Iterable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->n()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->n()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->c:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->i:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->d:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/h;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/h;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;B)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->c()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->p()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final g_()V
    .locals 0

    return-void
.end method
