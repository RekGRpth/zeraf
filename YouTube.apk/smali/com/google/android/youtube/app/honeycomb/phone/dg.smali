.class public final Lcom/google/android/youtube/app/honeycomb/phone/dg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/ui/i;


# instance fields
.field private a:Lcom/google/android/youtube/app/compat/t;

.field private b:Lcom/google/android/youtube/app/compat/t;

.field private c:Lcom/google/android/youtube/app/compat/t;

.field private d:Lcom/google/android/youtube/app/compat/t;

.field private e:Lcom/google/android/youtube/app/compat/t;

.field private f:Lcom/google/android/youtube/app/compat/t;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/google/android/youtube/core/model/VastAd;

.field private m:Lcom/google/android/youtube/core/model/Video;

.field private final n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

.field private final o:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->h:Z

    const-string v0, "actionBarMenuHelper can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {p2}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->o:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->g:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->e()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->e:Lcom/google/android/youtube/app/compat/t;

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->l:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v4, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->f:Lcom/google/android/youtube/app/compat/t;

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->l:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-interface {v4, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->k:Z

    if-eqz v0, :cond_6

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->j:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v3, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->d:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_7

    move v3, v1

    :goto_5
    invoke-interface {v4, v3}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->h:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->i:Z

    if-eqz v0, :cond_8

    :cond_2
    move v0, v1

    :goto_6
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v3, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_9

    :goto_7
    invoke-interface {v3, v1}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v3, v2

    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v1, v2

    goto :goto_7
.end method

.method private d()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->o:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->l:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->g:Z

    const v0, 0x7f070190

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a:Lcom/google/android/youtube/app/compat/t;

    const v0, 0x7f070192

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b:Lcom/google/android/youtube/app/compat/t;

    const v0, 0x7f070191

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c:Lcom/google/android/youtube/app/compat/t;

    const v0, 0x7f070193

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->d:Lcom/google/android/youtube/app/compat/t;

    const v0, 0x7f07018e

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->e:Lcom/google/android/youtube/app/compat/t;

    const v0, 0x7f07018f

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->f:Lcom/google/android/youtube/app/compat/t;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->l:Lcom/google/android/youtube/core/model/VastAd;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->d()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->m:Lcom/google/android/youtube/core/model/Video;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->l:Lcom/google/android/youtube/core/model/VastAd;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->d()V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->i:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->l:Lcom/google/android/youtube/core/model/VastAd;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->d()V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->k:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->d()V

    return-void
.end method

.method public final s()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->j:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    return-void
.end method

.method public final t()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/dg;->j:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->c()V

    return-void
.end method
