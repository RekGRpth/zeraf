.class public final Lcom/google/android/youtube/app/compat/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences$Editor;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;
    .locals 2

    instance-of v0, p2, Ljava/util/HashSet;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/util/HashSet;

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object p2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/google/android/youtube/app/compat/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    return-object p0

    :cond_0
    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/compat/ad;->b(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object p0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "putStringSet not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/youtube/app/compat/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    return-object p0
.end method

.method public final a()V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
