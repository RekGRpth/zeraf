.class final Lcom/google/android/youtube/app/compat/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/Menu;


# instance fields
.field private final a:Lcom/google/android/youtube/app/compat/m;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/compat/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/m;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->a(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->a(IIII)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->a(IIILjava/lang/CharSequence;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->a(Ljava/lang/CharSequence;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/youtube/app/compat/m;->a(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->b(I)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->b(IIII)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->b(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->b(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->a()V

    return-void
.end method

.method public final close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->b()V

    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->c()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->a(II)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/compat/m;->a(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->e(I)V

    return-void
.end method

.method public final removeItem(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/compat/m;->a(IZZ)V

    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->a(IZ)V

    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->b(IZ)V

    return-void
.end method

.method public final setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->a(Z)V

    return-void
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    return v0
.end method
