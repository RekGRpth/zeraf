.class public abstract Lcom/google/android/youtube/app/compat/SupportActionBar;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;
    .locals 3

    instance-of v0, p0, Lcom/google/android/youtube/app/compat/j;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/youtube/app/compat/j;

    invoke-interface {p0}, Lcom/google/android/youtube/app/compat/j;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->c(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "support action bar not available for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->c(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/youtube/app/compat/i;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/app/compat/c;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/compat/c;-><init>(Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sdk < 11 should implement SupportActionBarActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/app/compat/a;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/compat/a;-><init>(Landroid/app/ActionBar;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public abstract a(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/compat/h;)V
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a(Z)V
.end method

.method public a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract b()V
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Z)V
.end method

.method public b(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract c()Z
.end method
