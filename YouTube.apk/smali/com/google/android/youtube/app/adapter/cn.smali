.class public Lcom/google/android/youtube/app/adapter/cn;
.super Lcom/google/android/youtube/core/a/m;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/adapter/cn;->b:Lcom/google/android/youtube/core/a/g;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/m;-><init>(Lcom/google/android/youtube/core/a/g;)V

    const-string v0, "view cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cn;->a:Landroid/view/View;

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/app/adapter/cn;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cn;->a:Landroid/view/View;

    return-object v0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cn;->a:Landroid/view/View;

    return-object v0
.end method
