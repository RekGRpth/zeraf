.class final Lcom/google/android/youtube/app/adapter/cf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field public final a:Landroid/widget/ProgressBar;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field final synthetic d:Lcom/google/android/youtube/app/adapter/ce;

.field private final e:Lcom/google/android/youtube/app/adapter/bl;

.field private final f:Lcom/google/android/youtube/app/adapter/bl;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/ce;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/youtube/app/adapter/bl;Lcom/google/android/youtube/app/adapter/bl;)V
    .locals 0

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/youtube/app/adapter/cf;-><init>(Lcom/google/android/youtube/app/adapter/ce;Landroid/view/View;Lcom/google/android/youtube/app/adapter/bl;Lcom/google/android/youtube/app/adapter/bl;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/app/adapter/ce;Landroid/view/View;Lcom/google/android/youtube/app/adapter/bl;Lcom/google/android/youtube/app/adapter/bl;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/cf;->d:Lcom/google/android/youtube/app/adapter/ce;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/cf;->e:Lcom/google/android/youtube/app/adapter/bl;

    iput-object p4, p0, Lcom/google/android/youtube/app/adapter/cf;->f:Lcom/google/android/youtube/app/adapter/bl;

    const v0, 0x7f070090

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    const v1, 0x7f07003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->a:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    const v1, 0x7f070163

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->b:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    const v1, 0x7f070164

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->c:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    const v1, 0x7f07005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->i:Landroid/widget/TextView;

    return-void
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->a:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->b:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(Landroid/view/View;I)V

    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->i:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->g:Landroid/view/View;

    const v1, 0x7f070092

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(Landroid/view/View;I)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/youtube/app/ui/cz;

    invoke-virtual {p2}, Lcom/google/android/youtube/app/ui/cz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/adapter/cf;->a(I)V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/adapter/cf;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->e:Lcom/google/android/youtube/app/adapter/bl;

    iget-object v1, p2, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/adapter/cf;->a(I)V

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/adapter/cf;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cf;->f:Lcom/google/android/youtube/app/adapter/bl;

    iget-object v1, p2, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
