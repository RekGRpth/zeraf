.class public final Lcom/google/android/youtube/app/adapter/bb;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    new-instance v0, Lcom/google/android/youtube/app/adapter/bc;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bc;-><init>(Lcom/google/android/youtube/app/adapter/bb;Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bb;->a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bb;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bb;->a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/bb;
    .locals 2

    const-string v0, "thumbnailSize cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/adapter/bb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/youtube/app/adapter/bb;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/adapter/bd;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/bd;-><init>(Lcom/google/android/youtube/app/adapter/bb;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
