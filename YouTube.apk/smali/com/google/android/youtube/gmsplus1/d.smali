.class final Lcom/google/android/youtube/gmsplus1/d;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/gmsplus1/a;

.field private final b:Lvedroid/support/v4/app/FragmentActivity;

.field private final c:Landroid/accounts/Account;

.field private final d:Lcom/google/android/youtube/core/async/bk;

.field private final e:Z

.field private f:I

.field private g:Landroid/content/Intent;

.field private h:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/a;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/gmsplus1/d;-><init>(Lcom/google/android/youtube/gmsplus1/a;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/a;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/gmsplus1/d;->f:I

    iput-object p2, p0, Lcom/google/android/youtube/gmsplus1/d;->b:Lvedroid/support/v4/app/FragmentActivity;

    iput-object p3, p0, Lcom/google/android/youtube/gmsplus1/d;->c:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    iput-boolean p5, p0, Lcom/google/android/youtube/gmsplus1/d;->e:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/gmsplus1/d;)Lcom/google/android/youtube/core/async/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    return-object v0
.end method

.method private varargs a()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    invoke-static {v1}, Lcom/google/android/youtube/gmsplus1/a;->a(Lcom/google/android/youtube/gmsplus1/a;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/d;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    iget-object v3, v3, Lcom/google/android/youtube/gmsplus1/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/auth/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/d;->b:Lvedroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/auth/GooglePlayServicesAvailabilityException;->getConnectionStatusCode()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/gmsplus1/d;->f:I

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->h:Ljava/lang/Exception;

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/d;->b:Lvedroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->g:Landroid/content/Intent;

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->h:Ljava/lang/Exception;

    goto :goto_0

    :catch_2
    move-exception v1

    iput-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->h:Ljava/lang/Exception;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->c:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/d;->b:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    iget-boolean v4, p0, Lcom/google/android/youtube/gmsplus1/d;->e:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/gmsplus1/a;->a(Landroid/accounts/Account;Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/async/bk;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/d;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    new-instance v1, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/d;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    iget-object v3, v3, Lcom/google/android/youtube/gmsplus1/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/async/bk;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/gmsplus1/d;->f:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    invoke-static {v0, p0}, Lcom/google/android/youtube/gmsplus1/a;->a(Lcom/google/android/youtube/gmsplus1/a;Lcom/google/android/youtube/gmsplus1/d;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->b:Lvedroid/support/v4/app/FragmentActivity;

    iget v2, p0, Lcom/google/android/youtube/gmsplus1/d;->f:I

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/gmsplus1/e;->a(Lcom/google/android/youtube/gmsplus1/a;Lvedroid/support/v4/app/FragmentActivity;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->g:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->a:Lcom/google/android/youtube/gmsplus1/a;

    invoke-static {v0, p0}, Lcom/google/android/youtube/gmsplus1/a;->a(Lcom/google/android/youtube/gmsplus1/a;Lcom/google/android/youtube/gmsplus1/d;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->b:Lvedroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->g:Landroid/content/Intent;

    const/16 v2, 0x388

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->h:Ljava/lang/Exception;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GetToken error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->h:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/d;->c:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/gmsplus1/d;->h:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    const-string v0, "GetToken error: could not get token and no exception"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/d;->d:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V

    goto :goto_0
.end method
