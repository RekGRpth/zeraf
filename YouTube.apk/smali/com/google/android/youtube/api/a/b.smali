.class public final Lcom/google/android/youtube/api/a/b;
.super Lcom/google/android/youtube/api/jar/client/a;
.source "SourceFile"


# instance fields
.field private final i:Lcom/google/android/youtube/api/ApiPlayer;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/core/player/af;)V
    .locals 12

    new-instance v0, Lcom/google/android/youtube/api/jar/a;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/api/jar/a;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/youtube/api/jar/client/a;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)V

    const-string v0, "apiEnvironment cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p4

    check-cast v0, Landroid/view/View;

    invoke-virtual {p3, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setVideoView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/youtube/api/ApiPlayer;

    new-instance v2, Lcom/google/android/youtube/api/a/c;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/api/a/c;-><init>(Lcom/google/android/youtube/api/a/b;B)V

    iget-object v6, p0, Lcom/google/android/youtube/api/a/b;->d:Lcom/google/android/youtube/core/player/overlay/a;

    iget-object v7, p0, Lcom/google/android/youtube/api/a/b;->e:Lcom/google/android/youtube/core/player/overlay/c;

    iget-object v8, p0, Lcom/google/android/youtube/api/a/b;->c:Lcom/google/android/youtube/api/jar/b;

    iget-object v9, p0, Lcom/google/android/youtube/api/a/b;->f:Lcom/google/android/youtube/core/player/overlay/i;

    iget-object v10, p0, Lcom/google/android/youtube/api/a/b;->g:Lcom/google/android/youtube/core/player/overlay/aa;

    iget-object v11, p0, Lcom/google/android/youtube/api/a/b;->h:Lcom/google/android/youtube/core/player/overlay/ab;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/api/ApiPlayer;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/s;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/af;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/player/overlay/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/api/j;Z)V
    .locals 2

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;)V

    if-eqz p3, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;-><init>(Landroid/content/Context;)V

    :goto_0
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/youtube/api/a/b;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/core/player/af;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->Q()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/a/b;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/a/b;->g(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/a/b;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/a/b;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/a/b;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/a/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/a/b;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/a/b;->j(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->R()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/api/a/b;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/a/b;->i(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->S()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/api/a/b;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/a/b;->k(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->T()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->U()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->L()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->V()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->W()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->O()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->X()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->P()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->Y()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->P()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->Z()V

    return-void
.end method

.method static synthetic o(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->K()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->M()V

    return-void
.end method

.method static synthetic q(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/a/b;->N()V

    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->f()Z

    move-result v0

    return v0
.end method

.method public final B()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->g()V

    return-void
.end method

.method public final C()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->h()V

    return-void
.end method

.method public final D()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->i()I

    move-result v0

    return v0
.end method

.method public final E()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->j()I

    move-result v0

    return v0
.end method

.method public final F()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->n()V

    return-void
.end method

.method public final G()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->o()V

    return-void
.end method

.method public final H()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->m()V

    return-void
.end method

.method public final I()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->l()Z

    move-result v0

    return v0
.end method

.method public final J()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->a()V

    return-void
.end method

.method protected final a([B)Z
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    array-length v0, p1

    invoke-virtual {v1, p1, v2, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    const-class v0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;

    iget-object v2, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer$PlayerState;)V

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    const/4 v0, 0x1

    return v0
.end method

.method public final aa()Lcom/google/android/youtube/core/player/Director;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->p()Lcom/google/android/youtube/core/player/Director;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/api/ApiPlayer;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public final c(Ljava/lang/String;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/api/ApiPlayer;->a(Ljava/lang/String;II)V

    return-void
.end method

.method public final c(Ljava/util/List;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/api/ApiPlayer;->a(Ljava/util/List;II)V

    return-void
.end method

.method public final c(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/api/ApiPlayer;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/api/ApiPlayer;->b(Ljava/lang/String;I)V

    return-void
.end method

.method public final d(Ljava/lang/String;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/api/ApiPlayer;->b(Ljava/lang/String;II)V

    return-void
.end method

.method public final d(Ljava/util/List;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/api/ApiPlayer;->b(Ljava/util/List;II)V

    return-void
.end method

.method public final d(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/api/ApiPlayer;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->a(I)V

    return-void
.end method

.method public final f(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->b(I)V

    return-void
.end method

.method public final f(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->a(Z)V

    return-void
.end method

.method public final g(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->b(Z)V

    return-void
.end method

.method public final h(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->c(Z)V

    return-void
.end method

.method protected final u()[B
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->q()Lcom/google/android/youtube/api/ApiPlayer$PlayerState;

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final v()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->b()V

    return-void
.end method

.method public final w()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->c()V

    return-void
.end method

.method public final x()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->k()V

    return-void
.end method

.method public final y()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d()Z

    move-result v0

    return v0
.end method

.method public final z()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/b;->i:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->e()Z

    move-result v0

    return v0
.end method
