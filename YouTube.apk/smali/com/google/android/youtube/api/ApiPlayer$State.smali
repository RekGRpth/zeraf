.class final enum Lcom/google/android/youtube/api/ApiPlayer$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/api/ApiPlayer$State;

.field public static final enum DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

.field public static final enum LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

.field public static final enum LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

.field public static final enum UNINITIALIZED:Lcom/google/android/youtube/api/ApiPlayer$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/api/ApiPlayer$State;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/api/ApiPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->UNINITIALIZED:Lcom/google/android/youtube/api/ApiPlayer$State;

    new-instance v0, Lcom/google/android/youtube/api/ApiPlayer$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/api/ApiPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    new-instance v0, Lcom/google/android/youtube/api/ApiPlayer$State;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/api/ApiPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    new-instance v0, Lcom/google/android/youtube/api/ApiPlayer$State;

    const-string v1, "DESTROYED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/api/ApiPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->UNINITIALIZED:Lcom/google/android/youtube/api/ApiPlayer$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->$VALUES:[Lcom/google/android/youtube/api/ApiPlayer$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/api/ApiPlayer$State;
    .locals 1

    const-class v0, Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/ApiPlayer$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/api/ApiPlayer$State;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->$VALUES:[Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-virtual {v0}, [Lcom/google/android/youtube/api/ApiPlayer$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/api/ApiPlayer$State;

    return-object v0
.end method
