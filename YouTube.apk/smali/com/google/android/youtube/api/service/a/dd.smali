.class public final Lcom/google/android/youtube/api/service/a/dd;
.super Lcom/google/android/youtube/api/service/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/api/service/a/cx;


# instance fields
.field private final b:Lcom/google/android/youtube/api/service/a/cw;

.field private c:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/service/a/cw;Lcom/google/android/youtube/api/jar/client/by;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/youtube/api/service/a/a;-><init>(Lcom/google/android/youtube/api/jar/client/by;)V

    const-string v0, "surfaceTextureService cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/a/cw;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->b:Lcom/google/android/youtube/api/service/a/cw;

    invoke-virtual {p1, p0}, Lcom/google/android/youtube/api/service/a/cw;->a(Lcom/google/android/youtube/api/service/a/cx;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Surface;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->a()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaPlayer should only be attached after Surface has been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/view/Surface;)V

    return-void
.end method

.method public final a_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->b()V

    :cond_0
    return-void
.end method

.method public final b_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->b()V

    :cond_0
    return-void
.end method

.method public final c_()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->c()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/dd;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
