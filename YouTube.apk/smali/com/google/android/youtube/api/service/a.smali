.class public final Lcom/google/android/youtube/api/service/a;
.super Lcom/google/android/youtube/player/internal/ac;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/youtube/api/service/h;

.field private final c:Lcom/google/android/youtube/api/v;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/player/internal/y;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/ac;-><init>()V

    const-string v0, "apiEnvironment cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "client cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/api/service/h;

    invoke-direct {v0, p0, p3}, Lcom/google/android/youtube/api/service/h;-><init>(Lcom/google/android/youtube/api/service/a;Lcom/google/android/youtube/player/internal/y;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a;->b:Lcom/google/android/youtube/api/service/h;

    new-instance v0, Lcom/google/android/youtube/api/v;

    iget-object v1, p0, Lcom/google/android/youtube/api/service/a;->b:Lcom/google/android/youtube/api/service/h;

    invoke-direct {v0, v1, p2}, Lcom/google/android/youtube/api/v;-><init>(Lcom/google/android/youtube/api/w;Lcom/google/android/youtube/api/j;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a;->c:Lcom/google/android/youtube/api/v;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a;)Lcom/google/android/youtube/api/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->c:Lcom/google/android/youtube/api/v;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/service/a;)Lcom/google/android/youtube/api/service/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->b:Lcom/google/android/youtube/api/service/h;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/d;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/d;-><init>(Lcom/google/android/youtube/api/service/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/b;-><init>(Lcom/google/android/youtube/api/service/a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/service/c;-><init>(Lcom/google/android/youtube/api/service/a;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/e;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/e;-><init>(Lcom/google/android/youtube/api/service/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/f;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/f;-><init>(Lcom/google/android/youtube/api/service/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/g;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/g;-><init>(Lcom/google/android/youtube/api/service/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
