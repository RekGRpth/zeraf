.class public abstract Lcom/google/android/youtube/api/service/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/af;


# instance fields
.field protected a:Lcom/google/android/youtube/core/player/ag;

.field private b:Lcom/google/android/youtube/api/jar/client/by;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/jar/client/by;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/by;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/service/a/a;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    return-void
.end method

.method public final a(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/by;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/by;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->a:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->c()V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/by;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/youtube/core/player/ag;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ag;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->a:Lcom/google/android/youtube/core/player/ag;

    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/ah;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Zoom not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setVideoSize(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/a;->b:Lcom/google/android/youtube/api/jar/client/by;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/by;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setZoom(I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Zoom not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
