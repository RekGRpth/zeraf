.class final Lcom/google/android/youtube/api/service/a/ch;
.super Lcom/google/android/youtube/api/service/a/au;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/j;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/youtube/core/player/overlay/j;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/au;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/ch;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/ch;)Lcom/google/android/youtube/core/player/overlay/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ch;->b:Lcom/google/android/youtube/core/player/overlay/j;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/overlay/j;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/j;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/ch;->b:Lcom/google/android/youtube/core/player/overlay/j;

    return-void
.end method

.method public final v_()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ch;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/ci;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/ci;-><init>(Lcom/google/android/youtube/api/service/a/ch;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
