.class final Lcom/google/android/youtube/api/service/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/jar/client/bm;

.field final synthetic b:Lcom/google/android/youtube/api/jar/client/cb;

.field final synthetic c:Lcom/google/android/youtube/api/jar/client/ch;

.field final synthetic d:Lcom/google/android/youtube/api/jar/client/ck;

.field final synthetic e:Lcom/google/android/youtube/api/jar/client/by;

.field final synthetic f:Lcom/google/android/youtube/api/jar/client/bj;

.field final synthetic g:Lcom/google/android/youtube/api/jar/client/bp;

.field final synthetic h:Lcom/google/android/youtube/api/jar/client/bs;

.field final synthetic i:Lcom/google/android/youtube/api/jar/client/bv;

.field final synthetic j:Lcom/google/android/youtube/api/jar/client/ce;

.field final synthetic k:Lcom/google/android/youtube/api/jar/client/cn;

.field final synthetic l:Z

.field final synthetic m:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic n:Landroid/os/ConditionVariable;

.field final synthetic o:Lcom/google/android/youtube/api/service/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/api/service/a/b;Lcom/google/android/youtube/api/jar/client/bm;Lcom/google/android/youtube/api/jar/client/cb;Lcom/google/android/youtube/api/jar/client/ch;Lcom/google/android/youtube/api/jar/client/ck;Lcom/google/android/youtube/api/jar/client/by;Lcom/google/android/youtube/api/jar/client/bj;Lcom/google/android/youtube/api/jar/client/bp;Lcom/google/android/youtube/api/jar/client/bs;Lcom/google/android/youtube/api/jar/client/bv;Lcom/google/android/youtube/api/jar/client/ce;Lcom/google/android/youtube/api/jar/client/cn;ZLjava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/c;->o:Lcom/google/android/youtube/api/service/a/b;

    iput-object p2, p0, Lcom/google/android/youtube/api/service/a/c;->a:Lcom/google/android/youtube/api/jar/client/bm;

    iput-object p3, p0, Lcom/google/android/youtube/api/service/a/c;->b:Lcom/google/android/youtube/api/jar/client/cb;

    iput-object p4, p0, Lcom/google/android/youtube/api/service/a/c;->c:Lcom/google/android/youtube/api/jar/client/ch;

    iput-object p5, p0, Lcom/google/android/youtube/api/service/a/c;->d:Lcom/google/android/youtube/api/jar/client/ck;

    iput-object p6, p0, Lcom/google/android/youtube/api/service/a/c;->e:Lcom/google/android/youtube/api/jar/client/by;

    iput-object p7, p0, Lcom/google/android/youtube/api/service/a/c;->f:Lcom/google/android/youtube/api/jar/client/bj;

    iput-object p8, p0, Lcom/google/android/youtube/api/service/a/c;->g:Lcom/google/android/youtube/api/jar/client/bp;

    iput-object p9, p0, Lcom/google/android/youtube/api/service/a/c;->h:Lcom/google/android/youtube/api/jar/client/bs;

    iput-object p10, p0, Lcom/google/android/youtube/api/service/a/c;->i:Lcom/google/android/youtube/api/jar/client/bv;

    iput-object p11, p0, Lcom/google/android/youtube/api/service/a/c;->j:Lcom/google/android/youtube/api/jar/client/ce;

    iput-object p12, p0, Lcom/google/android/youtube/api/service/a/c;->k:Lcom/google/android/youtube/api/jar/client/cn;

    iput-boolean p13, p0, Lcom/google/android/youtube/api/service/a/c;->l:Z

    iput-object p14, p0, Lcom/google/android/youtube/api/service/a/c;->m:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p15, p0, Lcom/google/android/youtube/api/service/a/c;->n:Landroid/os/ConditionVariable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 18

    new-instance v1, Lcom/google/android/youtube/api/service/a/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/service/a/c;->o:Lcom/google/android/youtube/api/service/a/b;

    invoke-static {v2}, Lcom/google/android/youtube/api/service/a/b;->a(Lcom/google/android/youtube/api/service/a/b;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/service/a/c;->o:Lcom/google/android/youtube/api/service/a/b;

    invoke-static {v3}, Lcom/google/android/youtube/api/service/a/b;->b(Lcom/google/android/youtube/api/service/a/b;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/service/a/c;->o:Lcom/google/android/youtube/api/service/a/b;

    invoke-static {v4}, Lcom/google/android/youtube/api/service/a/b;->c(Lcom/google/android/youtube/api/service/a/b;)Lcom/google/android/youtube/api/service/k;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/service/a/c;->o:Lcom/google/android/youtube/api/service/a/b;

    invoke-static {v5}, Lcom/google/android/youtube/api/service/a/b;->d(Lcom/google/android/youtube/api/service/a/b;)Lcom/google/android/youtube/api/j;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/service/a/c;->a:Lcom/google/android/youtube/api/jar/client/bm;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/api/service/a/c;->b:Lcom/google/android/youtube/api/jar/client/cb;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/api/service/a/c;->c:Lcom/google/android/youtube/api/jar/client/ch;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/api/service/a/c;->d:Lcom/google/android/youtube/api/jar/client/ck;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/api/service/a/c;->e:Lcom/google/android/youtube/api/jar/client/by;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/api/service/a/c;->f:Lcom/google/android/youtube/api/jar/client/bj;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/api/service/a/c;->g:Lcom/google/android/youtube/api/jar/client/bp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/api/service/a/c;->h:Lcom/google/android/youtube/api/jar/client/bs;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/api/service/a/c;->i:Lcom/google/android/youtube/api/jar/client/bv;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/api/service/a/c;->j:Lcom/google/android/youtube/api/jar/client/ce;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/api/service/a/c;->k:Lcom/google/android/youtube/api/jar/client/cn;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/api/service/a/c;->l:Z

    move/from16 v17, v0

    invoke-direct/range {v1 .. v17}, Lcom/google/android/youtube/api/service/a/d;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/service/k;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/api/jar/client/bm;Lcom/google/android/youtube/api/jar/client/cb;Lcom/google/android/youtube/api/jar/client/ch;Lcom/google/android/youtube/api/jar/client/ck;Lcom/google/android/youtube/api/jar/client/by;Lcom/google/android/youtube/api/jar/client/bj;Lcom/google/android/youtube/api/jar/client/bp;Lcom/google/android/youtube/api/jar/client/bs;Lcom/google/android/youtube/api/jar/client/bv;Lcom/google/android/youtube/api/jar/client/ce;Lcom/google/android/youtube/api/jar/client/cn;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/service/a/c;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/api/service/a/c;->n:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method
