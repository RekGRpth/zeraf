.class final Lcom/google/android/youtube/api/jar/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/q;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/e;-><init>(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->f(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ControllerBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->g(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->c()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->f(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ControllerBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerBar;->setScrubberTime(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->g(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->setScrubberTime(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->h(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->h(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->h(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;->ENDED:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->k()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->f(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ControllerBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerBar;->setScrubberTime(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->g(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->setScrubberTime(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->f(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ControllerBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->g(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ah;->a(I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->h(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->h(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;->ENDED:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->k()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->h(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->b()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->i(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/e;->a:Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;)Lcom/google/android/youtube/api/jar/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ah;->g()V

    :cond_0
    return-void
.end method
