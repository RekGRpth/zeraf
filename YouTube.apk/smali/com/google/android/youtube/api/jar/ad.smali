.class public final Lcom/google/android/youtube/api/jar/ad;
.super Lcom/google/android/youtube/api/jar/af;
.source "SourceFile"


# instance fields
.field private final n:Landroid/graphics/Paint;

.field private final o:Landroid/graphics/Rect;

.field private final p:Landroid/graphics/drawable/Drawable;

.field private final q:Landroid/graphics/drawable/Drawable;

.field private final r:Landroid/graphics/drawable/Drawable;

.field private final s:Landroid/graphics/RectF;

.field private final t:Landroid/graphics/RectF;

.field private final u:Landroid/graphics/Rect;

.field private final v:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/af;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->o:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->u:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->s:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->t:Landroid/graphics/RectF;

    const v0, 0x7f020019

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->r:Landroid/graphics/drawable/Drawable;

    const v0, 0x7f02001a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->q:Landroid/graphics/drawable/Drawable;

    const v0, 0x7f02001c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->p:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->p:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    const/16 v9, 0x63

    const/16 v2, 0x62

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->l:I

    if-gt v0, v8, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    mul-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x62

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->o:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v7

    iget-object v7, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5, v6, v3, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget v3, p0, Lcom/google/android/youtube/api/jar/ad;->m:I

    if-gt v3, v8, :cond_4

    move v2, v1

    :cond_0
    :goto_1
    if-le v2, v0, :cond_1

    sub-int v0, v2, v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    mul-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x62

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->q:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ad;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v4

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->l:I

    if-lt v0, v9, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->l:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/google/android/youtube/api/jar/ad;->m:I

    if-ge v3, v9, :cond_0

    iget v2, p0, Lcom/google/android/youtube/api/jar/ad;->m:I

    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 8

    const/16 v7, 0x64

    const/high16 v2, 0x42b40000

    const/high16 v6, -0x3d4c0000

    const/4 v4, 0x1

    const/high16 v3, 0x43340000

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->l:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->k:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->s:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->k:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->o:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->l:I

    if-lt v0, v7, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->k:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->t:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->m:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/youtube/api/jar/ad;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->s:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/api/jar/ad;->m:I

    if-lt v0, v7, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/youtube/api/jar/ad;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ad;->t:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ad;->n:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->p:Landroid/graphics/drawable/Drawable;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ad;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ad;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ad;->u:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ad;->getState()[I

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/api/jar/ad;->a(Landroid/graphics/Rect;[I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->s:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ad;->t:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ad;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ad;->a()V

    return-void
.end method
