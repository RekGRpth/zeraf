.class public final Lcom/google/android/youtube/api/jar/client/dt;
.super Lcom/google/android/youtube/api/jar/client/cf;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/aa;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/aa;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/cf;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/aa;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->a:Lcom/google/android/youtube/core/player/overlay/aa;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/dt;)Lcom/google/android/youtube/core/player/overlay/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->a:Lcom/google/android/youtube/core/player/overlay/aa;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/dv;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/dv;-><init>(Lcom/google/android/youtube/api/jar/client/dt;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/dx;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/dx;-><init>(Lcom/google/android/youtube/api/jar/client/dt;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/du;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/du;-><init>(Lcom/google/android/youtube/api/jar/client/dt;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dt;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/dw;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/dw;-><init>(Lcom/google/android/youtube/api/jar/client/dt;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
