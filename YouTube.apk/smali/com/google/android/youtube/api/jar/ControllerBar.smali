.class public Lcom/google/android/youtube/api/jar/ControllerBar;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

.field private final e:Landroid/view/View;

.field private final f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

.field private final g:Landroid/widget/ImageButton;

.field private final h:Landroid/widget/ImageButton;

.field private i:Lcom/google/android/youtube/core/player/overlay/e;

.field private j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/q;Lcom/google/android/youtube/api/jar/ae;)V
    .locals 7

    const/4 v6, 0x0

    const/high16 v3, 0x3f000000

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const-string v0, "optionsViewListener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x42480000

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->a:I

    const/high16 v2, 0x42340000

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->b:I

    const/high16 v2, 0x40e00000

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    new-instance v1, Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-direct {v1, p1, p2}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/q;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    const v2, 0x7f020019

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-direct {v1, p1, p3}, Lcom/google/android/youtube/api/jar/NormalTimeBar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/ae;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    const v2, 0x7f020013

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    const-string v2, "LIVE"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    const/high16 v2, 0x41800000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    const v3, 0x7f020015

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    const v3, 0x7f020014

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    iget v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget v4, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget v5, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    const v2, 0x7f0b0094

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_SELECTED_STATE_SET:[I

    const v3, 0x7f020017

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->ENABLED_SELECTED_STATE_SET:[I

    const v3, 0x7f020016

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    const v3, 0x7f020012

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    const v3, 0x7f020011

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget v4, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    const v1, 0x7f0b008a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerBar;->addView(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/view/View;I)I
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setVisibility(I)V

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->a:I

    return v0
.end method

.method public final c()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->a:I

    iget v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->b:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    return v0
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getLocationInWindow([I)V

    aget v1, v0, v3

    aget v2, v0, v6

    aget v3, v0, v3

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    aget v0, v0, v6

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getBottom()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getTop()I

    move-result v4

    sub-int v4, v0, v4

    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    return v6
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->i:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "listener not set for ControllerOverlay"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->i:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/e;->b(Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a()V

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    sub-int v0, p4, p2

    sub-int v1, p5, p3

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v4, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v3}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v4, v3, v0, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->layout(IIII)V

    iget v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/api/jar/ControllerBar;->a(Landroid/view/View;I)I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    iget v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/api/jar/ControllerBar;->a(Landroid/view/View;I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/api/jar/ControllerBar;->a(Landroid/view/View;I)I

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/api/jar/ControllerBar;->a(Landroid/view/View;I)I

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    const/high16 v5, 0x40000000

    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerBar;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->c()I

    move-result v1

    invoke-static {v1, p2}, Lcom/google/android/youtube/api/jar/ControllerBar;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/api/jar/ControllerBar;->setMeasuredDimension(II)V

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    iget v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->b:I

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->measure(II)V

    iget v2, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->a:I

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->a:I

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->e:Landroid/view/View;

    invoke-virtual {v4, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageButton;->measure(II)V

    iget v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->c:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    :cond_0
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v1, v0, v3}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->measure(II)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3}, Landroid/widget/TextView;->measure(II)V

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setCcEnabled(Z)V

    return-void
.end method

.method public setControllerListener(Lcom/google/android/youtube/core/player/overlay/e;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->i:Lcom/google/android/youtube/core/player/overlay/e;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setControllerListener(Lcom/google/android/youtube/core/player/overlay/e;)V

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerBar;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f0b008b

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f0b008a

    goto :goto_0
.end method

.method public setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setHq(Z)V

    return-void
.end method

.method public setHQisHD(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setHQisHD(Z)V

    return-void
.end method

.method public setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setHasCc(Z)V

    return-void
.end method

.method public setScrubberTime(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setScrubberTime(I)V

    return-void
.end method

.method public setScrubbing(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setScrubbing(Z)V

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setEnabled(Z)V

    return-void
.end method

.method public setShowFullscreenButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->h:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->LIVE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setSupportsQualityToggle(Z)V

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->f:Lcom/google/android/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setTimes(III)V

    return-void
.end method

.method public setVideoTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerBar;->d:Lcom/google/android/youtube/api/jar/ControllerOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setVideoTitle(Ljava/lang/String;)V

    return-void
.end method
