.class public Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/api/jar/b;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Lcom/google/android/youtube/api/jar/z;

.field private final c:Landroid/view/View;

.field private final d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/Button;

.field private final i:Landroid/widget/Button;

.field private final j:Lcom/google/android/youtube/core/player/overlay/x;

.field private final k:I

.field private final l:Landroid/os/Handler;

.field private m:Lcom/google/android/youtube/core/player/overlay/e;

.field private n:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

.field private o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Z

.field private final u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x59

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x7f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x7e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/api/jar/z;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string v0, "parentView cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->c:Landroid/view/View;

    const-string v0, "layoutPolice cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/z;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b:Lcom/google/android/youtube/api/jar/z;

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->UNINITIALIZED:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04000e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->k:I

    new-instance v0, Lcom/google/android/youtube/api/jar/i;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/api/jar/i;-><init>(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->l:Landroid/os/Handler;

    const v0, 0x7f070044

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    const v0, 0x7f070043

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->e:Landroid/view/View;

    const v0, 0x7f070040

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->f:Landroid/view/View;

    const v0, 0x7f070041

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g:Landroid/widget/TextView;

    const v0, 0x7f070042

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/api/jar/k;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lcom/google/android/youtube/api/jar/k;-><init>(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;B)V

    const v0, 0x7f070046

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/o;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setMediaActionHelper(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    const v1, 0x7f070050

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    const v1, 0x7f070045

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->i:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/x;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->j:Lcom/google/android/youtube/core/player/overlay/x;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    return-object v0
.end method

.method private a(I)Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;
    .locals 2

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x55

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3e

    if-ne p1, v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->s:I

    return v0
.end method

.method private j()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->l:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->l:Landroid/os/Handler;

    iget v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->k:I

    int-to-long v1, v1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    :goto_0
    iput-boolean p2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->r:Z

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->j:Lcom/google/android/youtube/core/player/overlay/x;

    new-instance v1, Lcom/google/android/youtube/api/jar/j;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/j;-><init>(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/player/overlay/x;->a(Ljava/util/List;Lcom/google/android/youtube/core/player/overlay/z;)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final d()V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->l:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->j()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ENDED:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a()V

    return-void
.end method

.method public final g()V
    .locals 7

    const/4 v2, 0x1

    const/16 v4, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->q:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v3, v6, :cond_4

    sget-object v3, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    :goto_1
    invoke-virtual {v5, v3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setPlaybackState(Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;)V

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->e:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v3, v6, :cond_5

    move v3, v1

    :goto_2
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->f:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v3, v6, :cond_6

    move v3, v1

    :goto_3
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v5, v6, :cond_7

    :goto_4
    invoke-virtual {v3, v2}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setErrorState(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v5, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-eq v3, v5, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v5, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->UNINITIALIZED:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-eq v3, v5, :cond_1

    move v4, v1

    :cond_1
    invoke-virtual {v2, v4}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setVisibility(I)V

    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->p:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v3, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-eq v2, v3, :cond_8

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    sget-object v3, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->ERROR:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-eq v2, v3, :cond_8

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h()V

    :goto_5
    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->h()V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    goto :goto_1

    :cond_5
    move v3, v4

    goto :goto_2

    :cond_6
    move v3, v4

    goto :goto_3

    :cond_7
    move v2, v1

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setFocus(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    :cond_9
    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->q:Z

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->setVisibility(I)V

    goto :goto_5
.end method

.method public final h()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->q:Z

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->i()V

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->j:Lcom/google/android/youtube/core/player/overlay/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/x;->a()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->r:Z

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->r:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->j()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->i:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->t:Z

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    iget-boolean v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->t:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/e;->b(Z)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a(I)Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->n:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    goto :goto_1

    :cond_3
    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1

    :sswitch_0
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->q:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1

    :sswitch_1
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->q:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h()V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a(I)Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->q:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setCcEnabled(Z)V

    return-void
.end method

.method public setControlsPermanentlyHidden(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->p:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->h()V

    :cond_0
    return-void
.end method

.method public setFullscreen(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->t:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    return-void
.end method

.method public setHQ(Z)V
    .locals 0

    return-void
.end method

.method public setHQisHD(Z)V
    .locals 0

    return-void
.end method

.method public setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setShowCcButton(Z)V

    return-void
.end method

.method public setHasNext(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setNextEnabled(Z)V

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setPreviousEnabled(Z)V

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/overlay/e;)V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/api/jar/ah;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b:Lcom/google/android/youtube/api/jar/z;

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/api/jar/ah;-><init>(Lcom/google/android/youtube/core/player/overlay/e;Lcom/google/android/youtube/api/jar/z;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->m:Lcom/google/android/youtube/core/player/overlay/e;

    return-void
.end method

.method public setLoading()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->LOADING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    return-void
.end method

.method public setMinimal(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "MINIMAL mode is not supported for Google TV controls"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setOnPlayInYouTubeListener(Lcom/google/android/youtube/api/jar/c;)V
    .locals 0

    return-void
.end method

.method public setPlaying()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->o:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->g()V

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setScrubbingEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->u:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a(Z)V

    return-void
.end method

.method public setShowFullscreen(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->i:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->n:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iput p1, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->s:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->d:Lcom/google/android/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(III)V

    return-void
.end method

.method public setVideoTitle(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
