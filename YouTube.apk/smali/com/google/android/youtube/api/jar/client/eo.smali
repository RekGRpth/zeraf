.class public final Lcom/google/android/youtube/api/jar/client/eo;
.super Lcom/google/android/youtube/api/jar/client/co;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/ab;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/ab;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/co;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/ab;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->a:Lcom/google/android/youtube/core/player/overlay/ab;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/eo;)Lcom/google/android/youtube/core/player/overlay/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->a:Lcom/google/android/youtube/core/player/overlay/ab;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ep;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ep;-><init>(Lcom/google/android/youtube/api/jar/client/eo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/es;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/es;-><init>(Lcom/google/android/youtube/api/jar/client/eo;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/eq;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/eq;-><init>(Lcom/google/android/youtube/api/jar/client/eo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/eo;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/er;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/er;-><init>(Lcom/google/android/youtube/api/jar/client/eo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
