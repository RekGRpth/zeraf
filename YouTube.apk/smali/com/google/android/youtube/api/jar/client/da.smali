.class public final Lcom/google/android/youtube/api/jar/client/da;
.super Lcom/google/android/youtube/api/jar/client/cc;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/ai;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/youtube/api/jar/client/dd;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/ai;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/cc;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ai;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->a:Lcom/google/android/youtube/core/player/ai;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/da;)Lcom/google/android/youtube/api/jar/client/dd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->c:Lcom/google/android/youtube/api/jar/client/dd;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/da;Lcom/google/android/youtube/api/jar/client/dd;)Lcom/google/android/youtube/api/jar/client/dd;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/da;->c:Lcom/google/android/youtube/api/jar/client/dd;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/da;)Lcom/google/android/youtube/core/player/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->a:Lcom/google/android/youtube/core/player/ai;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->c:Lcom/google/android/youtube/api/jar/client/dd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->c:Lcom/google/android/youtube/api/jar/client/dd;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/dd;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->c:Lcom/google/android/youtube/api/jar/client/dd;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/aw;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/db;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/db;-><init>(Lcom/google/android/youtube/api/jar/client/da;Lcom/google/android/youtube/api/service/a/aw;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/da;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/dc;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/dc;-><init>(Lcom/google/android/youtube/api/jar/client/da;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
