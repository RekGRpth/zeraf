.class public final Lcom/google/android/youtube/api/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/a;


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "handler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/ac;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ac;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/ad;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/ad;-><init>(Lcom/google/android/youtube/api/ac;Lcom/google/android/youtube/core/player/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
