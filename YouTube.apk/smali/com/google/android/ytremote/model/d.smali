.class public final Lcom/google/android/ytremote/model/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/ytremote/model/a;

.field private final b:Landroid/net/Uri;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/ytremote/model/SsdpId;


# direct methods
.method public constructor <init>(Lcom/google/android/ytremote/model/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->a(Lcom/google/android/ytremote/model/e;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->b(Lcom/google/android/ytremote/model/e;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->c(Lcom/google/android/ytremote/model/e;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->d(Lcom/google/android/ytremote/model/e;)Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->e(Lcom/google/android/ytremote/model/e;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->f(Lcom/google/android/ytremote/model/e;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/ytremote/model/d;->d:Z

    invoke-static {p1}, Lcom/google/android/ytremote/model/e;->g(Lcom/google/android/ytremote/model/e;)Lcom/google/android/ytremote/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/ytremote/model/d;)Lcom/google/android/ytremote/model/SsdpId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/ytremote/model/d;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/ytremote/model/d;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/model/d;->d:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/ytremote/model/d;)Lcom/google/android/ytremote/model/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/ytremote/model/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/ytremote/model/d;
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/android/ytremote/model/e;

    invoke-direct {v0, p0}, Lcom/google/android/ytremote/model/e;-><init>(Lcom/google/android/ytremote/model/d;)V

    invoke-virtual {v0, p1}, Lcom/google/android/ytremote/model/e;->a(Ljava/lang/String;)Lcom/google/android/ytremote/model/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/e;->a()Lcom/google/android/ytremote/model/d;

    move-result-object p0

    goto :goto_0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/google/android/ytremote/model/SsdpId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/ytremote/model/d;

    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/google/android/ytremote/model/d;->d:Z

    iget-boolean v3, p1, Lcom/google/android/ytremote/model/d;->d:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    if-nez v2, :cond_d

    iget-object v2, p1, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    if-eqz v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    iget-object v3, p1, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    if-nez v2, :cond_f

    iget-object v2, p1, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    iget-object v3, p1, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/a;->a(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->a:Lcom/google/android/ytremote/model/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/model/d;->d:Z

    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/ytremote/model/d;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/ytremote/model/d;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/SsdpId;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "YouTubeDevice [deviceName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/ytremote/model/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ssdpId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/model/d;->g:Lcom/google/android/ytremote/model/SsdpId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
