.class final Lcom/google/common/collect/cd;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/common/collect/bk;Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)J
    .locals 4
    .param p3    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    const-wide/16 v0, 0x0

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v2, p2}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Ljava/lang/Object;Lcom/google/common/collect/BstSide;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0, p3}, Lcom/google/common/collect/bk;->b(Lcom/google/common/collect/bw;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-virtual {p3, p2}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v2

    invoke-interface {p0, v2}, Lcom/google/common/collect/bk;->a(Lcom/google/common/collect/bw;)J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p2}, Lcom/google/common/collect/BstSide;->other()Lcom/google/common/collect/BstSide;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object p3

    goto :goto_0

    :cond_0
    invoke-virtual {p3, p2}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object p3

    goto :goto_0

    :cond_1
    return-wide v0
.end method

.method public static a(Lcom/google/common/collect/bk;Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bw;)J
    .locals 4
    .param p2    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/common/collect/GeneralRange;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-wide/16 v0, 0x0

    :cond_1
    :goto_0
    return-wide v0

    :cond_2
    invoke-interface {p0, p2}, Lcom/google/common/collect/bk;->a(Lcom/google/common/collect/bw;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/common/collect/GeneralRange;->hasLowerBound()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-static {p0, p1, v2, p2}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/bk;Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    :cond_3
    invoke-virtual {p1}, Lcom/google/common/collect/GeneralRange;->hasUpperBound()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-static {p0, p1, v2, p2}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/bk;Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private static a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bl;Lcom/google/common/collect/by;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;
    .locals 4
    .param p4    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    :goto_0
    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p4}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0, p3}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Ljava/lang/Object;Lcom/google/common/collect/BstSide;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p4, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v1

    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p4, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v0

    sget-object v2, Lcom/google/common/collect/ce;->a:[I

    invoke-virtual {p3}, Lcom/google/common/collect/BstSide;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    sget-object v2, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-static {p0, p1, p2, v2, v0}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bl;Lcom/google/common/collect/by;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    :goto_2
    invoke-interface {p1, p2, p4, v1, v0}, Lcom/google/common/collect/bl;->a(Lcom/google/common/collect/by;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    sget-object v2, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-static {p0, p1, p2, v2, v1}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bl;Lcom/google/common/collect/by;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v1

    goto :goto_2

    :cond_1
    invoke-virtual {p4, p3}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object p4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bl;Lcom/google/common/collect/by;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;
    .locals 3
    .param p3    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/common/collect/GeneralRange;->hasUpperBound()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bl;Lcom/google/common/collect/by;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/common/collect/GeneralRange;->hasLowerBound()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-static {p0, p1, p2, v1, p3}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/bl;Lcom/google/common/collect/by;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v1

    :cond_0
    invoke-interface {p1, p2, v1, v0}, Lcom/google/common/collect/bl;->a(Lcom/google/common/collect/by;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/cc;Lcom/google/common/collect/bw;)Lcom/google/common/collect/cb;
    .locals 1
    .param p3    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2, p3}, Lcom/google/common/collect/cc;->a(Lcom/google/common/collect/bw;)Lcom/google/common/collect/cb;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/cc;Lcom/google/common/collect/cb;)Lcom/google/common/collect/cb;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/cc;Lcom/google/common/collect/cb;)Lcom/google/common/collect/cb;
    .locals 4

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p3}, Lcom/google/common/collect/cb;->b()Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p0, v2, p1}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Ljava/lang/Object;Lcom/google/common/collect/BstSide;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/common/collect/BstSide;->other()Lcom/google/common/collect/BstSide;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/common/collect/bw;->hasChild(Lcom/google/common/collect/BstSide;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/common/collect/BstSide;->other()Lcom/google/common/collect/BstSide;

    move-result-object v1

    invoke-interface {p2, p3, v1}, Lcom/google/common/collect/cc;->a(Lcom/google/common/collect/cb;Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/cb;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v0

    :cond_1
    :goto_1
    return-object p3

    :cond_2
    invoke-virtual {v1, p1}, Lcom/google/common/collect/bw;->hasChild(Lcom/google/common/collect/BstSide;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2, p3, p1}, Lcom/google/common/collect/cc;->a(Lcom/google/common/collect/cb;Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/cb;

    move-result-object v1

    invoke-static {p0, p1, p2, v1}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/cc;Lcom/google/common/collect/cb;)Lcom/google/common/collect/cb;

    move-result-object v1

    if-eqz v1, :cond_3

    move-object p3, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/common/collect/BstSide;->other()Lcom/google/common/collect/BstSide;

    move-result-object v1

    invoke-static {p0, v2, v1}, Lcom/google/common/collect/cd;->a(Lcom/google/common/collect/GeneralRange;Ljava/lang/Object;Lcom/google/common/collect/BstSide;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p3, v0

    goto :goto_1
.end method

.method private static a(Lcom/google/common/collect/GeneralRange;Ljava/lang/Object;Lcom/google/common/collect/BstSide;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/common/collect/ce;->a:[I

    invoke-virtual {p2}, Lcom/google/common/collect/BstSide;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/common/collect/GeneralRange;->tooLow(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/common/collect/GeneralRange;->tooHigh(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
