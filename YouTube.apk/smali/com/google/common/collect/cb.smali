.class abstract Lcom/google/common/collect/cb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/common/collect/bw;

.field private final b:Lcom/google/common/collect/cb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/common/collect/bw;Lcom/google/common/collect/cb;)V
    .locals 1
    .param p2    # Lcom/google/common/collect/cb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bw;

    iput-object v0, p0, Lcom/google/common/collect/cb;->a:Lcom/google/common/collect/bw;

    iput-object p2, p0, Lcom/google/common/collect/cb;->b:Lcom/google/common/collect/cb;

    return-void
.end method


# virtual methods
.method public final b()Lcom/google/common/collect/bw;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cb;->a:Lcom/google/common/collect/bw;

    return-object v0
.end method

.method public final c()Lcom/google/common/collect/cb;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/google/common/collect/cb;->b:Lcom/google/common/collect/cb;

    return-object v0
.end method

.method public final d()Lcom/google/common/collect/cb;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/cb;->b:Lcom/google/common/collect/cb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ag;->b(Z)V

    iget-object v0, p0, Lcom/google/common/collect/cb;->b:Lcom/google/common/collect/cb;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
