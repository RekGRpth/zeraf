.class public final Lcom/google/net/async/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/net/async/e;
.implements Lcom/google/net/async/o;


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/util/logging/Logger;


# instance fields
.field private final c:Lcom/google/net/async/u;

.field private d:Ljava/net/InetSocketAddress;

.field private e:Lcom/google/net/async/aj;

.field private f:Lcom/google/net/async/Connection;

.field private g:Lcom/google/net/async/n;

.field private final h:Lcom/google/net/async/al;

.field private final i:Lcom/google/net/async/am;

.field private j:Lcom/google/net/async/f;

.field private k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/net/async/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/net/async/l;->a:Z

    const-class v0, Lcom/google/net/async/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/net/async/l;->b:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/net/async/u;Lcom/google/net/async/f;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    iput-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    iput-object v0, p0, Lcom/google/net/async/l;->g:Lcom/google/net/async/n;

    new-instance v0, Lcom/google/net/async/al;

    invoke-direct {v0}, Lcom/google/net/async/al;-><init>()V

    iput-object v0, p0, Lcom/google/net/async/l;->h:Lcom/google/net/async/al;

    new-instance v0, Lcom/google/net/async/am;

    invoke-direct {v0}, Lcom/google/net/async/am;-><init>()V

    iput-object v0, p0, Lcom/google/net/async/l;->i:Lcom/google/net/async/am;

    iput v2, p0, Lcom/google/net/async/l;->k:I

    iput v2, p0, Lcom/google/net/async/l;->l:I

    iput-boolean v1, p0, Lcom/google/net/async/l;->m:Z

    iput-boolean v1, p0, Lcom/google/net/async/l;->n:Z

    iput-boolean v1, p0, Lcom/google/net/async/l;->p:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "eventRegistry cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    iput-object p2, p0, Lcom/google/net/async/l;->j:Lcom/google/net/async/f;

    return-void
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    :try_start_0
    invoke-direct {p0}, Lcom/google/net/async/l;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/net/async/l;->p:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/google/net/async/l;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/net/async/l;->j:Lcom/google/net/async/f;

    invoke-interface {v1, v0}, Lcom/google/net/async/f;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-virtual {v0}, Lcom/google/net/async/aj;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-virtual {v0}, Lcom/google/net/async/aj;->d()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/net/async/IORuntimeException;

    const-string v1, "Seems like a bug: handleConnectEvent() invoked when connection is still in progress"

    invoke-direct {v0, v1}, Lcom/google/net/async/IORuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    iget-object v1, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-interface {v0, v1}, Lcom/google/net/async/u;->a(Ljava/nio/channels/SelectableChannel;)V

    new-instance v0, Lcom/google/net/async/Connection;

    iget-object v1, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    iget-object v2, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    iget-object v3, p0, Lcom/google/net/async/l;->j:Lcom/google/net/async/f;

    sget-object v4, Lcom/google/net/async/Connection$ConnectionMode;->CLIENT:Lcom/google/net/async/Connection$ConnectionMode;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/net/async/Connection;-><init>(Lcom/google/net/async/aj;Lcom/google/net/async/u;Lcom/google/net/async/f;Lcom/google/net/async/Connection$ConnectionMode;)V

    iput-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    iget-object v0, p0, Lcom/google/net/async/l;->o:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    iget-object v0, p0, Lcom/google/net/async/l;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/net/async/Connection;->a(Ljava/lang/String;)V

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    iget-object v0, p0, Lcom/google/net/async/l;->h:Lcom/google/net/async/al;

    iget-object v1, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v1}, Lcom/google/net/async/Connection;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/net/async/al;->a(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/google/net/async/l;->i:Lcom/google/net/async/am;

    iget-object v1, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v1}, Lcom/google/net/async/Connection;->b()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/net/async/am;->a(Ljava/io/OutputStream;)V

    iget v0, p0, Lcom/google/net/async/l;->k:I

    if-lez v0, :cond_6

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    iget v1, p0, Lcom/google/net/async/l;->k:I

    invoke-virtual {v0, v1}, Lcom/google/net/async/Connection;->a(I)V

    :cond_6
    iget v0, p0, Lcom/google/net/async/l;->l:I

    if-lez v0, :cond_7

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    iget v1, p0, Lcom/google/net/async/l;->l:I

    invoke-virtual {v0, v1}, Lcom/google/net/async/Connection;->b(I)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/net/async/l;->m:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v0}, Lcom/google/net/async/Connection;->c()V

    :cond_8
    iget-boolean v0, p0, Lcom/google/net/async/l;->n:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v0}, Lcom/google/net/async/Connection;->d()V

    :cond_9
    iget-object v0, p0, Lcom/google/net/async/l;->g:Lcom/google/net/async/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/l;->g:Lcom/google/net/async/n;

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public final a(Ljava/net/InetSocketAddress;Lcom/google/net/async/n;)V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/google/net/async/aj;->a()Lcom/google/net/async/aj;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/net/async/l;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "alreading connecting"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/net/async/l;->j:Lcom/google/net/async/f;

    iget-object v2, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    invoke-static {v1, v0, v2}, Lcom/google/net/async/g;->a(Lcom/google/net/async/f;Ljava/lang/Exception;Lcom/google/net/async/u;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/net/async/l;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "alreading connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v1, p0, Lcom/google/net/async/l;->p:Z

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput-object p2, p0, Lcom/google/net/async/l;->g:Lcom/google/net/async/n;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iput-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    iget-object v1, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/net/async/aj;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    iget-object v1, p0, Lcom/google/net/async/l;->d:Ljava/net/InetSocketAddress;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-virtual {v1}, Lcom/google/net/async/aj;->b()Ljava/net/Socket;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v2, p0, Lcom/google/net/async/l;->d:Ljava/net/InetSocketAddress;

    invoke-virtual {v1, v2}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    :cond_4
    iget-object v1, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-virtual {v1, p1}, Lcom/google/net/async/aj;->a(Ljava/net/SocketAddress;)Z

    iget-object v1, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    iget-object v2, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-interface {v1, v2, p0}, Lcom/google/net/async/u;->a(Ljava/nio/channels/SelectableChannel;Lcom/google/net/async/o;)V

    invoke-virtual {v0}, Lcom/google/net/async/aj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    new-instance v1, Lcom/google/net/async/m;

    invoke-direct {v1, p0}, Lcom/google/net/async/m;-><init>(Lcom/google/net/async/l;)V

    invoke-interface {v0, v1}, Lcom/google/net/async/u;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/google/net/async/l;->j:Lcom/google/net/async/f;

    iget-object v2, p0, Lcom/google/net/async/l;->c:Lcom/google/net/async/u;

    invoke-static {v1, v0, v2}, Lcom/google/net/async/g;->a(Lcom/google/net/async/f;Ljava/lang/Exception;Lcom/google/net/async/u;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method

.method public final b()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/l;->h:Lcom/google/net/async/al;

    return-object v0
.end method

.method public final c()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/l;->i:Lcom/google/net/async/am;

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v0}, Lcom/google/net/async/Connection;->c()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/net/async/l;->m:Z

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v0}, Lcom/google/net/async/Connection;->d()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/net/async/l;->n:Z

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/net/async/l;->p:Z

    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-virtual {v0}, Lcom/google/net/async/aj;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    :cond_0
    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v0}, Lcom/google/net/async/Connection;->e()V

    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-direct {p0}, Lcom/google/net/async/l;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Status = connecting; Connecting SocketChannel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/net/async/l;->e:Lcom/google/net/async/aj;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    const-string v1, ";maxSizePerReadToSet_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/google/net/async/l;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ";maxSizePerFlushToSet_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/google/net/async/l;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ";asyncReadAfterConnect_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v1, p0, Lcom/google/net/async/l;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, ";asyncFlushAfterConnect_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v1, p0, Lcom/google/net/async/l;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, ";isClosed_ = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v1, p0, Lcom/google/net/async/l;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/net/async/l;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Status = connected; Connection = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/net/async/l;->f:Lcom/google/net/async/Connection;

    invoke-virtual {v1}, Lcom/google/net/async/Connection;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    const-string v1, "Status = idle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
