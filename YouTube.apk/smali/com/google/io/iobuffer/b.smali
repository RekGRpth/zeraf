.class public Lcom/google/io/iobuffer/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Ljava/util/List;

.field private c:Ljava/nio/ByteBuffer;

.field private d:Ljava/nio/CharBuffer;

.field private e:Ljava/nio/ByteBuffer;

.field private f:Ljava/nio/CharBuffer;

.field private g:Ljava/lang/String;

.field private h:Ljava/nio/charset/CharsetEncoder;

.field private i:Ljava/nio/charset/CharsetDecoder;

.field private j:I

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Lcom/google/io/iobuffer/a;

.field private p:Ljava/nio/ByteBuffer;

.field private q:Ljava/nio/CharBuffer;

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/io/iobuffer/b;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->h:Ljava/nio/charset/CharsetEncoder;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->i:Ljava/nio/charset/CharsetDecoder;

    const/16 v0, 0x1000

    iput v0, p0, Lcom/google/io/iobuffer/b;->j:I

    iput-boolean v2, p0, Lcom/google/io/iobuffer/b;->k:Z

    iput v2, p0, Lcom/google/io/iobuffer/b;->l:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/io/iobuffer/b;->m:I

    iput v2, p0, Lcom/google/io/iobuffer/b;->n:I

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->o:Lcom/google/io/iobuffer/a;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->p:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    iput v2, p0, Lcom/google/io/iobuffer/b;->r:I

    return-void
.end method

.method private a(Ljava/nio/CharBuffer;Z)V
    .locals 3

    :cond_0
    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->e()Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    invoke-virtual {p1}, Ljava/nio/CharBuffer;->get()C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/CharBuffer;->put(C)Ljava/nio/CharBuffer;

    :cond_1
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    :goto_0
    iget-object v2, p0, Lcom/google/io/iobuffer/b;->h:Ljava/nio/charset/CharsetEncoder;

    invoke-virtual {v2, v0, v1, p2}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/CoderResult;->OVERFLOW:Ljava/nio/charset/CoderResult;

    if-ne v1, v2, :cond_4

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->j()V

    :cond_2
    invoke-virtual {p1}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_3
    move-object v0, p1

    goto :goto_0

    :cond_4
    sget-object v2, Ljava/nio/charset/CoderResult;->UNDERFLOW:Ljava/nio/charset/CoderResult;

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->remaining()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    :goto_1
    invoke-virtual {v0}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/io/iobuffer/b;->q:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->get()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/nio/CharBuffer;->put(C)Ljava/nio/CharBuffer;

    goto :goto_1
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->h:Ljava/nio/charset/CharsetEncoder;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No encoding has been set. Must call setCharacterEncoding() before flushing buffers."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-direct {p0, v0, p1}, Lcom/google/io/iobuffer/b;->a(Ljava/nio/CharBuffer;Z)V

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "encode() should consume the entire write_char_buf_"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->h:Ljava/nio/charset/CharsetEncoder;

    invoke-virtual {v0}, Ljava/nio/charset/CharsetEncoder;->reset()Ljava/nio/charset/CharsetEncoder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    :goto_0
    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->k()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget v0, p0, Lcom/google/io/iobuffer/b;->r:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, Lcom/google/io/iobuffer/b;->r:I

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/io/iobuffer/b;->r:I

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method private i()Ljava/nio/CharBuffer;
    .locals 3

    const/16 v0, 0x400

    iget-object v1, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/io/iobuffer/b;->m:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    :goto_1
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    :goto_2
    return-object v0

    :cond_0
    iget v1, p0, Lcom/google/io/iobuffer/b;->m:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/io/iobuffer/b;->b(Z)V

    goto :goto_1
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, Lcom/google/io/iobuffer/b;->l:I

    iget-object v1, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->k()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private k()V
    .locals 2

    iget v0, p0, Lcom/google/io/iobuffer/b;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->d()I

    move-result v0

    iget v1, p0, Lcom/google/io/iobuffer/b;->m:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/io/iobuffer/b;->a(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)I
    .locals 6

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot read from the byte buffer: character data has already been read."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    :goto_0
    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->f()Ljava/nio/ByteBuffer;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-gt v3, v0, :cond_1

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    add-int v0, v1, v3

    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->g()V

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    add-int/2addr v0, v1

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_1

    :cond_2
    return v1
.end method

.method public final a([B)I
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/io/iobuffer/b;->a([BII)I

    move-result v0

    return v0
.end method

.method public final a([BII)I
    .locals 3

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot read from the byte buffer: character data has already been read."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, p3

    :goto_0
    if-lez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->f()Ljava/nio/ByteBuffer;

    move-result-object v2

    if-nez v2, :cond_1

    if-ne p3, v1, :cond_3

    const/4 v0, -0x1

    :goto_1
    return v0

    :cond_1
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-le v0, v1, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v2, p1, p2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    sub-int/2addr v1, v0

    add-int/2addr p2, v0

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->g()V

    goto :goto_0

    :cond_3
    sub-int v0, p3, v1

    goto :goto_1
.end method

.method public final a(J)J
    .locals 6

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot skip bytes: character data has already been read."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-wide v1, p1

    :goto_0
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->f()Ljava/nio/ByteBuffer;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    int-to-long v4, v0

    cmp-long v4, v4, v1

    if-lez v4, :cond_1

    long-to-int v0, v1

    :cond_1
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    int-to-long v3, v0

    sub-long v0, v1, v3

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->g()V

    move-wide v1, v0

    goto :goto_0

    :cond_2
    sub-long v0, p1, v1

    return-wide v0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/io/iobuffer/b;->a(Z)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->i()Ljava/nio/CharBuffer;

    move-result-object v0

    int-to-char v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/CharBuffer;->put(C)Ljava/nio/CharBuffer;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->o:Lcom/google/io/iobuffer/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->o:Lcom/google/io/iobuffer/a;

    :cond_0
    return-void
.end method

.method public final a([CII)V
    .locals 3

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->i()Ljava/nio/CharBuffer;

    move-result-object v0

    move v2, p3

    :goto_0
    if-lez v2, :cond_2

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->i()Ljava/nio/CharBuffer;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/nio/CharBuffer;->remaining()I

    move-result v1

    if-le v1, v2, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, p1, p2, v1}, Ljava/nio/CharBuffer;->put([CII)Ljava/nio/CharBuffer;

    sub-int/2addr v2, v1

    add-int/2addr p2, v1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/io/iobuffer/b;->b(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lez v0, :cond_1

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->j()V

    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot write to the byte buffer: character data has already been written."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->e()Ljava/nio/ByteBuffer;

    move-result-object v0

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final b([B)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/io/iobuffer/b;->b([BII)V

    return-void
.end method

.method public final b([BII)V
    .locals 3

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot write to the byte buffer: character data has already been written."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :goto_0
    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->e()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v2, p1, p2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    sub-int/2addr v1, v0

    add-int/2addr p2, v0

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    move v1, p3

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot read from byte buffer: character data has already been read."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/io/iobuffer/b;->f()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->g()V

    goto :goto_0
.end method

.method public final d()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v2, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final e()Ljava/nio/ByteBuffer;
    .locals 4

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->j()V

    :cond_1
    iget v0, p0, Lcom/google/io/iobuffer/b;->n:I

    iget v1, p0, Lcom/google/io/iobuffer/b;->l:I

    if-lez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/io/iobuffer/b;->k:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/io/iobuffer/b;->l:I

    iget v2, p0, Lcom/google/io/iobuffer/b;->j:I

    div-int/lit8 v2, v2, 0x2

    if-gt v1, v2, :cond_6

    iget v1, p0, Lcom/google/io/iobuffer/b;->j:I

    shr-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/io/iobuffer/b;->j:I

    :goto_1
    iget v1, p0, Lcom/google/io/iobuffer/b;->j:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/io/iobuffer/b;->j:I

    iget v1, p0, Lcom/google/io/iobuffer/b;->j:I

    const/high16 v2, 0x400000

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/io/iobuffer/b;->j:I

    :cond_2
    iget v1, p0, Lcom/google/io/iobuffer/b;->m:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/google/io/iobuffer/b;->m:I

    iget v2, p0, Lcom/google/io/iobuffer/b;->j:I

    if-ge v1, v2, :cond_3

    iget v1, p0, Lcom/google/io/iobuffer/b;->m:I

    iput v1, p0, Lcom/google/io/iobuffer/b;->j:I

    :cond_3
    iget v1, p0, Lcom/google/io/iobuffer/b;->n:I

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/io/iobuffer/b;->j:I

    if-le v0, v1, :cond_4

    iput v0, p0, Lcom/google/io/iobuffer/b;->j:I

    :cond_4
    iget v0, p0, Lcom/google/io/iobuffer/b;->j:I

    add-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Lcom/google/io/iobuffer/b;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/io/iobuffer/b;->a:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "allocating "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/io/iobuffer/b;->j:I

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " byte input buffer last byte buf size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/io/iobuffer/b;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_5
    iput-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    :cond_6
    iget v1, p0, Lcom/google/io/iobuffer/b;->l:I

    iget v2, p0, Lcom/google/io/iobuffer/b;->j:I

    add-int/lit8 v2, v2, 0x2

    if-ne v1, v2, :cond_7

    iget v1, p0, Lcom/google/io/iobuffer/b;->j:I

    shl-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/io/iobuffer/b;->j:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/io/iobuffer/b;->k:Z

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/io/iobuffer/b;->k:Z

    goto/16 :goto_1
.end method

.method public final f()Ljava/nio/ByteBuffer;
    .locals 3

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->h()V

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    :cond_1
    iget-object v1, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/io/iobuffer/b;->h()V

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    const-string v0, "US-ASCII"

    move-object v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v0

    iget-object v3, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->position()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->length()I

    move-result v3

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/io/iobuffer/b;->d:Ljava/nio/CharBuffer;

    invoke-virtual {v5}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v5

    invoke-direct {v4, v5, v0, v3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    iget-object v3, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    iget-object v4, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int/2addr v3, v4

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/io/iobuffer/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v4, v5, v0, v3, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v3, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v5

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    new-instance v7, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-direct {v7, v0, v5, v6, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3

    throw v0
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to convert IOBuffer to string: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v0

    iget-object v3, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->position()I

    move-result v3

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/io/iobuffer/b;->f:Ljava/nio/CharBuffer;

    invoke-virtual {v5}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v5

    invoke-direct {v4, v5, v0, v3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    iget-object v3, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/io/iobuffer/b;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v4, v5, v0, v3, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method
