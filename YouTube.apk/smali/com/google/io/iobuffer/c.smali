.class public final Lcom/google/io/iobuffer/c;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/io/iobuffer/b;


# direct methods
.method public constructor <init>(Lcom/google/io/iobuffer/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/google/io/iobuffer/c;->a:Lcom/google/io/iobuffer/b;

    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/c;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/io/iobuffer/b;->d()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 0

    return-void
.end method

.method public final mark(I)V
    .locals 0

    return-void
.end method

.method public final markSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/c;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/io/iobuffer/b;->c()I

    move-result v0

    return v0
.end method

.method public final read([B)I
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/c;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0, p1}, Lcom/google/io/iobuffer/b;->a([B)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    iget-object v0, p0, Lcom/google/io/iobuffer/c;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/io/iobuffer/b;->a([BII)I

    move-result v0

    return v0
.end method

.method public final reset()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final skip(J)J
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/c;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/io/iobuffer/b;->a(J)J

    move-result-wide v0

    return-wide v0
.end method
