.class public final Lcom/google/zxing/pdf417/decoder/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/pdf417/decoder/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/zxing/pdf417/decoder/a/b;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    iput-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    return-void
.end method


# virtual methods
.method public final a([II[I)V
    .locals 12

    new-instance v2, Lcom/google/zxing/pdf417/decoder/a/c;

    iget-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-direct {v2, v0, p1}, Lcom/google/zxing/pdf417/decoder/a/c;-><init>(Lcom/google/zxing/pdf417/decoder/a/b;[I)V

    new-array v3, p2, [I

    const/4 v0, 0x0

    move v1, p2

    :goto_0
    if-lez v1, :cond_1

    iget-object v4, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v4, v1}, Lcom/google/zxing/pdf417/decoder/a/b;->a(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/zxing/pdf417/decoder/a/c;->b(I)I

    move-result v4

    sub-int v5, p2, v1

    aput v4, v3, v5

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a/b;->b()Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    array-length v2, p3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget v4, p3, v1

    iget-object v5, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    array-length v6, p1

    add-int/lit8 v6, v6, -0x1

    sub-int v4, v6, v4

    invoke-virtual {v5, v4}, Lcom/google/zxing/pdf417/decoder/a/b;->a(I)I

    move-result v4

    new-instance v5, Lcom/google/zxing/pdf417/decoder/a/c;

    iget-object v6, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v4}, Lcom/google/zxing/pdf417/decoder/a/b;->c(II)I

    move-result v4

    aput v4, v7, v8

    const/4 v4, 0x1

    const/4 v8, 0x1

    aput v8, v7, v4

    invoke-direct {v5, v6, v7}, Lcom/google/zxing/pdf417/decoder/a/c;-><init>(Lcom/google/zxing/pdf417/decoder/a/b;[I)V

    invoke-virtual {v0, v5}, Lcom/google/zxing/pdf417/decoder/a/c;->c(Lcom/google/zxing/pdf417/decoder/a/c;)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/google/zxing/pdf417/decoder/a/c;

    iget-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-direct {v1, v0, v3}, Lcom/google/zxing/pdf417/decoder/a/c;-><init>(Lcom/google/zxing/pdf417/decoder/a/b;[I)V

    iget-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    const/4 v2, 0x1

    invoke-virtual {v0, p2, v2}, Lcom/google/zxing/pdf417/decoder/a/b;->a(II)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v3

    if-ge v2, v3, :cond_e

    :goto_2
    iget-object v2, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/b;->a()Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v3

    iget-object v2, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/b;->b()Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v2

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    :goto_3
    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v0

    div-int/lit8 v5, p2, 0x2

    if-lt v0, v5, :cond_5

    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a/b;->a()Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/google/zxing/pdf417/decoder/a/c;->a(I)I

    move-result v5

    iget-object v6, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v6, v5}, Lcom/google/zxing/pdf417/decoder/a/b;->c(I)I

    move-result v5

    :goto_4
    invoke-virtual {v1}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v6

    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v7

    if-lt v6, v7, :cond_4

    invoke-virtual {v1}, Lcom/google/zxing/pdf417/decoder/a/c;->b()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v1}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v6

    invoke-virtual {v2}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v1}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/zxing/pdf417/decoder/a/c;->a(I)I

    move-result v8

    invoke-virtual {v7, v8, v5}, Lcom/google/zxing/pdf417/decoder/a/b;->d(II)I

    move-result v7

    iget-object v8, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v8, v6, v7}, Lcom/google/zxing/pdf417/decoder/a/b;->a(II)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/zxing/pdf417/decoder/a/c;->a(Lcom/google/zxing/pdf417/decoder/a/c;)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    invoke-virtual {v2, v6, v7}, Lcom/google/zxing/pdf417/decoder/a/c;->a(II)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/zxing/pdf417/decoder/a/c;->b(Lcom/google/zxing/pdf417/decoder/a/c;)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v1

    goto :goto_4

    :cond_4
    invoke-virtual {v0, v3}, Lcom/google/zxing/pdf417/decoder/a/c;->c(Lcom/google/zxing/pdf417/decoder/a/c;)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/zxing/pdf417/decoder/a/c;->b(Lcom/google/zxing/pdf417/decoder/a/c;)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a/c;->c()Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    move-object v4, v3

    move-object v3, v0

    move-object v11, v2

    move-object v2, v1

    move-object v1, v11

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/zxing/pdf417/decoder/a/c;->a(I)I

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_6
    iget-object v1, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v1, v0}, Lcom/google/zxing/pdf417/decoder/a/b;->c(I)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/zxing/pdf417/decoder/a/c;->c(I)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v1

    invoke-virtual {v2, v0}, Lcom/google/zxing/pdf417/decoder/a/c;->c(I)Lcom/google/zxing/pdf417/decoder/a/c;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/zxing/pdf417/decoder/a/c;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    const/4 v0, 0x0

    aget-object v3, v2, v0

    const/4 v0, 0x1

    aget-object v2, v2, v0

    invoke-virtual {v3}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v4

    new-array v5, v4, [I

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_5
    iget-object v6, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v6}, Lcom/google/zxing/pdf417/decoder/a/b;->c()I

    move-result v6

    if-ge v0, v6, :cond_8

    if-ge v1, v4, :cond_8

    invoke-virtual {v3, v0}, Lcom/google/zxing/pdf417/decoder/a/c;->b(I)I

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v6, v0}, Lcom/google/zxing/pdf417/decoder/a/b;->c(I)I

    move-result v6

    aput v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    if-eq v1, v4, :cond_9

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_9
    invoke-virtual {v3}, Lcom/google/zxing/pdf417/decoder/a/c;->a()I

    move-result v1

    new-array v4, v1, [I

    const/4 v0, 0x1

    :goto_6
    if-gt v0, v1, :cond_a

    sub-int v6, v1, v0

    iget-object v7, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v3, v0}, Lcom/google/zxing/pdf417/decoder/a/c;->a(I)I

    move-result v8

    invoke-virtual {v7, v0, v8}, Lcom/google/zxing/pdf417/decoder/a/b;->d(II)I

    move-result v7

    aput v7, v4, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    new-instance v1, Lcom/google/zxing/pdf417/decoder/a/c;

    iget-object v0, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-direct {v1, v0, v4}, Lcom/google/zxing/pdf417/decoder/a/c;-><init>(Lcom/google/zxing/pdf417/decoder/a/b;[I)V

    array-length v3, v5

    new-array v4, v3, [I

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v3, :cond_b

    iget-object v6, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    aget v7, v5, v0

    invoke-virtual {v6, v7}, Lcom/google/zxing/pdf417/decoder/a/b;->c(I)I

    move-result v6

    iget-object v7, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    const/4 v8, 0x0

    invoke-virtual {v2, v6}, Lcom/google/zxing/pdf417/decoder/a/c;->b(I)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/google/zxing/pdf417/decoder/a/b;->c(II)I

    move-result v7

    iget-object v8, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v1, v6}, Lcom/google/zxing/pdf417/decoder/a/c;->b(I)I

    move-result v6

    invoke-virtual {v8, v6}, Lcom/google/zxing/pdf417/decoder/a/b;->c(I)I

    move-result v6

    iget-object v8, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    invoke-virtual {v8, v7, v6}, Lcom/google/zxing/pdf417/decoder/a/b;->d(II)I

    move-result v6

    aput v6, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_b
    const/4 v0, 0x0

    :goto_8
    array-length v1, v5

    if-ge v0, v1, :cond_d

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    aget v3, v5, v0

    invoke-virtual {v2, v3}, Lcom/google/zxing/pdf417/decoder/a/b;->b(I)I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_c

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_c
    iget-object v2, p0, Lcom/google/zxing/pdf417/decoder/a/a;->a:Lcom/google/zxing/pdf417/decoder/a/b;

    aget v3, p1, v1

    aget v6, v4, v0

    invoke-virtual {v2, v3, v6}, Lcom/google/zxing/pdf417/decoder/a/b;->c(II)I

    move-result v2

    aput v2, p1, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    return-void

    :cond_e
    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto/16 :goto_2
.end method
