.class final Lcom/google/protobuf/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/l;


# instance fields
.field final a:Lcom/google/protobuf/x;

.field final b:I

.field final c:Lcom/google/protobuf/WireFormat$FieldType;

.field final d:Z

.field final e:Z


# direct methods
.method constructor <init>(Lcom/google/protobuf/x;ILcom/google/protobuf/WireFormat$FieldType;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/protobuf/r;->a:Lcom/google/protobuf/x;

    iput p2, p0, Lcom/google/protobuf/r;->b:I

    iput-object p3, p0, Lcom/google/protobuf/r;->c:Lcom/google/protobuf/WireFormat$FieldType;

    iput-boolean p4, p0, Lcom/google/protobuf/r;->d:Z

    iput-boolean p5, p0, Lcom/google/protobuf/r;->e:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/r;->b:I

    return v0
.end method

.method public final b()Lcom/google/protobuf/WireFormat$FieldType;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/r;->c:Lcom/google/protobuf/WireFormat$FieldType;

    return-object v0
.end method

.method public final c()Lcom/google/protobuf/WireFormat$JavaType;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/r;->c:Lcom/google/protobuf/WireFormat$FieldType;

    invoke-virtual {v0}, Lcom/google/protobuf/WireFormat$FieldType;->getJavaType()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lcom/google/protobuf/r;

    iget v0, p0, Lcom/google/protobuf/r;->b:I

    iget v1, p1, Lcom/google/protobuf/r;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/r;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/r;->e:Z

    return v0
.end method

.method public final f()Lcom/google/protobuf/x;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/r;->a:Lcom/google/protobuf/x;

    return-object v0
.end method
