.class public abstract Lcom/google/protobuf/GeneratedMessageLite;
.super Lcom/google/protobuf/a;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/a;-><init>()V

    return-void
.end method

.method protected constructor <init>(Lcom/google/protobuf/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/a;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/protobuf/j;Lcom/google/protobuf/ad;Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z
    .locals 1

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/protobuf/GeneratedMessageLite;->parseUnknownField(Lcom/google/protobuf/j;Lcom/google/protobuf/ad;Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z

    move-result v0

    return v0
.end method

.method static varargs getMethodOrDie(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Generated message class \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" missing method \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static internalMutableDefault(Ljava/lang/String;)Lcom/google/protobuf/ag;
    .locals 3

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getDefaultInstance"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageLite;->getMethodOrDie(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/protobuf/GeneratedMessageLite;->invokeOrDie(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ag;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot load the corresponding mutable class. Please add necessary dependencies."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static varargs invokeOrDie(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t use Java reflection to implement protocol message reflection."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Error;

    throw v0

    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected exception thrown by generated accessor method."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static newRepeatedGeneratedExtension(Lcom/google/protobuf/ad;Lcom/google/protobuf/ad;Lcom/google/protobuf/x;ILcom/google/protobuf/WireFormat$FieldType;ZLjava/lang/Class;)Lcom/google/protobuf/s;
    .locals 8

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    new-instance v7, Lcom/google/protobuf/s;

    new-instance v0, Lcom/google/protobuf/r;

    const/4 v4, 0x1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/r;-><init>(Lcom/google/protobuf/x;ILcom/google/protobuf/WireFormat$FieldType;ZZ)V

    move-object v1, v7

    move-object v2, p0

    move-object v3, v6

    move-object v4, p1

    move-object v5, v0

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/protobuf/s;-><init>(Lcom/google/protobuf/ad;Ljava/lang/Object;Lcom/google/protobuf/ad;Lcom/google/protobuf/r;Ljava/lang/Class;)V

    return-object v7
.end method

.method public static newSingularGeneratedExtension(Lcom/google/protobuf/ad;Ljava/lang/Object;Lcom/google/protobuf/ad;Lcom/google/protobuf/x;ILcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Class;)Lcom/google/protobuf/s;
    .locals 8

    const/4 v4, 0x0

    new-instance v7, Lcom/google/protobuf/s;

    new-instance v0, Lcom/google/protobuf/r;

    move-object v1, p3

    move v2, p4

    move-object v3, p5

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/r;-><init>(Lcom/google/protobuf/x;ILcom/google/protobuf/WireFormat$FieldType;ZZ)V

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v0

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/protobuf/s;-><init>(Lcom/google/protobuf/ad;Ljava/lang/Object;Lcom/google/protobuf/ad;Lcom/google/protobuf/r;Ljava/lang/Class;)V

    return-object v7
.end method

.method private static parseUnknownField(Lcom/google/protobuf/j;Lcom/google/protobuf/ad;Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p4}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v2

    invoke-static {p4}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v3

    invoke-virtual {p3, p1, v3}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/ad;I)Lcom/google/protobuf/s;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v4, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v4}, Lcom/google/protobuf/r;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/WireFormat$FieldType;Z)I

    move-result v4

    if-ne v2, v4, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {p2, p4}, Lcom/google/protobuf/g;->a(I)Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget-object v4, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    iget-boolean v4, v4, Lcom/google/protobuf/r;->d:Z

    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    iget-object v4, v4, Lcom/google/protobuf/r;->c:Lcom/google/protobuf/WireFormat$FieldType;

    invoke-virtual {v4}, Lcom/google/protobuf/WireFormat$FieldType;->isPackable()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v4}, Lcom/google/protobuf/r;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/WireFormat$FieldType;Z)I

    move-result v4

    if-ne v2, v4, :cond_1

    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lcom/google/protobuf/g;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/protobuf/g;->b(I)I

    move-result v0

    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v2

    sget-object v4, Lcom/google/protobuf/WireFormat$FieldType;->ENUM:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne v2, v4, :cond_4

    :goto_2
    invoke-virtual {p2}, Lcom/google/protobuf/g;->i()I

    move-result v2

    if-lez v2, :cond_5

    invoke-virtual {p2}, Lcom/google/protobuf/g;->d()I

    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->f()Lcom/google/protobuf/x;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/x;->a()Lcom/google/protobuf/w;

    move-result-object v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v4, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v3, v2}, Lcom/google/protobuf/s;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v4, v2}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/l;Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    :goto_3
    invoke-virtual {p2}, Lcom/google/protobuf/g;->i()I

    move-result v2

    if-lez v2, :cond_5

    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;

    move-result-object v2

    iget-object v4, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {p0, v4, v2}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/l;Ljava/lang/Object;)V

    goto :goto_3

    :cond_5
    invoke-virtual {p2, v0}, Lcom/google/protobuf/g;->c(I)V

    :goto_4
    move v0, v1

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/google/protobuf/m;->a:[I

    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->c()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/WireFormat$JavaType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    iget-object v0, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0}, Lcom/google/protobuf/r;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;

    move-result-object v0

    :cond_7
    :goto_5
    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->d()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v3, v0}, Lcom/google/protobuf/s;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/l;Ljava/lang/Object;)V

    goto :goto_4

    :pswitch_0
    const/4 v2, 0x0

    iget-object v0, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0}, Lcom/google/protobuf/r;->d()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {p0, v0}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ad;

    if-eqz v0, :cond_b

    invoke-interface {v0}, Lcom/google/protobuf/ad;->c()Lcom/google/protobuf/ae;

    move-result-object v0

    :goto_6
    if-nez v0, :cond_8

    iget-object v0, v3, Lcom/google/protobuf/s;->c:Lcom/google/protobuf/ad;

    invoke-interface {v0}, Lcom/google/protobuf/ad;->b()Lcom/google/protobuf/ae;

    move-result-object v0

    :cond_8
    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v2

    sget-object v4, Lcom/google/protobuf/WireFormat$FieldType;->GROUP:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne v2, v4, :cond_9

    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v2}, Lcom/google/protobuf/r;->a()I

    move-result v2

    invoke-virtual {p2, v2, v0, p3}, Lcom/google/protobuf/g;->a(ILcom/google/protobuf/ae;Lcom/google/protobuf/h;)V

    :goto_7
    invoke-interface {v0}, Lcom/google/protobuf/ae;->b()Lcom/google/protobuf/ad;

    move-result-object v0

    goto :goto_5

    :cond_9
    invoke-virtual {p2, v0, p3}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ae;Lcom/google/protobuf/h;)V

    goto :goto_7

    :pswitch_1
    invoke-virtual {p2}, Lcom/google/protobuf/g;->d()I

    iget-object v0, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0}, Lcom/google/protobuf/r;->f()Lcom/google/protobuf/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/x;->a()Lcom/google/protobuf/w;

    move-result-object v0

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_1

    :cond_a
    iget-object v2, v3, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v3, v0}, Lcom/google/protobuf/s;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/l;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_b
    move-object v0, v2

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getParserForType()Lcom/google/protobuf/ah;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected internalMutableDefault()Lcom/google/protobuf/ag;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected makeExtensionsImmutable()V
    .locals 0

    return-void
.end method

.method public mutableCopy()Lcom/google/protobuf/ag;
    .locals 4

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite;->internalMutableDefault()Lcom/google/protobuf/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/ag;->d()Lcom/google/protobuf/ag;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite;->toByteArray()[B

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/g;->a([BII)Lcom/google/protobuf/g;

    invoke-interface {v0}, Lcom/google/protobuf/ag;->e()Lcom/google/protobuf/ag;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected parseUnknownField(Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z
    .locals 1

    invoke-virtual {p1, p3}, Lcom/google/protobuf/g;->a(I)Z

    move-result v0

    return v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/protobuf/GeneratedMessageLite$SerializedForm;

    invoke-direct {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite$SerializedForm;-><init>(Lcom/google/protobuf/ad;)V

    return-object v0
.end method
