.class public Lcom/zte/engineer/ZteEngineerCode;
.super Ljava/lang/Object;
.source "ZteEngineerCode.java"


# static fields
.field public static final ACTION_KEY_TEST:Ljava/lang/String; = "com.zte.engineer.action.KEY_TEST"

.field public static final ACTION_LAUNCHER_TEST:Ljava/lang/String; = "com.zte.engineer.action.LAUNCHER_TEST"

.field public static final ACTION_LAUNCHER_TEST_LIST:Ljava/lang/String; = "com.zte.engineer.action.TEST_LIST"

.field public static final ACTION_SELF_TEST:Ljava/lang/String; = "com.zte.engineer.action.SELF_TEST"

.field public static final BLUETOOTH_TEST:Ljava/lang/String; = "*983*28#"

.field public static final COMMAND:Ljava/lang/String; = "command"

.field public static final EXTERNAL_VERSION_CHECK:Ljava/lang/String; = "*983*32#"

.field public static final FACTORY_RESET:Ljava/lang/String; = "*983*57#"

.field public static final FLAG_CHECK:Ljava/lang/String; = "*983*7#"

.field public static final FM_TEST:Ljava/lang/String; = "*983*36#"

.field public static final FUNCTION_TEST:Ljava/lang/String; = "*983*3640#"

.field public static final G_SENSOR_CALIBRATION:Ljava/lang/String; = "*983*367#"

.field public static final HARDWARE_CHECK:Ljava/lang/String; = "*983*9529#"

.field public static final INTERNAL_VERSION_CHECK:Ljava/lang/String; = "*983*1275#"

.field public static final SELF_TEST:Ljava/lang/String; = "*983*70#"

.field public static final SIGNAL_FUNCTION_TEST:Ljava/lang/String; = "*#66*#"

.field public static final TEST_LIST:Ljava/lang/String; = "*983*0#"

.field public static final VENEER_SERIAL:Ljava/lang/String; = "*983*22#"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
