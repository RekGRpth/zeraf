.class public Lcom/zte/engineer/AudioLoopTest;
.super Lcom/zte/engineer/ZteActivity;
.source "AudioLoopTest.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "ZTEAudioLoopTest"


# instance fields
.field private isHeadsetConnect:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private running:Z

.field private soundeffect:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zte/engineer/AudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    iput-boolean v1, p0, Lcom/zte/engineer/AudioLoopTest;->soundeffect:Z

    iput-boolean v1, p0, Lcom/zte/engineer/AudioLoopTest;->running:Z

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/AudioLoopTest;)Z
    .locals 1
    .param p0    # Lcom/zte/engineer/AudioLoopTest;

    iget-boolean v0, p0, Lcom/zte/engineer/AudioLoopTest;->running:Z

    return v0
.end method

.method static synthetic access$100(Lcom/zte/engineer/AudioLoopTest;)Z
    .locals 1
    .param p0    # Lcom/zte/engineer/AudioLoopTest;

    iget-boolean v0, p0, Lcom/zte/engineer/AudioLoopTest;->soundeffect:Z

    return v0
.end method

.method static synthetic access$200(Lcom/zte/engineer/AudioLoopTest;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/zte/engineer/AudioLoopTest;

    iget-object v0, p0, Lcom/zte/engineer/AudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/AudioLoopTest;->running:Z

    iget-object v0, p0, Lcom/zte/engineer/AudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/AudioLoopTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/AudioLoopTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/AudioLoopTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x600080

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f080055

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f060007

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/zte/engineer/AudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const v1, 0x7f08005e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08005f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/AudioLoopTest;->running:Z

    iget-object v0, p0, Lcom/zte/engineer/AudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zte/engineer/AudioLoopTest;->running:Z

    new-instance v0, Lcom/zte/engineer/AudioLoopTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/AudioLoopTest$1;-><init>(Lcom/zte/engineer/AudioLoopTest;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/AudioLoopTest;->running:Z

    iget-object v0, p0, Lcom/zte/engineer/AudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    return-void
.end method
