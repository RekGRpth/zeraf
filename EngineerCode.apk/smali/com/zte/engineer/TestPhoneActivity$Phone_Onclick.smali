.class Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;
.super Ljava/lang/Object;
.source "TestPhoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/TestPhoneActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Phone_Onclick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/TestPhoneActivity;


# direct methods
.method constructor <init>(Lcom/zte/engineer/TestPhoneActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tel:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v11}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060094

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    new-instance v1, Landroid/content/Intent;

    const-string v10, "android.intent.action.CALL"

    invoke-direct {v1, v10, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v10, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tel:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v11}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060092

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.intent.action.CALL"

    invoke-direct {v4, v10, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v10, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tel:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v11}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060090

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    new-instance v3, Landroid/content/Intent;

    const-string v10, "android.intent.action.CALL"

    invoke-direct {v3, v10, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v10, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-static {v10}, Lcom/zte/engineer/TestPhoneActivity;->access$000(Lcom/zte/engineer/TestPhoneActivity;)Landroid/widget/EditText;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x3

    if-lt v10, v11, :cond_0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tel:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v2, Landroid/content/Intent;

    const-string v10, "android.intent.action.CALL"

    invoke-direct {v2, v10, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v10, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_0
    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    const v11, 0x7f060098

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_4
    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v10}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :sswitch_5
    iget-object v10, p0, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;->this$0:Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v10}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f08000a -> :sswitch_4
        0x7f080070 -> :sswitch_0
        0x7f080072 -> :sswitch_1
        0x7f080074 -> :sswitch_2
        0x7f080076 -> :sswitch_3
        0x7f080077 -> :sswitch_5
    .end sparse-switch
.end method
