.class public Lcom/zte/engineer/Launcher;
.super Ljava/lang/Object;
.source "Launcher.java"


# static fields
.field public static final BLUETOOTH_SETTINGS_CLASS:Ljava/lang/String; = "com.android.settings.bluetooth.BluetoothSettings"

.field public static final BLUETOOTH_SETTINGS_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field public static final CAMERA_PACKAGE:Ljava/lang/String; = "com.android.gallery3d"

.field public static final CAMERA_TARGET_CLASS:Ljava/lang/String; = "com.android.camera.CameraLauncher"

.field public static final FACTORY_RESET_CLASS:Ljava/lang/String; = "com.android.settings.Settings$PrivacySettingsActivity"

.field public static final FACTORY_RESET_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field public static final FM_TEST_PACKAGES:Ljava/lang/String; = "com.mediatek.FMRadio"

.field public static final FM_TEST_TARGET_CLASS:Ljava/lang/String; = "com.mediatek.FMRadio.FMRadioEMActivity"

.field public static final GPS_TEST_PACKAGE:Ljava/lang/String; = "com.mediatek.ygps"

.field public static final GPS_TEST_TRAGET_CLASS:Ljava/lang/String; = "com.mediatek.ygps.YgpsActivity"

.field public static final RADIO_INFO_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field public static final RADIO_INFO_TARGET_CLASS:Ljava/lang/String; = "com.android.settings.RadioInfo"

.field public static final WIFI_SETTINGS_CLASS:Ljava/lang/String; = "com.android.settings.wifi.WifiSettings"

.field public static final WIFI_SETTINGS_PACKAGE:Ljava/lang/String; = "com.android.settings"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
