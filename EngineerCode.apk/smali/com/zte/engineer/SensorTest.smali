.class public Lcom/zte/engineer/SensorTest;
.super Lcom/zte/engineer/ZteActivity;
.source "SensorTest.java"


# instance fields
.field GsensorX:Landroid/widget/TextView;

.field GsensorY:Landroid/widget/TextView;

.field GsensorZ:Landroid/widget/TextView;

.field GyroscopeX:Landroid/widget/TextView;

.field GyroscopeY:Landroid/widget/TextView;

.field GyroscopeZ:Landroid/widget/TextView;

.field LightView:Landroid/widget/TextView;

.field MagneticX:Landroid/widget/TextView;

.field MagneticY:Landroid/widget/TextView;

.field MagneticZ:Landroid/widget/TextView;

.field ProximityView:Landroid/widget/TextView;

.field private lsn:Landroid/hardware/SensorEventListener;

.field private sensorMgr:Landroid/hardware/SensorManager;

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/SensorTest;)F
    .locals 1
    .param p0    # Lcom/zte/engineer/SensorTest;

    iget v0, p0, Lcom/zte/engineer/SensorTest;->x:F

    return v0
.end method

.method static synthetic access$002(Lcom/zte/engineer/SensorTest;F)F
    .locals 0
    .param p0    # Lcom/zte/engineer/SensorTest;
    .param p1    # F

    iput p1, p0, Lcom/zte/engineer/SensorTest;->x:F

    return p1
.end method

.method static synthetic access$100(Lcom/zte/engineer/SensorTest;)F
    .locals 1
    .param p0    # Lcom/zte/engineer/SensorTest;

    iget v0, p0, Lcom/zte/engineer/SensorTest;->y:F

    return v0
.end method

.method static synthetic access$102(Lcom/zte/engineer/SensorTest;F)F
    .locals 0
    .param p0    # Lcom/zte/engineer/SensorTest;
    .param p1    # F

    iput p1, p0, Lcom/zte/engineer/SensorTest;->y:F

    return p1
.end method

.method static synthetic access$200(Lcom/zte/engineer/SensorTest;)F
    .locals 1
    .param p0    # Lcom/zte/engineer/SensorTest;

    iget v0, p0, Lcom/zte/engineer/SensorTest;->z:F

    return v0
.end method

.method static synthetic access$202(Lcom/zte/engineer/SensorTest;F)F
    .locals 0
    .param p0    # Lcom/zte/engineer/SensorTest;
    .param p1    # F

    iput p1, p0, Lcom/zte/engineer/SensorTest;->z:F

    return p1
.end method

.method private initSensorListener()V
    .locals 6

    iget-object v3, p0, Lcom/zte/engineer/SensorTest;->sensorMgr:Landroid/hardware/SensorManager;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/zte/engineer/SensorTest$1;

    invoke-direct {v3, p0}, Lcom/zte/engineer/SensorTest$1;-><init>(Lcom/zte/engineer/SensorTest;)V

    iput-object v3, p0, Lcom/zte/engineer/SensorTest;->lsn:Landroid/hardware/SensorEventListener;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Sensor;

    iget-object v3, p0, Lcom/zte/engineer/SensorTest;->sensorMgr:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/zte/engineer/SensorTest;->lsn:Landroid/hardware/SensorEventListener;

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v1, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private initUi()V
    .locals 1

    const v0, 0x7f080053

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080054

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080048

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->GsensorX:Landroid/widget/TextView;

    const v0, 0x7f080049

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->GsensorY:Landroid/widget/TextView;

    const v0, 0x7f08004a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->GsensorZ:Landroid/widget/TextView;

    const v0, 0x7f08004e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->MagneticX:Landroid/widget/TextView;

    const v0, 0x7f08004f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->MagneticY:Landroid/widget/TextView;

    const v0, 0x7f080050

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->MagneticZ:Landroid/widget/TextView;

    const v0, 0x7f08004b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->GyroscopeX:Landroid/widget/TextView;

    const v0, 0x7f08004c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->GyroscopeY:Landroid/widget/TextView;

    const v0, 0x7f08004d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->GyroscopeZ:Landroid/widget/TextView;

    const v0, 0x7f080051

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->LightView:Landroid/widget/TextView;

    const v0, 0x7f080052

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->ProximityView:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/zte/engineer/SensorTest;->sensorMgr:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest;->lsn:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/SensorTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/SensorTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/SensorTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080053
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030011

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/zte/engineer/SensorTest;->sensorMgr:Landroid/hardware/SensorManager;

    invoke-direct {p0}, Lcom/zte/engineer/SensorTest;->initUi()V

    invoke-direct {p0}, Lcom/zte/engineer/SensorTest;->initSensorListener()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
