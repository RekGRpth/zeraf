.class public Lcom/zte/engineer/TestPhoneActivity;
.super Landroid/app/Activity;
.source "TestPhoneActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;
    }
.end annotation


# static fields
.field public static final RESULT_FALSE:I = 0x14

.field public static final RESULT_PASS:I = 0xa


# instance fields
.field private China_Mobile:Landroid/widget/TextView;

.field private China_Telecom:Landroid/widget/TextView;

.field private China_Unicom:Landroid/widget/TextView;

.field private bt_China_Mobile:Landroid/widget/Button;

.field private bt_China_Telecom:Landroid/widget/Button;

.field private bt_China_Unicom:Landroid/widget/Button;

.field private bt_no:Landroid/widget/Button;

.field private bt_others:Landroid/widget/Button;

.field private bt_pass:Landroid/widget/Button;

.field private others:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/TestPhoneActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/zte/engineer/TestPhoneActivity;

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->others:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const v1, 0x7f08006f

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030014

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f080070

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_China_Mobile:Landroid/widget/Button;

    const v0, 0x7f080072

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_China_Unicom:Landroid/widget/Button;

    const v0, 0x7f080074

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_China_Telecom:Landroid/widget/Button;

    const v0, 0x7f080076

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_others:Landroid/widget/Button;

    const v0, 0x7f08000a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_pass:Landroid/widget/Button;

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_pass:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080077

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_no:Landroid/widget/Button;

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_no:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->China_Mobile:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->China_Unicom:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->China_Telecom:Landroid/widget/TextView;

    const v0, 0x7f080075

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->others:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_China_Mobile:Landroid/widget/Button;

    new-instance v1, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;

    invoke-direct {v1, p0}, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;-><init>(Lcom/zte/engineer/TestPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_China_Unicom:Landroid/widget/Button;

    new-instance v1, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;

    invoke-direct {v1, p0}, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;-><init>(Lcom/zte/engineer/TestPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_China_Telecom:Landroid/widget/Button;

    new-instance v1, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;

    invoke-direct {v1, p0}, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;-><init>(Lcom/zte/engineer/TestPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_others:Landroid/widget/Button;

    new-instance v1, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;

    invoke-direct {v1, p0}, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;-><init>(Lcom/zte/engineer/TestPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_pass:Landroid/widget/Button;

    new-instance v1, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;

    invoke-direct {v1, p0}, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;-><init>(Lcom/zte/engineer/TestPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/zte/engineer/TestPhoneActivity;->bt_no:Landroid/widget/Button;

    new-instance v1, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;

    invoke-direct {v1, p0}, Lcom/zte/engineer/TestPhoneActivity$Phone_Onclick;-><init>(Lcom/zte/engineer/TestPhoneActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
