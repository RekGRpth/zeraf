.class public Lcom/zte/engineer/BTAddressTest;
.super Lcom/zte/engineer/ZteActivity;
.source "BTAddressTest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BTAddressTest"


# instance fields
.field isManualTurnOn:Z

.field mBluetooth:Landroid/bluetooth/BluetoothAdapter;

.field mBluetoothAddress:Landroid/widget/TextView;

.field protected mBluetoothReceiver:Landroid/content/BroadcastReceiver;

.field mBluetoothStatus:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/BTAddressTest;->isManualTurnOn:Z

    new-instance v0, Lcom/zte/engineer/BTAddressTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/BTAddressTest$1;-><init>(Lcom/zte/engineer/BTAddressTest;)V

    iput-object v0, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/zte/engineer/BTAddressTest;->isManualTurnOn:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    :cond_0
    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/BTAddressTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/BTAddressTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/BTAddressTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const v4, 0x7f06004b

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030012

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    const v3, 0x7f080055

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f080057

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothStatus:Landroid/widget/TextView;

    const v3, 0x7f080058

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothAddress:Landroid/widget/TextView;

    const v3, 0x7f060011

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    if-ne v7, v1, :cond_0

    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    const v6, 0x7f060022

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothAddress:Landroid/widget/TextView;

    const v4, 0x7f06004c

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const v3, 0x7f08005e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f08005f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    iput-boolean v7, p0, Lcom/zte/engineer/BTAddressTest;->isManualTurnOn:Z

    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    const v6, 0x7f060023

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/zte/engineer/BTAddressTest;->mBluetoothAddress:Landroid/widget/TextView;

    const v4, 0x7f06004d

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
