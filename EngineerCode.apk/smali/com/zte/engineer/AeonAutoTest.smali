.class public Lcom/zte/engineer/AeonAutoTest;
.super Landroid/app/Activity;
.source "AeonAutoTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/engineer/AeonAutoTest$1;,
        Lcom/zte/engineer/AeonAutoTest$MyLocationListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AutoLoopTest"


# instance fields
.field list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private unusefulcode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/zte/engineer/AeonAutoTest;->unusefulcode:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    return-void
.end method

.method private initTestList()V
    .locals 6

    const/high16 v5, 0x10000000

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/VersionTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/CalibrationTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/AeonTouchScreenTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/SIMTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/RingerTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/VibratorTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/AudioLoopTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/SDcardTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/MemoryTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/KeyTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/BTAddressTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.bluetooth.BluetoothSettings"

    invoke-direct {p0, v2, v3}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/WifiAddressTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.wifi.WifiSettings"

    invoke-direct {p0, v2, v3}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/ImeiTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-class v2, Lcom/zte/engineer/SensorTest;

    invoke-direct {p0, p0, v2}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-string v2, "com.mediatek.FMRadio"

    const-string v3, "com.mediatek.FMRadio.FMRadioEMActivity"

    invoke-direct {p0, v2, v3}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extras.CAMERA_FACING"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extras.CAMERA_FACING"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    const-string v2, "com.mediatek.ygps"

    const-string v3, "com.mediatek.ygps.YgpsActivity"

    invoke-direct {p0, v2, v3}, Lcom/zte/engineer/AeonAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$PrivacySettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "do_factory_reset"

    const-string v2, "FactoryMode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "AutoLoopTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "list count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private newIntent(Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    add-int/lit8 v0, p1, 0x1

    iget v2, p0, Lcom/zte/engineer/AeonAutoTest;->unusefulcode:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/zte/engineer/AeonAutoTest;->unusefulcode:I

    iget v2, p0, Lcom/zte/engineer/AeonAutoTest;->unusefulcode:I

    if-ge v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v1, "gps"

    const/4 v2, 0x1

    invoke-static {v9, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    new-instance v5, Lcom/zte/engineer/AeonAutoTest$MyLocationListener;

    const/4 v1, 0x0

    invoke-direct {v5, p0, v1}, Lcom/zte/engineer/AeonAutoTest$MyLocationListener;-><init>(Lcom/zte/engineer/AeonAutoTest;Lcom/zte/engineer/AeonAutoTest$1;)V

    new-instance v8, Landroid/content/Intent;

    const-string v1, "com.mediatek.ygps.YGPSService"

    invoke-direct {v8, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :try_start_0
    const-string v1, "location"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/zte/engineer/AeonAutoTest;->initTestList()V

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/zte/engineer/AeonAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Intent;

    invoke-virtual {p0, v7, v6}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
