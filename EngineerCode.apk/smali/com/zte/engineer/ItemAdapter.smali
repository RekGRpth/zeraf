.class public Lcom/zte/engineer/ItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "ItemAdapter.java"


# static fields
.field private static final EXTRAS_CAMERA_FACING:Ljava/lang/String; = "android.intent.extras.CAMERA_FACING"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImageIds:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/zte/engineer/ItemAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/zte/engineer/ItemAdapter;->mImageIds:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/zte/engineer/ItemAdapter;

    iget-object v0, p0, Lcom/zte/engineer/ItemAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/zte/engineer/ItemAdapter;->mImageIds:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    new-instance v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/16 v3, 0xe6

    const/16 v4, 0x5a

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    move v0, p1

    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter;->mImageIds:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/zte/engineer/ItemAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/zte/engineer/ItemAdapter$1;-><init>(Lcom/zte/engineer/ItemAdapter;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1

    :cond_0
    move-object v1, p2

    check-cast v1, Landroid/widget/Button;

    goto :goto_0
.end method
