.class public Lcom/zte/engineer/VibratorTest;
.super Lcom/zte/engineer/ZteActivity;
.source "VibratorTest.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "VibrateTest"

.field private static final NOTIFY_LED:I = 0x1010

.field private static final TIMER_EVENT_TICK:I = 0x1


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mLed:Landroid/app/NotificationManager;

.field private mNotification:Landroid/app/Notification;

.field mVibFreq:[J

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/zte/engineer/VibratorTest;->mVibFreq:[J

    new-instance v0, Lcom/zte/engineer/VibratorTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/VibratorTest$1;-><init>(Lcom/zte/engineer/VibratorTest;)V

    iput-object v0, p0, Lcom/zte/engineer/VibratorTest;->mHandler:Landroid/os/Handler;

    return-void

    nop

    :array_0
    .array-data 8
        0xa
        0x1388
    .end array-data
.end method

.method static synthetic access$000(Lcom/zte/engineer/VibratorTest;)V
    .locals 0
    .param p0    # Lcom/zte/engineer/VibratorTest;

    invoke-direct {p0}, Lcom/zte/engineer/VibratorTest;->changeVibratorLedStatus()V

    return-void
.end method

.method private changeVibratorLedStatus()V
    .locals 4

    const/16 v3, 0x1010

    const/high16 v2, -0x10000

    const v1, -0xff0100

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mLed:Landroid/app/NotificationManager;

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledARGB:I

    if-ne v2, v0, :cond_1

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    iput v1, v0, Landroid/app/Notification;->ledARGB:I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mLed:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mVibrator:Landroid/os/Vibrator;

    iget-object v1, p0, Lcom/zte/engineer/VibratorTest;->mVibFreq:[J

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledARGB:I

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    iput v2, v0, Landroid/app/Notification;->ledARGB:I

    goto :goto_0
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mLed:Landroid/app/NotificationManager;

    const/16 v1, 0x1010

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/VibratorTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/VibratorTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/VibratorTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f08005e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08005f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f080055

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06000b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/zte/engineer/VibratorTest;->mVibrator:Landroid/os/Vibrator;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/zte/engineer/VibratorTest;->mLed:Landroid/app/NotificationManager;

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    iput-object v1, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    iget-object v1, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/app/Notification;->flags:I

    iget-object v1, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    const/high16 v2, -0x10000

    iput v2, v1, Landroid/app/Notification;->ledARGB:I

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mLed:Landroid/app/NotificationManager;

    const/16 v1, 0x1010

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mLed:Landroid/app/NotificationManager;

    const/16 v1, 0x1010

    iget-object v2, p0, Lcom/zte/engineer/VibratorTest;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mVibrator:Landroid/os/Vibrator;

    iget-object v1, p0, Lcom/zte/engineer/VibratorTest;->mVibFreq:[J

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    iget-object v0, p0, Lcom/zte/engineer/VibratorTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
