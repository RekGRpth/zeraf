.class public Lcom/zte/engineer/BacklightTest;
.super Lcom/zte/engineer/ZteActivity;
.source "BacklightTest.java"


# static fields
.field private static final BACKLIGHT_TIMER_EVENT_TICK:I = 0x1

.field private static final LOGTAG:Ljava/lang/String; = "BacklightTest"

.field private static final MAXIMUM_BACKLIGHT:I = 0xff

.field private static final MINIMUM_BACKLIGHT:I = 0x1e

.field private static mCurrentBrightness:I


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    new-instance v0, Lcom/zte/engineer/BacklightTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/BacklightTest$1;-><init>(Lcom/zte/engineer/BacklightTest;)V

    iput-object v0, p0, Lcom/zte/engineer/BacklightTest;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/BacklightTest;)V
    .locals 0
    .param p0    # Lcom/zte/engineer/BacklightTest;

    invoke-direct {p0}, Lcom/zte/engineer/BacklightTest;->changeBrightness()V

    return-void
.end method

.method private changeBrightness()V
    .locals 3

    const/16 v2, 0xff

    const/16 v1, 0x1e

    sget v0, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    if-ne v1, v0, :cond_1

    sput v2, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    :cond_0
    :goto_0
    sget v0, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    invoke-direct {p0, v0}, Lcom/zte/engineer/BacklightTest;->setBrightness(I)V

    return-void

    :cond_1
    sget v0, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    if-ne v2, v0, :cond_0

    sput v1, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    goto :goto_0
.end method

.method private set2SystemBrightness()V
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_brightness"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    invoke-direct {p0, v1}, Lcom/zte/engineer/BacklightTest;->setBrightness(I)V

    return-void

    :catch_0
    move-exception v0

    const/16 v1, 0xff

    goto :goto_0
.end method

.method private setBrightness(I)V
    .locals 2
    .param p1    # I

    sget-boolean v1, Lcom/zte/engineer/Util;->DEBUG:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    goto :goto_0
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/zte/engineer/BacklightTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/zte/engineer/BacklightTest;->set2SystemBrightness()V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/BacklightTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/BacklightTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/BacklightTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f080055

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06000a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f08005e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08005f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/zte/engineer/BacklightTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/zte/engineer/BacklightTest;->set2SystemBrightness()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/16 v0, 0xff

    sput v0, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    sget v0, Lcom/zte/engineer/BacklightTest;->mCurrentBrightness:I

    invoke-direct {p0, v0}, Lcom/zte/engineer/BacklightTest;->setBrightness(I)V

    iget-object v0, p0, Lcom/zte/engineer/BacklightTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
