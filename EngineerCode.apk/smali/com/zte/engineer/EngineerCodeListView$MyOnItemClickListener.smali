.class Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;
.super Ljava/lang/Object;
.source "EngineerCodeListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/EngineerCodeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOnItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/EngineerCodeListView;


# direct methods
.method private constructor <init>(Lcom/zte/engineer/EngineerCodeListView;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/zte/engineer/EngineerCodeListView;Lcom/zte/engineer/EngineerCodeListView$1;)V
    .locals 0
    .param p1    # Lcom/zte/engineer/EngineerCodeListView;
    .param p2    # Lcom/zte/engineer/EngineerCodeListView$1;

    invoke-direct {p0, p1}, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;-><init>(Lcom/zte/engineer/EngineerCodeListView;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    invoke-static {v1}, Lcom/zte/engineer/EngineerCodeListView;->access$100(Lcom/zte/engineer/EngineerCodeListView;)[I

    move-result-object v1

    aget v1, v1, p3

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    const-class v2, Lcom/zte/engineer/NotSupportNotification;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "notification"

    iget-object v2, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    iget-object v3, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    invoke-static {v3}, Lcom/zte/engineer/EngineerCodeListView;->access$100(Lcom/zte/engineer/EngineerCodeListView;)[I

    move-result-object v3

    aget v3, v3, p3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    iget-object v1, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/EngineerCodeListView;

    const-class v2, Lcom/zte/engineer/ProduceInfoListView;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_1
    const-string v1, "com.zte.engineer.action.TEST_LIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_2
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$PrivacySettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "do_factory_reset"

    const-string v2, "FactoryMode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_3
    const-string v1, "com.zte.engineer.action.BATTERY_LOG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f06006c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
