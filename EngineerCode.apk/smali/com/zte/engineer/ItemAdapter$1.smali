.class Lcom/zte/engineer/ItemAdapter$1;
.super Ljava/lang/Object;
.source "ItemAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zte/engineer/ItemAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/ItemAdapter;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/zte/engineer/ItemAdapter;I)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    iput p2, p0, Lcom/zte/engineer/ItemAdapter$1;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/high16 v3, 0x10000000

    const/4 v4, 0x0

    iget v2, p0, Lcom/zte/engineer/ItemAdapter$1;->val$index:I

    add-int/lit8 v0, v2, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/TouchScreenTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/LcdTestActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_2
    const-string v2, "com.mediatek.ygps"

    const-string v3, "com.mediatek.ygps.YgpsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/UsbMainActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/KeyTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/RingerTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_6
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_7
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/ImeiTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_8
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/AudioLoopTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_9
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.wifi.WifiSettings"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_a
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.bluetooth.BluetoothSettings"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_b
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/VibratorTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_c
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/TestPhoneActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_d
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/BacklightTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_e
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/MemoryTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_f
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/SensorTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_10
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/SDcardTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_11
    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "CAMAPP"

    const-string v3, "__________should to boot the back camera ..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_12
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f06008a

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_0
    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "android.intent.extras.CAMERA_FACING"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "CAMAPP"

    const-string v3, "__________should to boot the front camera ..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_13
    const-string v2, "com.mediatek.FMRadio"

    const-string v3, "com.mediatek.FMRadio.FMRadioEMActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    :pswitch_14
    iget-object v2, p0, Lcom/zte/engineer/ItemAdapter$1;->this$0:Lcom/zte/engineer/ItemAdapter;

    invoke-static {v2}, Lcom/zte/engineer/ItemAdapter;->access$000(Lcom/zte/engineer/ItemAdapter;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/zte/engineer/SIMTest;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method
