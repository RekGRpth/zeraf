.class public Lcom/zte/engineer/LcdOffTest;
.super Lcom/zte/engineer/ZteActivity;
.source "LcdOffTest.java"


# static fields
.field private static final DELAY_TIME:I = 0x7d0

.field private static final LCDOFF_TIMER_EVENT_TICK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "LcdOffTest"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mLcdPowerControl:Lcom/zte/engineer/LcdPowerControl;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    new-instance v0, Lcom/zte/engineer/LcdOffTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/LcdOffTest$1;-><init>(Lcom/zte/engineer/LcdOffTest;)V

    iput-object v0, p0, Lcom/zte/engineer/LcdOffTest;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/LcdOffTest;Z)V
    .locals 0
    .param p0    # Lcom/zte/engineer/LcdOffTest;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/zte/engineer/LcdOffTest;->finishSelf(Z)V

    return-void
.end method

.method private finishSelf(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/zte/engineer/LcdOffTest;->finishSelf(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x4

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030012

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f08005e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f08005f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/zte/engineer/LcdPowerControl;

    invoke-direct {v0}, Lcom/zte/engineer/LcdPowerControl;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/LcdOffTest;->mLcdPowerControl:Lcom/zte/engineer/LcdPowerControl;

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/zte/engineer/LcdOffTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    iget-object v0, p0, Lcom/zte/engineer/LcdOffTest;->mLcdPowerControl:Lcom/zte/engineer/LcdPowerControl;

    invoke-virtual {v0}, Lcom/zte/engineer/LcdPowerControl;->LcdPowerOn()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LcdOffTest"

    const-string v1, "LcdPowerOn fail"

    invoke-static {v0, v1}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/zte/engineer/LcdOffTest;->finishSelf(Z)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/zte/engineer/LcdOffTest;->mLcdPowerControl:Lcom/zte/engineer/LcdPowerControl;

    invoke-virtual {v0}, Lcom/zte/engineer/LcdPowerControl;->LcdPowerOff()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LcdOffTest"

    const-string v1, "LcdPowerOff fail"

    invoke-static {v0, v1}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/zte/engineer/LcdOffTest;->finishSelf(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/zte/engineer/LcdOffTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
