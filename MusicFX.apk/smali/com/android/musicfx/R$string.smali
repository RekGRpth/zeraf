.class public final Lcom/android/musicfx/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/musicfx/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f06000b

.field public static final bass_boost_strength:I = 0x7f06000f

.field public static final ci_extreme:I = 0x7f060011

.field public static final eq_dialog_title:I = 0x7f06000d

.field public static final eq_preset_classical:I = 0x7f060002

.field public static final eq_preset_dance:I = 0x7f060003

.field public static final eq_preset_flat:I = 0x7f060004

.field public static final eq_preset_folk:I = 0x7f060005

.field public static final eq_preset_heavymetal:I = 0x7f060006

.field public static final eq_preset_hiphop:I = 0x7f060007

.field public static final eq_preset_jazz:I = 0x7f060008

.field public static final eq_preset_normal:I = 0x7f060001

.field public static final eq_preset_pop:I = 0x7f060009

.field public static final eq_preset_rock:I = 0x7f06000a

.field public static final headset_plug:I = 0x7f06000e

.field public static final main_toggle_effects_title:I = 0x7f060014

.field public static final no_effects:I = 0x7f06000c

.field public static final picker_title:I = 0x7f060013

.field public static final pr_dialog_title:I = 0x7f060017

.field public static final pr_summary:I = 0x7f060016

.field public static final pr_title:I = 0x7f060015

.field public static final presetreverb_name:I = 0x7f060000

.field public static final setup:I = 0x7f060018

.field public static final user:I = 0x7f060012

.field public static final virtualizer_strength:I = 0x7f060010


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
