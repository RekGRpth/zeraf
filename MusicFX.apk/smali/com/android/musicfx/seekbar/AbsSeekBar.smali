.class public abstract Lcom/android/musicfx/seekbar/AbsSeekBar;
.super Lcom/android/musicfx/seekbar/ProgressBar;
.source "AbsSeekBar.java"


# static fields
.field private static final NO_ALPHA:I = 0xff


# instance fields
.field private mDisabledAlpha:F

.field private mIsDragging:Z

.field mIsUserSeekable:Z

.field mIsVertical:Z

.field private mKeyProgressIncrement:I

.field private mScaledTouchSlop:I

.field private mThumb:Landroid/graphics/drawable/Drawable;

.field private mThumbOffset:I

.field private mTouchDownX:F

.field private mTouchDownY:F

.field mTouchProgressOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/android/musicfx/seekbar/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsUserSeekable:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    iput v1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/musicfx/seekbar/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsUserSeekable:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    iput v1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/musicfx/seekbar/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v5, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsUserSeekable:Z

    iput-boolean v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    iput v5, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    sget-object v3, Lcom/android/internal/R$styleable;->SeekBar:[I

    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->getThumbOffset()I

    move-result v3

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumbOffset(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v3, Lcom/android/internal/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v3, 0x3

    const/high16 v4, 0x3f000000

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mDisabledAlpha:F

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mScaledTouchSlop:I

    return-void
.end method

.method private attemptClaimDrag()V
    .locals 2

    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    return-void
.end method

.method private setThumbPos(IILandroid/graphics/drawable/Drawable;FI)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/graphics/drawable/Drawable;
    .param p4    # F
    .param p5    # I

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    iget-boolean v10, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v10, :cond_0

    iget v10, p0, Landroid/view/View;->mPaddingTop:I

    sub-int v10, p2, v10

    iget v11, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int/2addr v10, v11

    sub-int v1, v10, v6

    :goto_0
    iget v10, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v1, v10

    iget-boolean v10, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v10, :cond_2

    const/high16 v10, 0x3f800000

    sub-float v10, v10, p4

    int-to-float v11, v1

    mul-float/2addr v10, v11

    float-to-int v7, v10

    const/high16 v10, -0x80000000

    move/from16 v0, p5

    if-ne v0, v10, :cond_1

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget v3, v4, Landroid/graphics/Rect;->left:I

    iget v5, v4, Landroid/graphics/Rect;->right:I

    :goto_1
    add-int v10, v7, v6

    invoke-virtual {p3, v3, v7, v5, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :goto_2
    return-void

    :cond_0
    iget v10, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int v10, p1, v10

    iget v11, p0, Landroid/view/View;->mPaddingRight:I

    sub-int/2addr v10, v11

    sub-int v1, v10, v8

    goto :goto_0

    :cond_1
    move/from16 v3, p5

    add-int v5, p5, v8

    goto :goto_1

    :cond_2
    int-to-float v10, v1

    mul-float v10, v10, p4

    float-to-int v7, v10

    const/high16 v10, -0x80000000

    move/from16 v0, p5

    if-ne v0, v10, :cond_3

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget v9, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->bottom:I

    :goto_3
    add-int v10, v7, v8

    invoke-virtual {p3, v7, v9, v10, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_2

    :cond_3
    move/from16 v9, p5

    add-int v2, p5, v6

    goto :goto_3
.end method

.method private trackTouchEvent(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    iget-boolean v8, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v8, p0, Landroid/view/View;->mPaddingTop:I

    sub-int v8, v1, v8

    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int v0, v8, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v7, v8

    iget v8, p0, Landroid/view/View;->mPaddingTop:I

    if-ge v7, v8, :cond_0

    const/high16 v4, 0x3f800000

    :goto_0
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v2

    int-to-float v8, v2

    mul-float/2addr v8, v4

    add-float/2addr v3, v8

    :goto_1
    float-to-int v8, v3

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lcom/android/musicfx/seekbar/ProgressBar;->setProgress(IZ)V

    return-void

    :cond_0
    iget v8, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int v8, v1, v8

    if-le v7, v8, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const/high16 v8, 0x3f800000

    iget v9, p0, Landroid/view/View;->mPaddingTop:I

    sub-int v9, v7, v9

    int-to-float v9, v9

    int-to-float v10, v0

    div-float/2addr v9, v10

    sub-float v4, v8, v9

    iget v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mTouchProgressOffset:F

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int v8, v5, v8

    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v0, v8, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v6, v8

    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    if-ge v6, v8, :cond_3

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v2

    int-to-float v8, v2

    mul-float/2addr v8, v4

    add-float/2addr v3, v8

    goto :goto_1

    :cond_3
    iget v8, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v8, v5, v8

    if-le v6, v8, :cond_4

    const/high16 v4, 0x3f800000

    goto :goto_2

    :cond_4
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int v8, v6, v8

    int-to-float v8, v8

    int-to-float v9, v0

    div-float v4, v8, v9

    iget v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mTouchProgressOffset:F

    goto :goto_2
.end method

.method private updateThumbPos(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iget-object v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    iget-boolean v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v0, :cond_6

    if-nez v3, :cond_3

    move v10, v5

    :goto_0
    iget v0, p0, Lcom/android/musicfx/seekbar/ProgressBar;->mMaxWidth:I

    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int v1, p1, v1

    iget v2, p0, Landroid/view/View;->mPaddingRight:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v8

    if-lez v8, :cond_0

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v8

    div-float v4, v0, v1

    :cond_0
    if-le v10, v12, :cond_4

    if-eqz v3, :cond_1

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumbPos(IILandroid/graphics/drawable/Drawable;FI)V

    :cond_1
    sub-int v0, v10, v12

    div-int/lit8 v7, v0, 0x2

    if-eqz v6, :cond_2

    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v0, p1, v0

    sub-int/2addr v0, v7

    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int v1, p2, v1

    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    sub-int/2addr v1, v2

    invoke-virtual {v6, v7, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    goto :goto_0

    :cond_4
    if-eqz v6, :cond_5

    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v0, p1, v0

    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int v1, p2, v1

    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    sub-int/2addr v1, v2

    invoke-virtual {v6, v5, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_5
    sub-int v0, v12, v10

    div-int/lit8 v5, v0, 0x2

    if-eqz v3, :cond_2

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumbPos(IILandroid/graphics/drawable/Drawable;FI)V

    goto :goto_1

    :cond_6
    if-nez v3, :cond_9

    move v9, v5

    :goto_2
    iget v0, p0, Lcom/android/musicfx/seekbar/ProgressBar;->mMaxHeight:I

    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    sub-int v1, p2, v1

    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v11

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v8

    if-lez v8, :cond_7

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, v8

    div-float v4, v0, v1

    :cond_7
    if-le v9, v11, :cond_a

    if-eqz v3, :cond_8

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumbPos(IILandroid/graphics/drawable/Drawable;FI)V

    :cond_8
    sub-int v0, v9, v11

    div-int/lit8 v7, v0, 0x2

    if-eqz v6, :cond_2

    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v0, p1, v0

    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int v1, p2, v1

    sub-int/2addr v1, v7

    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    sub-int/2addr v1, v2

    invoke-virtual {v6, v5, v7, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    :cond_9
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    goto :goto_2

    :cond_a
    if-eqz v6, :cond_b

    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    sub-int v0, p1, v0

    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    sub-int v1, p2, v1

    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    sub-int/2addr v1, v2

    invoke-virtual {v6, v5, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_b
    sub-int v0, v11, v9

    div-int/lit8 v5, v0, 0x2

    if-eqz v3, :cond_2

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumbPos(IILandroid/graphics/drawable/Drawable;FI)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 4

    invoke-super {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->drawableStateChanged()V

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0xff

    :goto_0
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    return-void

    :cond_2
    const/high16 v2, 0x437f0000

    iget v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mDisabledAlpha:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    goto :goto_0
.end method

.method public getKeyProgressIncrement()I
    .locals 1

    iget v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    return v0
.end method

.method public getThumbOffset()I
    .locals 1

    iget v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/android/musicfx/seekbar/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    int-to-float v0, v0

    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    iget v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    iget v1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method onKeyChange()V
    .locals 0

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getProgress()I

    move-result v0

    const/16 v2, 0x15

    if-ne p1, v2, :cond_0

    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v2, :cond_1

    :cond_0
    const/16 v2, 0x14

    if-ne p1, v2, :cond_2

    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v2, :cond_2

    :cond_1
    if-lez v0, :cond_5

    iget v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    sub-int v2, v0, v2

    invoke-virtual {p0, v2, v1}, Lcom/android/musicfx/seekbar/ProgressBar;->setProgress(IZ)V

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onKeyChange()V

    :goto_0
    return v1

    :cond_2
    const/16 v2, 0x16

    if-ne p1, v2, :cond_3

    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v2, :cond_4

    :cond_3
    const/16 v2, 0x13

    if-ne p1, v2, :cond_5

    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v2

    if-ge v0, v2, :cond_5

    iget v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2, v1}, Lcom/android/musicfx/seekbar/ProgressBar;->setProgress(IZ)V

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onKeyChange()V

    goto :goto_0

    :cond_5
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected declared-synchronized onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_2

    :goto_0
    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v4, p0, Lcom/android/musicfx/seekbar/ProgressBar;->mMinWidth:I

    iget v5, p0, Lcom/android/musicfx/seekbar/ProgressBar;->mMaxWidth:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v4, p0, Lcom/android/musicfx/seekbar/ProgressBar;->mMinHeight:I

    iget v5, p0, Lcom/android/musicfx/seekbar/ProgressBar;->mMaxHeight:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_0
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    iget v5, p0, Landroid/view/View;->mPaddingRight:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    iget v5, p0, Landroid/view/View;->mPaddingBottom:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    const/4 v4, 0x0

    invoke-static {v2, p1, v4}, Lcom/android/musicfx/seekbar/AbsSeekBar;->resolveSizeAndState(III)I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v1, p2, v5}, Lcom/android/musicfx/seekbar/AbsSeekBar;->resolveSizeAndState(III)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Landroid/view/View;->setMeasuredDimension(II)V

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    if-le v4, v5, :cond_1

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method onProgressRefresh(FZ)V
    .locals 6
    .param p1    # F
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/android/musicfx/seekbar/ProgressBar;->onProgressRefresh(FZ)V

    iget-object v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    const/high16 v5, -0x80000000

    move-object v0, p0

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setThumbPos(IILandroid/graphics/drawable/Drawable;FI)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2}, Lcom/android/musicfx/seekbar/AbsSeekBar;->updateThumbPos(II)V

    return-void
.end method

.method onStartTrackingTouch()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsDragging:Z

    return-void
.end method

.method onStopTrackingTouch()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsDragging:Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsUserSeekable:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_2
    :goto_1
    move v2, v3

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Landroid/view/View;->isInScrollingContainer()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mTouchDownX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mTouchDownY:F

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v3}, Landroid/view/View;->setPressed(Z)V

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    :cond_4
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStartTrackingTouch()V

    invoke-direct {p0, p1}, Lcom/android/musicfx/seekbar/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    invoke-direct {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->attemptClaimDrag()V

    goto :goto_1

    :pswitch_1
    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsDragging:Z

    if-eqz v2, :cond_5

    invoke-direct {p0, p1}, Lcom/android/musicfx/seekbar/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v2, :cond_7

    iget v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mTouchDownY:F

    sub-float v2, v1, v2

    :goto_2
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mScaledTouchSlop:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_2

    invoke-virtual {p0, v3}, Landroid/view/View;->setPressed(Z)V

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    :cond_6
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStartTrackingTouch()V

    invoke-direct {p0, p1}, Lcom/android/musicfx/seekbar/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    invoke-direct {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->attemptClaimDrag()V

    goto :goto_1

    :cond_7
    iget v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mTouchDownX:F

    sub-float v2, v0, v2

    goto :goto_2

    :pswitch_2
    iget-boolean v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsDragging:Z

    if-eqz v4, :cond_8

    invoke-direct {p0, p1}, Lcom/android/musicfx/seekbar/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStopTrackingTouch()V

    invoke-virtual {p0, v2}, Landroid/view/View;->setPressed(Z)V

    :goto_3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStartTrackingTouch()V

    invoke-direct {p0, p1}, Lcom/android/musicfx/seekbar/AbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStopTrackingTouch()V

    goto :goto_3

    :pswitch_3
    iget-boolean v4, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsDragging:Z

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->onStopTrackingTouch()V

    invoke-virtual {p0, v2}, Landroid/view/View;->setPressed(Z)V

    :cond_9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setKeyProgressIncrement(I)V
    .locals 0
    .param p1    # I

    if-gez p1, :cond_0

    neg-int p1, p1

    :cond_0
    iput p1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    return-void
.end method

.method public declared-synchronized setMax(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/android/musicfx/seekbar/ProgressBar;->setMax(I)V

    iget v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v0

    iget v1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mKeyProgressIncrement:I

    div-int/2addr v0, v1

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/musicfx/seekbar/ProgressBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41a00000

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setKeyProgressIncrement(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eq p1, v2, :cond_3

    iget-object v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    const/4 v0, 0x1

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-boolean v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mIsVertical:Z

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iget-object v3, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    if-eq v2, v3, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    :cond_1
    iput-object p1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/android/musicfx/seekbar/AbsSeekBar;->updateThumbPos(II)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    goto :goto_1
.end method

.method public setThumbOffset(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumbOffset:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/musicfx/seekbar/AbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/android/musicfx/seekbar/ProgressBar;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
