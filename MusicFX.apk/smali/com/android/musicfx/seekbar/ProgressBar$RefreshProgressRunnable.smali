.class Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;
.super Ljava/lang/Object;
.source "ProgressBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/musicfx/seekbar/ProgressBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshProgressRunnable"
.end annotation


# instance fields
.field private mFromUser:Z

.field private mId:I

.field private mProgress:I

.field final synthetic this$0:Lcom/android/musicfx/seekbar/ProgressBar;


# direct methods
.method constructor <init>(Lcom/android/musicfx/seekbar/ProgressBar;IIZ)V
    .locals 0
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    iput-object p1, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->this$0:Lcom/android/musicfx/seekbar/ProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mId:I

    iput p3, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mProgress:I

    iput-boolean p4, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mFromUser:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->this$0:Lcom/android/musicfx/seekbar/ProgressBar;

    iget v1, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mId:I

    iget v2, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mProgress:I

    iget-boolean v3, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mFromUser:Z

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/musicfx/seekbar/ProgressBar;->access$000(Lcom/android/musicfx/seekbar/ProgressBar;IIZZ)V

    iget-object v0, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->this$0:Lcom/android/musicfx/seekbar/ProgressBar;

    invoke-static {v0, p0}, Lcom/android/musicfx/seekbar/ProgressBar;->access$102(Lcom/android/musicfx/seekbar/ProgressBar;Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;)Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;

    return-void
.end method

.method public setup(IIZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    iput p1, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mId:I

    iput p2, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mProgress:I

    iput-boolean p3, p0, Lcom/android/musicfx/seekbar/ProgressBar$RefreshProgressRunnable;->mFromUser:Z

    return-void
.end method
