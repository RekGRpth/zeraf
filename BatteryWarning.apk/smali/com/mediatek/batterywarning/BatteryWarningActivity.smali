.class public Lcom/mediatek/batterywarning/BatteryWarningActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BatteryWarningActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final BATTERY_OVER_TEMPERATURE_TYPE:I = 0x1

.field private static final BATTERY_OVER_VOLTAGE_TYPE:I = 0x3

.field private static final CHARGER_OVER_VOLTAGE_TYPE:I = 0x0

.field private static final CURRENT_OVER_PROTECTION_TYPE:I = 0x2

.field private static final SAFETY_OVER_TIMEOUT_TYPE:I = 0x4

.field private static final SHARED_PREFERENCES_NAME:Ljava/lang/String; = "battery_warning_settings"

.field private static final TAG:Ljava/lang/String; = "BatteryWarningActivity"

.field private static final WARNING_SOUND_URI:Landroid/net/Uri;

.field private static final sWarningMsg:[I

.field private static final sWarningTitle:[I


# instance fields
.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRingtone:Landroid/media/Ringtone;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x5

    const-string v0, "file:///system/media/audio/ui/VideoRecord.ogg"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->WARNING_SOUND_URI:Landroid/net/Uri;

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->sWarningTitle:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->sWarningMsg:[I

    return-void

    :array_0
    .array-data 4
        0x7f050001
        0x7f050002
        0x7f050003
        0x7f050004
        0x7f050005
    .end array-data

    :array_1
    .array-data 4
        0x7f050006
        0x7f050007
        0x7f050008
        0x7f050009
        0x7f05000a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/batterywarning/BatteryWarningActivity$1;-><init>(Lcom/mediatek/batterywarning/BatteryWarningActivity;)V

    iput-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/batterywarning/BatteryWarningActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/batterywarning/BatteryWarningActivity;

    iget v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    return v0
.end method

.method private createView(I)Landroid/view/View;
    .locals 6
    .param p1    # I

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/high16 v4, 0x7f040000

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/high16 v3, 0x7f020000

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v2
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 2

    const-string v0, "battery_warning_settings"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private playAlertSound(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mRingtone:Landroid/media/Ringtone;

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mRingtone:Landroid/media/Ringtone;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mRingtone:Landroid/media/Ringtone;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->setStreamType(I)V

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->play()V

    :cond_0
    return-void
.end method

.method private showWarningDialog(I)V
    .locals 4
    .param p1    # I

    invoke-direct {p0}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lcom/mediatek/batterywarning/BatteryWarningActivity;->sWarningTitle:[I

    aget v2, v2, p1

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "BatteryWarningActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showDialog flag is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningActivity;->sWarningTitle:[I

    aget v1, v1, p1

    sget-object v2, Lcom/mediatek/batterywarning/BatteryWarningActivity;->sWarningMsg:[I

    aget v2, v2, p1

    invoke-direct {p0, v1, v2}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->warningMessageDialog(II)V

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningActivity;->WARNING_SOUND_URI:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->playAlertSound(Landroid/net/Uri;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private stopRingtone()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mRingtone:Landroid/media/Ringtone;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mRingtone:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    :cond_0
    return-void
.end method

.method private warningMessageDialog(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    invoke-direct {p0, p2}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->createView(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    const v1, 0x7f05000b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const v1, 0x7f05000c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0}, Lcom/android/internal/app/AlertActivity;->setupAlert()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v1, "BatteryWarningActivity"

    const-string v2, "onClick"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->stopRingtone()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->stopRingtone()V

    invoke-direct {p0}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningActivity;->sWarningTitle:[I

    iget v2, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    aget v1, v1, v2

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v1, "BatteryWarningActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " false"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    const-string v1, "BatteryWarningActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate, mType is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    const/4 v2, 0x4

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    invoke-direct {p0, v1}, Lcom/mediatek/batterywarning/BatteryWarningActivity;->showWarningDialog(I)V

    iget-object v1, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mType:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
