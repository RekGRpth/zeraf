.class public final Lcom/mediatek/batterywarning/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/batterywarning/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final batteryWarning_label:I = 0x7f050000

.field public static final btn_cancel_msg:I = 0x7f05000c

.field public static final btn_ok_msg:I = 0x7f05000b

.field public static final msg_battery_over_temperature:I = 0x7f050007

.field public static final msg_battery_over_voltage:I = 0x7f050009

.field public static final msg_charger_over_voltage:I = 0x7f050006

.field public static final msg_over_current_protection:I = 0x7f050008

.field public static final msg_safety_timer_timeout:I = 0x7f05000a

.field public static final title_battery_over_temperature:I = 0x7f050002

.field public static final title_battery_over_voltage:I = 0x7f050004

.field public static final title_charger_over_voltage:I = 0x7f050001

.field public static final title_over_current_protection:I = 0x7f050003

.field public static final title_safety_timer_timeout:I = 0x7f050005


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
