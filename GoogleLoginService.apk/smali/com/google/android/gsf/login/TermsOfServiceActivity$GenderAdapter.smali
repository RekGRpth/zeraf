.class public Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TermsOfServiceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/TermsOfServiceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "GenderAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/ArrayAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mHint:Ljava/lang/String;

.field private mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

.field private mObjects:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[TT;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    check-cast p3, [Ljava/lang/CharSequence;

    check-cast p3, [Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mObjects:[Ljava/lang/CharSequence;

    sget-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->GONE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    iput-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHint:Ljava/lang/String;

    return-void
.end method

.method private bindView(ILandroid/widget/TextView;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/widget/TextView;

    sget-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$1;->$SwitchMap$com$google$android$gsf$login$TermsOfServiceActivity$GenderAdapter$HintState:[I

    iget-object v1, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mObjects:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHint:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mObjects:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->notifyDataSetChanged()V

    sget-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->GONE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    iput-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static createFromResource(Landroid/content/Context;II)Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    invoke-direct {v1, p0, p2, v0}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-object v1
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, p1, v1}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->bindView(ILandroid/widget/TextView;)V

    return-object v0
.end method

.method public hideHint()V
    .locals 1

    sget-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->REMOVE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    iput-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    return-void
.end method

.method public isHintShowing()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    sget-object v1, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->SHOW:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHint:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->SHOW:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    iput-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->mHintState:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    return-void
.end method
