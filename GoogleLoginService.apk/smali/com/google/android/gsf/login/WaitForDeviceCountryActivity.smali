.class public Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "WaitForDeviceCountryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static testMode:Z


# instance fields
.field private final WAITING_FOR_DEVICE_COUNTRY_TIMEOUT:I

.field private mCancelButton:Landroid/widget/Button;

.field private final mGservicesChangeReceiver:Landroid/content/BroadcastReceiver;

.field private final mHandler:Landroid/os/Handler;

.field private final mTimeOutWaitingForDeviceCountry:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->testMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    new-instance v0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity$1;-><init>(Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mGservicesChangeReceiver:Landroid/content/BroadcastReceiver;

    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->WAITING_FOR_DEVICE_COUNTRY_TIMEOUT:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity$2;-><init>(Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;)V

    iput-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mTimeOutWaitingForDeviceCountry:Ljava/lang/Runnable;

    return-void
.end method

.method public static haveCheckin(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p0    # Landroid/content/ContentResolver;

    const/4 v1, 0x0

    sget-boolean v2, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->testMode:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "digest"

    invoke-static {p0, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private initView()V
    .locals 1

    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->setContentView(I)V

    const v0, 0x7f0b001b

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->returnResult(ILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v2, -0x1

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->haveCheckin(Landroid/content/ContentResolver;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->returnResult(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mGservicesChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->haveCheckin(Landroid/content/ContentResolver;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->returnResult(I)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.server.checkin.CHECKIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->initView()V

    iget-object v1, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mTimeOutWaitingForDeviceCountry:Ljava/lang/Runnable;

    const-wide/16 v3, 0x2710

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mGservicesChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->mGservicesChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/WaitForDeviceCountryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
